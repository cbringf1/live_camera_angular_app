var $zoho = $zoho || {};
$zoho.salesiq = $zoho.salesiq ||
    {
        widgetcode: "0fe72782554a3b5def12eff03486574abe1199ab28ce0cfca630075573300f88",
        values: {},
        ready: function () { },
        floatbutton: {
            visible: function () { }
        },
    };
var d = document;
var s = d.createElement("script");

s.type = "text/javascript";
s.id = "zsiqscript";
s.defer = true;
s.src = "https://salesiq.zoho.com/widget";

var t = d.getElementsByTagName("script")[0];

t.parentNode.insertBefore(s, t);
d.write("<div id='zsiqwidget'></div>");

$zoho.salesiq.ready = () => {
    $zoho.salesiq.floatbutton.visible("hide");
}
