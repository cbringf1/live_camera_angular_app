import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
	{
		id: 'livemap',
		title: 'Live Map',
		type: 'group',
		children: [
			{
				id: 'map',
				title: 'Map',
				type: 'item',
				icon: 'map',
				url: '/map/_full'
			},
			{
				id: 'sites-item',
				title: 'Locations',
				type: 'item',
				icon: 'location_city',
				function: () => {}
			},
			{
				id: 'add-site',
				title: 'Add Location',
				type: 'item',
				icon: 'add',
				function: () => {}
			},
			{
				id: 'add-camera',
				title: 'Add Camera',
				type: 'item',
				icon: 'add',
				function: () => {}
			}
		]
	},
	{
		id: 'management',
		title: 'Management',
		type: 'group',
		children: [
			// {
			// 	id: 'inbox',
			// 	title: 'Inbox',
			// 	type: 'item',
			// 	url: '/management/inbox'
			// },
			{
				id: 'site-list',
				title: 'Locations',
				type: 'item',
				url: '/management/sites',
				exactMatch: true
			},
			// {
			// 	id: 'evidence-list',
			// 	title: 'Evidence',
			// 	type: 'item',
			// 	url: '/management/evidences',
			// 	exactMatch: true
			// }
		]
	},
	{
		id: 'settings',
		title: 'Settings',
		type: 'group',
		children: [{
			id: 'profile',
			title: 'Profile',
			type: 'item',
			url: '/settings'
		}]
	}
];
