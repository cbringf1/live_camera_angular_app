import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthStateService } from '../services/auth/auth-state.service';
import { LocalAuthService } from '../services/auth/local-auth.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {
	constructor(
		private localAuthService: LocalAuthService, 
		private router: Router,
		private authState: AuthStateService) { }

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		return this.checkLogin(state.url);
	}

	private checkLogin(url: string): boolean {
		if(this.localAuthService.isLoggedIn()) {
			return true;
		}
		else {
			this.authState.redirectUrl = url;
			this.router.navigate(['/login']);
			return false;
		}
	}
}
