import { ISite } from '../repositories/site-repo/site.model';

export const siteListRowFactory = (site: ISite) => {
		return new Object({
			id: site.id,
			name: site.name,
			address: site.address || 'unavailable',
			city: site.city || 'unavailable',
			state: site.state || 'unavailable',
			zip: site.zip || 'unavailable',
			cameras: site.cameras.length
		})
	};