export const parentNavigationItem = (id, title, icon, children) => {	
	return {
		'id': id,
		'title': title,
		'type': 'group',
		'icon': icon || 'settings',
		'children': children || []
	};
};

export const childNavigationItem = (id, title, cameras, icon, action: Function) => {
	let result = {
		'id': id,
		'title': title,
		'type': 'item',
		'icon': icon || 'settings',
		'function': action
	};
	if (cameras >= 0) {
		result['badge'] = {
			title: cameras,
			bg: '#4E96F5',
			fg: '#FFFFFF'
		};
	}
	return result;
};

export const childUrlNavigationItem = (id, title, badge, icon, link) => {
	let result = {
		'id': id,
		'title': title,
		'type': 'item',
		'icon': icon || 'settings',
		'url': link
	};
	if (badge >= 0) {
		result['badge'] = badge;
	}
	return result;
}