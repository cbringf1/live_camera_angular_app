export const positionFactory = (lat: number, lng: number) => {
	return { lat: lat, lng: lng };
};