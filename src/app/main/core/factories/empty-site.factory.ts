import { ISite } from '../repositories/site-repo/site.model';

export const emptySiteFactory = (): ISite => {
	return {
		name: '',
		lat: 0,
		lon: 0,
		cameras: [],
		address: '',
		city: '',
		state: '',
		zip: ''
	};
};