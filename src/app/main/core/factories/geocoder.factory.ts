import { IGeocoderModel } from '../services/geo/geocoder.model';
import * as _ from 'lodash';

export const geocoderFactory = (data): IGeocoderModel => {	
	if(!data || data.length === 0) return null;
	
	const targetData = data[0].address_components;
	let streetNumber = targetData.filter(d => d.types.indexOf('street_number') > -1);
	let route = targetData.filter(d => d.types.indexOf('route') > -1);
	let locality = targetData.filter(d => d.types.indexOf('locality') > -1);
	let stateCode = targetData.filter(d => d.types.indexOf('administrative_area_level_1') > -1);
	let countryCode = targetData.filter(d => d.types.indexOf('country') > -1);
	let postalCode = targetData.filter(d => d.types.indexOf('postal_code') > -1);

	streetNumber = streetNumber.length > 0 ? `${streetNumber[0].long_name}` : '';
	route = route.length > 0 ? route[0].long_name : '';
	locality = locality.length > 0 ? locality[0].long_name : '';
	stateCode = stateCode.length > 0 ? stateCode[0].short_name : '';
	countryCode = countryCode.length > 0 ? countryCode[0].short_name : '';
	postalCode = postalCode.length > 0 ? postalCode[0].long_name : '';

	let result = new Object({
		fullFormattedAddress: data[0].formatted_address,
		localAddress: `${streetNumber} ${route}` || 'no address',
		city: locality,
		state: `${countryCode}-${stateCode}`,
		zip: postalCode
	}) as IGeocoderModel;
	return _.mapValues(result, v => v.trim() === '' ? 'unavailable' : v);
};