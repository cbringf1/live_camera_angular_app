import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {
    AbstractControl, FormBuilder,
    FormGroup, ValidationErrors,
    ValidatorFn, Validators
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { LocalAuthService } from '../../services/auth/local-auth.service';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { nullifyFields } from '../../services/utils/nullify-fields';
import { IRegisterUser } from '../../services/auth/register-user.model';
import { backgroundLoader, getBackgroundSubject, BG_KEY } from '../../services/utils/resource-loader/dynamic-bg-loader';
import { defaults } from '../../config/defaults';
import { ISiteowner } from '../../repositories/siteowner/siteowner.model';
import { SiteInfoService } from '../../services/utils/resource-loader/site-info.service';
import { MatDialog } from '@angular/material';
import { UseTermsDialogComponent } from '../dialogs/use-terms-form/use-terms.dialog.component';

@Component({
    selector: 'register-2',
    templateUrl: './register-2.component.html',
    styleUrls: ['./register-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Register2Component implements OnInit, OnDestroy {
    registerForm: FormGroup;
    apiError: any = {};
    backgroundImage: string;
    departmentName: string;
    accepted: boolean;
    wasRead: boolean;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private localAuth: LocalAuthService,
        private router: Router,
        private siteInfoService: SiteInfoService,
        private dialog: MatDialog
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        this.accepted = false;
        this.wasRead = false;

        // Set the private defaults
        this._unsubscribeAll = new Subject();

        this.backgroundImage = defaults.backgroundImage;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.registerForm = this._formBuilder.group({
            first_name: ['', [Validators.required, Validators.maxLength(50)]],
            last_name: ['', [Validators.required, Validators.maxLength(50)]],
            organization_name: [''],
            email: ['', [Validators.required, Validators.email]],
            password1: ['', Validators.required],
            password2: ['', [Validators.required, confirmPasswordValidator]]
        });
        // backgroundLoader();

        getBackgroundSubject().subscribe(() => {
            const url = localStorage.getItem(BG_KEY);
            this.backgroundImage = url || this.backgroundImage;
        });
        getBackgroundSubject().next();

        this.siteInfoService.getAllSiteInfo().subscribe((info: ISiteowner) => {
            this.departmentName = info.name;
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.registerForm.get('password1').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.registerForm.get('password2').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    createAccount() {
        if (this.registerForm.valid) {
            this.showTermsOfUsesDialog().subscribe(res => {
                this.wasRead = res.wasRead;
                this.accepted = res.accepted;

                if (this.accepted) {
                    let user = { username: this.registerForm.get('email').value };
                    let finalUser = {
                        ...nullifyFields(_.merge(user, this.registerForm.value)),
                        agree_terms: true
                    };
                    this.localAuth.emailSignUp(finalUser as IRegisterUser)
                        .subscribe(() => this.router.navigate(['/account-confirm/_unconfirmed']),
                            err => {
                                if (err.status === 400) {
                                    this.apiError = err.error;
                                    this.apiError = _.mapValues(this.apiError, e => e[0]);
                                }
                            }
                        );
                }
            });
        }
    }

    showTermsOfUsesDialog() {
        const dialogRef = this.dialog.open(UseTermsDialogComponent, {
            maxWidth: '100vw',
            maxHeight: '100vh',
            height: '90%',
            width: '50%',
            disableClose: true,
        });
        return dialogRef.afterClosed();
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password1');
    const passwordConfirm = control.parent.get('password2');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { 'passwordsNotMatching': true };
};
