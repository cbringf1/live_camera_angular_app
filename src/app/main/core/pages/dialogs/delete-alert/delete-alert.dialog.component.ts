import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SiteFormDialogComponent } from '../site-form/site-form.dialog.component';

@Component({
	selector: 'delete-alert',
	templateUrl: './delete-alert.dialog.component.html',
	styleUrls: ['./delete-alert.dialog.component.scss']
})
export class DeleteAlertDialogComponent {
	title: string;
	body: string;

	constructor(
		@Inject(MAT_DIALOG_DATA) private data: any,
		public dialogRef: MatDialogRef<SiteFormDialogComponent>) {
		this.title = `Delete ${data.itemType}`;
		this.body = `Are you sure to delete ${data.itemName} ${data.itemType}?`

	}

	delete() {
		this.dialogRef.close(true);
	}
}