import { Component, OnInit, ViewChild, ElementRef, NgZone, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import * as _ from 'lodash';
import { ISite } from 'app/main/core/repositories/site-repo/site.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IPosition } from 'app/main/core/services/geo/position.model';
import { emptySiteFactory } from 'app/main/core/factories/empty-site.factory';
import { positionFactory } from 'app/main/core/factories/position.factory';
import { geocoderFactory } from 'app/main/core/factories/geocoder.factory';
import { IGeocoderModel } from 'app/main/core/services/geo/geocoder.model';
import { SiteMapComponent } from 'app/main/core/components/site-map/site-map.component';
import { siteFormGroupData } from 'app/main/core/config/forms';
import { SiteRepository } from 'app/main/core/repositories/site-repo/site.repository';
import { HttpErrorResponse } from '@angular/common/http';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Observable } from 'rxjs';
import { nullifyFields } from 'app/main/core/services/utils/nullify-fields';
import { LocalAuthService } from 'app/main/core/services/auth/local-auth.service';
import { SiteInfoService } from 'app/main/core/services/utils/resource-loader/site-info.service';
import { ISiteowner } from 'app/main/core/repositories/siteowner/siteowner.model';

const FREQUENTLY_FIELDS = [
    'fullAddress',
    'address',
    'lat',
    'lon',
    'zip',
    'city',
    'state'];

@Component({
    selector: 'site-form',
    templateUrl: './site-form.dialog.component.html',
    styleUrls: ['./site-form.dialog.component.scss']
})
export class SiteFormDialogComponent implements OnInit {
    form: FormGroup;
    @ViewChild('search') searchElementRef: ElementRef;
    @ViewChild('siteMap') siteMap: SiteMapComponent;
    site: ISite;
    address: string;
    httpErrors: any;
    title: string;
    siteowner: ISiteowner;

    constructor(
        private formBuilder: FormBuilder,
        private mapsApiLoader: MapsAPILoader,
        private ngZone: NgZone,
        @Inject(MAT_DIALOG_DATA) private data: any,
        public dialogRef: MatDialogRef<SiteFormDialogComponent>,
        private siteRepo: SiteRepository,
        private localAuth: LocalAuthService,
        private siteInfoService: SiteInfoService
    ) { }

    get currentSite() {
        return this.site;
    }

    ngOnInit() {
        this.title = this.data.title;
        if (this.data && this.data.site) {
            this.site = this.data.site;
            this.siteMap.computePosition(this.site, null);
        }
        else {
            this.site = emptySiteFactory();
            this.siteInfoService.getAllSiteInfo().subscribe((info: ISiteowner) => {
                this.siteowner = info;
                this.siteMap.computePosition(null, this.siteowner);
            });
        }

        this.form = this.formBuilder.group(siteFormGroupData(this.site, this.localAuth.getUser()));
        this.mapsApiLoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ["address"]
            });

            autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    let address = geocoderFactory(place ? [place] : null);

                    if (place.geometry) {
                        this.updatePositionData(
                            positionFactory(
                                place.geometry.location.lat(),
                                place.geometry.location.lng()),
                            address
                        );
                        this.form.patchValue(_.pick(this.site, FREQUENTLY_FIELDS));
                    }
                });
            });
        });
        this.form.valueChanges
            .subscribe(f => {
                if (f.primary_contact_phone && f.primary_contact_phone.startsWith("+1")) {
                    this.form.get('primary_contact_phone')
                        .setValue(f.primary_contact_phone.slice(2));
                }
                if (f.secondary_contact_phone && f.secondary_contact_phone.startsWith("+1")) {
                    this.form.get('secondary_contact_phone')
                        .setValue(f.secondary_contact_phone.slice(2));
                }
            });
    }

    private updatePositionData(position: IPosition, address: IGeocoderModel) {
        this.site.lat = position.lat;
        this.site.lon = position.lng;
        this.site.fullAddress = address.fullFormattedAddress;
        _.merge(this.site, _.omit(address, ['fullFormattedAddress', 'localAddress']));
        this.site.address = address.localAddress;
        this.site = _.clone(this.site);
    }

    positionLoaded(event: { position: IPosition, address: IGeocoderModel }) {
        this.updatePositionData(event.position, event.address);
        this.form.patchValue(_.pick(this.site, FREQUENTLY_FIELDS));
    }

    saveSite() {
        if (this.form.valid) {
            let result = _.omit(this.form.value, ['fullAddress']);

            if (this.site && this.site.id) {
                result['id'] = this.site.id;
            }
            result = nullifyFields(result);
            for (let k of Object.keys(result)) {
                if (k.endsWith('phone') && result[k]) {
                    result[k] = `+1${result[k]}`;
                }
            }
            if (!this.data.site) {
                console.log('::::::::::::::::::::::::::::::create');
                
                this.handleObservable(this.siteRepo.create(result));
            }
            else {
                console.log('::::::::::::::::::::::::::::::patch');
                this.handleObservable(this.siteRepo.patch(result));
            }
        }
    }

    private handleObservable(obs: Observable<ISite>) {
        obs.subscribe(
            res => {
                this.dialogRef.close(res)
            },
            err => {
                this.handleHttpErrors(err)
            }
        );
    }

    private handleHttpErrors(err: HttpErrorResponse) {
        if (err.status === 400) {
            this.httpErrors = err.error;
        }
    }
}