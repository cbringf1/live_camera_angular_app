import { Component, OnInit, Inject } from '@angular/core';
import { EvidenceRepository } from 'app/main/core/repositories/evidence/evidence.repository';
import { ISite } from 'app/main/core/repositories/site-repo/site.model';
import { SiteRepository } from 'app/main/core/repositories/site-repo/site.repository';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { defaults } from 'app/main/core/config/defaults';
import { switchMap, map } from 'rxjs/operators';
import { NotificationService } from 'app/main/core/services/utils/notification.service';

@Component({
	selector: 'upload-media',
	templateUrl: './upload-media.dialog.component.html',
	styleUrls: ['./upload-media.dialog.component.scss']
})
export class UploadMediaDialogComponent implements OnInit {
	sitesList: ISite[];
	form: FormGroup;
	httpError: string;
	mediaImg = defaults.noMediaLogo;
	private reader: FileReader;
	private file: File;
	private fileName: string;
	private uploading = false;

	constructor(
		private formBuilder: FormBuilder,
		private evidenceRepo: EvidenceRepository,
		private siteRepo: SiteRepository,
		private dialogRef: MatDialogRef<UploadMediaDialogComponent>,
		private notificationService: NotificationService,
		@Inject(MAT_DIALOG_DATA) private data: any,
	) {
		this.reader = new FileReader();
	}

	ngOnInit() {
		const media = this.data.media;

		this.form = this.formBuilder.group({
			name: [
				media ? media.name : '',
				Validators.required
			],
			extension: [
				media ? media.extension : null,
				Validators.required
			],
			date: [
				media ? media.date : '',
				Validators.required
			],
			id: [
				media ? media.id : ''
			]
		});
		this.siteRepo.getAll().subscribe(res => this.sitesList = res);
		this.reader.onload = e => this.mediaImg = (<any>e.target).result;
	}

	selectionChange(event) {
		this.form.patchValue({
			location_id: event
		});
	}

	saveEvidence() {
		let mediaRes;

		if (this.form.valid) {
			if (typeof this.form.value.date !== 'string') {
				this.form.value.date = this.form.value.date.format();
			}
			this.uploading = true;
			this.evidenceRepo.createMedia(this.form.value, this.data.evidenceId)
				.pipe(switchMap(res => {
					mediaRes = res;
					return this.evidenceRepo.uploadMediaFile(this.file, res);
				}))
				.pipe(map(() => mediaRes))
				.subscribe(res => {
					this.notificationService.notify(['Evidence media successfully created']);
					this.dialogRef.close(res);
					this.uploading = false;
				});
		}
	}

	onFileChange(event) {
		if (event.target.files.length > 0) {
			this.file = event.target.files[0];
			this.fileName = this.file.name;

			const fileExt = this.file.name.split('.');

			if (this.file.type.startsWith('image')) {
				this.reader.readAsDataURL(this.file);
			}
			this.form.patchValue({
				extension: fileExt[fileExt.length - 1]
			});
		}
	}
}