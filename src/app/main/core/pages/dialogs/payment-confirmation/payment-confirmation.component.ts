import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'payment-confirmation',
    templateUrl: './payment-confirmation.component.html',
    styleUrls: ['./payment-confirmation.component.scss']
})
export class PaymentConfirmationDialogComponent implements OnInit {
    result: boolean;

    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any,
        public dialogRef: MatDialogRef<PaymentConfirmationDialogComponent>) {
        this.result = this.data.result;
    }

    ngOnInit() { }
}