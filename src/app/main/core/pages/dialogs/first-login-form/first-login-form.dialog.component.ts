import { Component, OnInit, ViewChild, ElementRef, NgZone, Inject, ViewEncapsulation } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import * as _ from 'lodash';
import { ISite } from 'app/main/core/repositories/site-repo/site.model';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatStepper } from '@angular/material';
import { IPosition } from 'app/main/core/services/geo/position.model';
import { emptySiteFactory } from 'app/main/core/factories/empty-site.factory';
import { positionFactory } from 'app/main/core/factories/position.factory';
import { geocoderFactory } from 'app/main/core/factories/geocoder.factory';
import { IGeocoderModel } from 'app/main/core/services/geo/geocoder.model';
import { SiteMapComponent } from 'app/main/core/components/site-map/site-map.component';
import { siteFormGroupData } from 'app/main/core/config/forms';
import { SiteRepository } from 'app/main/core/repositories/site-repo/site.repository';
import { HttpErrorResponse } from '@angular/common/http';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Observable } from 'rxjs';
import { nullifyFields } from 'app/main/core/services/utils/nullify-fields';
import { LocalAuthService } from 'app/main/core/services/auth/local-auth.service';
import { ICamera } from 'app/main/core/repositories/camera-repo/camera.model';
import { resolutions } from '../camera-form/resolutions';
import { CameraRepository } from 'app/main/core/repositories/camera-repo/camera.repository';
import { NavbarService } from 'app/main/core/services/ui/navbar.service';
import { DialogsService } from '../../../services/ui/dialog.service';
import { CameraConfirmationDialogComponent } from '../camera-added-confirmation/camera-added-confirmation.component';
import { SiteInfoService } from 'app/main/core/services/utils/resource-loader/site-info.service';
import { ISiteowner } from 'app/main/core/repositories/siteowner/siteowner.model';
import { ShareVideoDialogComponent } from '../share-video/share-video.component';
import { NotificationService } from 'app/main/core/services/utils/notification.service';
import { phoneValidator } from 'app/main/core/validators/phone.validator';
import { IUserModel } from 'app/main/core/services/auth/user.model';

const FREQUENTLY_FIELDS = [
    'fullAddress',
    'address',
    'lat',
    'lon',
    'zip',
    'city',
    'state'];

@Component({
    selector: 'first-login-form',
    templateUrl: './first-login-form.dialog.component.html',
    styleUrls: ['./first-login-form.dialog.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FirstLoginFormDialogComponent implements OnInit {
    cameraMapIconNumber: number;
    form: FormGroup;
    @ViewChild('search') searchElementRef: ElementRef;
    @ViewChild('siteMap') siteMap: SiteMapComponent;
    @ViewChild('stepper') stepper: MatStepper;
    site: ISite;
    address: string;
    httpErrors: any;
    httpErrorDialog: string;
    sitesListCameraDialog: ISite[] = [];
    formCameraDialog: FormGroup;
    currentSiteCameraDialog: ISite;
    cameraCoordsCameraDialog: IPosition;
    currentPositionCameraDialog: IPosition;
    cameraToEditCameraDialog: ICamera;
    disabledCamerasCameraDialog: ICamera[] = [];
    @ViewChild('siteMapCameraDialog') siteMapCameraDialog: SiteMapComponent;
    private resolutionListCameraDialog: any[];
    stepName: string;
    departmentName: string;
    enableButtonFinish: boolean;
    siteowner: ISiteowner;
    enableAddNewCameraBtn: boolean;
    isNewCameraToAdd: boolean;
    currentCamera: ICamera;
    wasCreated: boolean;
    canNext: boolean;
    user: IUserModel;

    constructor(
        private formBuilder: FormBuilder,
        private mapsApiLoader: MapsAPILoader,
        private ngZone: NgZone,
        @Inject(MAT_DIALOG_DATA) private data: any,
        public dialogRef: MatDialogRef<FirstLoginFormDialogComponent>,
        private siteRepo: SiteRepository,
        private localAuth: LocalAuthService,
        private cameraRepo: CameraRepository,
        private dialog: MatDialog,
        private navbarService: NavbarService,
        private siteInfoService: SiteInfoService,
        private notificationService: NotificationService
    ) {
        this.cameraMapIconNumber = 4;
        this.enableButtonFinish = false;
        this.enableAddNewCameraBtn = false;
        this.isNewCameraToAdd = true;
        this.wasCreated = false;
        this.canNext = false;
        this.user = this.data.user;

        if (this.data.sitesList) {
            this.data.sitesList.subscribe(res => this.sitesListCameraDialog = res);
        }
        this.resolutionListCameraDialog = resolutions.map(r => new Object({ 'id': r[0], 'value': r[1] }));
    }

    get currentSite() {
        return this.site;
    }

    ngOnInit() {
        this.stepName = 'Get Started';

        this.siteInfoService.getAllSiteInfo().subscribe((info: ISiteowner) => {
            this.departmentName = info.name;
            this.siteowner = info;
            this.siteMap.computePosition(null, this.siteowner);

            if (this.data && this.data.site) {
                this.site = this.data.site;
                this.siteMap.computePosition(this.site);
            }
            else {
                this.site = emptySiteFactory();
                this.siteMap.computePosition();
            }

            // if (this.currentSiteCameraDialog) {
            //     this.siteMapCameraDialog.computePosition(this.currentSiteCameraDialog);                
            // }
            // else {
            //     this.currentSiteCameraDialog = emptySiteFactory();
            //     this.siteMapCameraDialog.computePosition(null, this.siteowner);
            // }
        });

        this.initializeFormCameraDialog();
        this.form = this.formBuilder.group(siteFormGroupData(this.site, this.localAuth.getUser()));
        // this.initializeSiteForm();
        this.mapsApiLoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ["address"]
            });

            autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    let address = geocoderFactory(place ? [place] : null);

                    if (place.geometry) {
                        this.updatePositionData(
                            positionFactory(
                                place.geometry.location.lat(),
                                place.geometry.location.lng()),
                            address
                        );
                        this.form.patchValue(_.pick(this.site, FREQUENTLY_FIELDS));
                    }
                });
            });
        });
        this.form.valueChanges
            .subscribe(f => {
                if (f.primary_contact_phone && f.primary_contact_phone.startsWith("+1")) {
                    this.form.get('primary_contact_phone')
                        .setValue(f.primary_contact_phone.slice(2));
                }
                if (f.secondary_contact_phone && f.secondary_contact_phone.startsWith("+1")) {
                    this.form.get('secondary_contact_phone')
                        .setValue(f.secondary_contact_phone.slice(2));
                }
            });
    }

    // initializeSiteForm() {
    //     this.form = this.formBuilder.group({
    //         fullAddress: [''],
    //         address: ['', Validators.required],
    //         name: ['', Validators.required],
    //         lat: [Validators.required],
    //         lon: [Validators.required],
    //         state: ['', Validators.required],
    //         zip: ['', Validators.required],
    //         city: ['', Validators.required],
    //         primary_contact_name: [''],
    //         primary_contact_email: ['', Validators.email],
    //         primary_contact_phone: ['', phoneValidator('primary_contact_phone')],
    //         secondary_contact_name: ['', phoneValidator('primary_contact_phone')],
    //         secondary_contact_email: ['', Validators.email],
    //         secondary_contact_phone: ['', phoneValidator('secondary_contact_phone')]
    //     });
    // }

    setCameraStepValues() {
        this.formCameraDialog.controls['locationCameraDialog'].setValue(this.currentSiteCameraDialog.id);
        this.siteMapCameraDialog.addDefaultCamMarker(this.currentSiteCameraDialog);
        this.siteMapCameraDialog.computePosition(this.currentSiteCameraDialog);
    }

    private updatePositionData(position: IPosition, address: IGeocoderModel) {
        this.site.lat = position.lat;
        this.site.lon = position.lng;
        this.site.fullAddress = address.fullFormattedAddress;
        _.merge(this.site, _.omit(address, ['fullFormattedAddress', 'localAddress']));
        this.site.address = address.localAddress;
        this.site = _.clone(this.site);
    }

    positionLoaded(event: { position: IPosition, address: IGeocoderModel }) {
        this.updatePositionData(event.position, event.address);
        this.form.patchValue(_.pick(this.site, FREQUENTLY_FIELDS));
    }

    saveSite() {
        if (this.form.valid) {
            let result = _.omit(this.form.value, ['fullAddress']);

            if (this.site && this.site.id) {
                result['id'] = this.site.id;
            }
            result = nullifyFields(result);
            for (let k of Object.keys(result)) {
                if (k.endsWith('phone') && result[k]) {
                    result[k] = `+1${result[k]}`;
                }
            }
            if (!this.wasCreated) {
                this.handleObservable(this.siteRepo.create(result));
            } else {
                result['id'] = this.currentSiteCameraDialog.id;
                this.handleObservable(this.siteRepo.patch(result));
            }
        }
    }

    private handleObservable(obs: Observable<ISite>) {
        obs.subscribe(
            res => {
                this.currentSiteCameraDialog = res;
                this.navbarService.addSiteItem(res);
                this.siteRepo.reload();
                this.setCameraStepValues();

                if (!this.wasCreated) {
                    this.wasCreated = true;
                    this.notificationService.notify(['Success! Location was created correctly'], 5000);
                } else {
                    this.notificationService.notify(['Success! Location was saved correctly'], 5000);
                }
                this.canNext = true;
            },
            err => {
                this.canNext = false;
                this.notificationService.notify(['Error! You have some errors in the form'], 5000);
                this.handleHttpErrors(err);
            }
        );
    }

    private handleHttpErrors(err: HttpErrorResponse) {
        if (err.status === 400) {
            this.httpErrors = err.error;
        }
    }

    private initializeFormCameraDialog() {
        const auxCamera: ICamera = this.data.editData ?
            this.data.editData.camera : null;
        let startPosition = null;

        if (auxCamera) {
            this.cameraToEditCameraDialog = auxCamera;
        }
        if (this.data.editData && this.data.editData.site) {
            this.currentSiteCameraDialog = this.data.editData.site;
            this.sitesListCameraDialog = [this.currentSiteCameraDialog];
        }
        if (this.data.site) {
            this.currentSiteCameraDialog = this.data.site;
            this.sitesListCameraDialog = [this.currentSiteCameraDialog];
        }
        if (this.currentSiteCameraDialog || auxCamera) {
            startPosition = positionFactory(
                auxCamera ? auxCamera.lat : this.currentSiteCameraDialog.lat,
                auxCamera ? auxCamera.lon : this.currentSiteCameraDialog.lon
            );
        }
        this.formCameraDialog = this.formBuilder.group({
            idCameraDialog: [auxCamera ? auxCamera.id : null],
            latCameraDialog: [
                startPosition ? startPosition.lat : 0,
                Validators.required],
            lonCameraDialog: [
                startPosition ? startPosition.lng : 0,
                Validators.required],
            locationCameraDialog: [
                auxCamera ? auxCamera.location : '',
                Validators.required],
            nameCameraDialog: [
                auxCamera ? auxCamera.name : '',
                Validators.required
            ],
            // distance_ftCameraDialog: [
            //     auxCamera ? auxCamera.distance_ft : null,
            //     Validators.pattern(/^[0-9\.]*$/)],
            // height_ftCameraDialog: [
            //     auxCamera ? auxCamera.height_ft : null,
            //     Validators.pattern(/^[0-9\.]*$/)],
            // fovCameraDialog: [
            //     auxCamera ? auxCamera.fov : null,
            //     Validators.pattern(/^[0-9\.]*$/)],
            // resolutionCameraDialog: [auxCamera ? auxCamera.resolution : null],
            // modelCameraDialog: [auxCamera ? auxCamera.model : null],
            makeCameraDialog: [auxCamera ? auxCamera.make : null]
        });

    }

    positionLoadedCameraDialog(event: { position: IPosition }) {
        if (!this.currentSiteCameraDialog) {
            this.currentSiteCameraDialog = emptySiteFactory();
        }
        this.currentSiteCameraDialog.lat = event.position.lat;
        this.currentSiteCameraDialog.lon = event.position.lng;
    }

    selectionChange(event) {
        this.currentSiteCameraDialog = this.sitesListCameraDialog.filter(s => s.id === event.value)[0];
    }

    cameraDragEnd(position: IPosition) {
        this.formCameraDialog.patchValue({
            latCameraDialog: position.lat,
            lonCameraDialog: position.lng
        });
    }

    dragEnd(event: { position: IPosition, address: string }) {
        this.formCameraDialog.patchValue({
            latCameraDialog: event.position.lat,
            lonCameraDialog: event.position.lng
        });
    }

    finishStep() {
        this.stepper.selectedIndex = this.stepper._steps.length - 1;
    }

    saveCamera() {
        if (this.formCameraDialog.valid) {
            let result = this.formCameraDialog.value;
            let newCamera = {
                id: result['idCameraDialog'],
                name: result['nameCameraDialog'],
                location: result['locationCameraDialog'],
                lat: result['latCameraDialog'],
                lon: result['lonCameraDialog'],
                distance_ft: result['distance_ftCameraDialog'],
                fov: result['fovCameraDialog'],
                height_ft: result['height_ftCameraDialog'],
                make: result['makeCameraDialog'],
                model: result['modelCameraDialog'],
                resolution: result['resolutionCameraDialog']
            };
            if (this.cameraToEditCameraDialog) {
                newCamera['id'] = this.cameraToEditCameraDialog.id;
            }
            if (this.formCameraDialog.controls.locationCameraDialog.disabled) {
                newCamera['location'] = this.formCameraDialog.controls.locationCameraDialog.value;
            }
            if (result.latCameraDialog === 0 || result.lonCameraDialog === 0) {
                newCamera.lat = this.currentSiteCameraDialog.lat;
                newCamera.lon = this.currentSiteCameraDialog.lon;
            }
            let camExist = this.currentSiteCameraDialog.cameras.some(cam => {
                return cam.lat == newCamera.lat && cam.lon == newCamera.lon
            });

            if (this.isNewCameraToAdd) {
                if (camExist) {
                    this.dialog.open(CameraConfirmationDialogComponent, {
                        height: '250px',
                        width: '400px',
                        data: { existCamera: true, cameraName: newCamera.name }
                    });
                } else {
                    this.saveNewCamera(newCamera);
                    this.isNewCameraToAdd = false;
                    this.enableAddNewCameraBtn = true;
                }
            } else {

                let updateCamera = this.currentCamera;
                updateCamera['name'] = newCamera.name;
                updateCamera['make'] = newCamera.make;
                updateCamera['lat'] = newCamera.lat;
                updateCamera['lon'] = newCamera.lon;
                this.updateCamera(updateCamera);
                this.enableAddNewCameraBtn = true;
            }
        }
    }

    clear() {
        this.initializeFormCameraDialog();
        this.isNewCameraToAdd = true;
        this.cameraToEditCameraDialog = null;
        this.cameraMapIconNumber = 4;
        if(this.currentSiteCameraDialog.cameras && this.currentSiteCameraDialog.cameras.length == 0){
            this.enableAddNewCameraBtn = false;
        }        
        this.siteMapCameraDialog.addDefaultCamMarker(this.currentSiteCameraDialog);
        this.formCameraDialog.controls['locationCameraDialog'].setValue(this.currentSiteCameraDialog.id);
    }

    stepChange(event: any) {
        switch (event.selectedIndex) {
            case 0: this.stepName = 'Get Started'; break;
            case 1: this.stepName = 'Register Location'; break;
            case 2: this.stepName = 'Register Camera(s)'; break;
            case 3: this.stepName = 'Finish'; break;
        }
    }

    saveNewCamera(newCamera: any) {
        this.cameraRepo.create(_.omitBy(newCamera, _.isNull) as ICamera)
            .subscribe(res => {
                this.currentCamera = res;
                const auxSite = _.find(this.sitesListCameraDialog, s => s.id === res.location);
                this.navbarService.updateCamerasCountBadge(res.id, auxSite.cameras.length + 1);

                this.siteRepo.reload().subscribe(() => {
                    // this.cameraToEditCameraDialog = null;
                    // this.currentCamera = null;
                });
                // this.initializeFormCameraDialog();
                // this.formCameraDialog.controls['locationCameraDialog'].setValue(this.currentSiteCameraDialog.id);

                this.dialog.open(CameraConfirmationDialogComponent, {
                    height: '250px',
                    width: '400px',
                    disableClose: true,
                    data: { existCamera: false, cameraName: newCamera.name }
                });
                this.currentSiteCameraDialog.cameras.push(res);
                this.siteMapCameraDialog.computePosition(this.currentSiteCameraDialog);
                // this.siteMapCameraDialog.addDefaultCamMarker(this.currentSiteCameraDialog);
                this.enableButtonFinish = true;
            },
                err => {
                    this.httpErrorDialog = err.statusText;
                }
            );
    }

    updateCamera(newCamera: any) {
        this.cameraRepo.patch(_.omitBy(newCamera, _.isNull) as ICamera)
            .subscribe(res => {

                let camExistIndex = this.currentSiteCameraDialog.cameras.findIndex(cam => {
                    return cam.id == newCamera.id
                });

                this.currentSiteCameraDialog.cameras[camExistIndex] = newCamera;

                // const auxSite = _.find(this.sitesListCameraDialog, s => s.id === res.location);
                // this.navbarService.updateCamerasCountBadge(res.id, auxSite.cameras.length + 1);
                this.siteRepo.reload().subscribe(site => {
                    let siteIndex = site.findIndex(s => { return s.id == this.currentSiteCameraDialog.id });
                    this.currentSiteCameraDialog.cameras = site[siteIndex].cameras;
                    // this.cameraToEditCameraDialog = null;
                    // this.currentCamera = null;
                });
                // this.initializeFormCameraDialog();
                // this.formCameraDialog.controls['locationCameraDialog'].setValue(this.currentSiteCameraDialog.id);

                // this.dialog.open(CameraConfirmationDialogComponent, {
                //     height: '250px',
                //     width: '400px',
                //     disableClose: true,
                //     data: { existCamera: false, cameraName: newCamera.name }
                // });
                // this.currentSiteCameraDialog.cameras.push(res);

                this.siteMapCameraDialog.computePosition(this.currentSiteCameraDialog);
                // this.siteMapCameraDialog.addDefaultCamMarker(this.currentSiteCameraDialog);
                // this.enableButtonFinish = true;
            },
                err => {
                    this.httpErrorDialog = err.statusText;
                }
            );
    }

    learnMore() {
        let redirectWindow = window.open('https://www.fusus.com/how-it-works', '_blank');
        redirectWindow.location;
    }

    share() {
        this.dialog.open(ShareVideoDialogComponent, {
            maxWidth: '100vw',
            maxHeight: '100vh',
            height: '100%',
            width: '100%',
            disableClose: true,
            data: {
                site: this.currentSiteCameraDialog,
                step: 'root',
                user: this.user
            }
        });
    }

    selectedCameraByClick(camera: ICamera) {
        console.log('-------------------------------------------------');
        console.log(camera);
        this.cameraToEditCameraDialog = camera;
        this.currentCamera = camera;
        this.cameraMapIconNumber = 0;
        this.isNewCameraToAdd = false;
        this.formCameraDialog.controls['latCameraDialog'].setValue(camera.lat);
        this.formCameraDialog.controls['lonCameraDialog'].setValue(camera.lon);
        this.formCameraDialog.controls['nameCameraDialog'].setValue(camera.name);
        this.formCameraDialog.controls['makeCameraDialog'].setValue(camera.make);
        console.log(this.currentCamera);
        console.log(this.cameraToEditCameraDialog);
        console.log('-------------------------------------------------');
    }
}
