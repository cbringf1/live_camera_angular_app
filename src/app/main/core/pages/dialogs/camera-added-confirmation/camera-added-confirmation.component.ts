import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'camera-added-confirmation',
    templateUrl: './camera-added-confirmation.component.html',
    styleUrls: ['./camera-added-confirmation.component.scss']
})
export class CameraConfirmationDialogComponent implements OnInit {
    existCamera: boolean;
    cameraName: string;

    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any,
        public dialogRef: MatDialogRef<CameraConfirmationDialogComponent>) {
        this.existCamera = this.data.existCamera;
        this.cameraName = this.data.cameraName;
    }

    ngOnInit() { }
}