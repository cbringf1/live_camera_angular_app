import { Component, OnInit, Inject } from '@angular/core';
import { EvidenceRepository } from 'app/main/core/repositories/evidence/evidence.repository';
import { ISite } from 'app/main/core/repositories/site-repo/site.model';
import { SiteRepository } from 'app/main/core/repositories/site-repo/site.repository';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
	selector: 'evidence-form',
	templateUrl: './evidence-form.dialog.component.html',
	styleUrls: ['./evidence-form.dialog.component.scss']
})
export class EvidenceFormDialogComponent implements OnInit {
	sitesList: ISite[];
	form: FormGroup;
	httpError: string;

	constructor(
		private formBuilder: FormBuilder,
		private evidenceRepo: EvidenceRepository,
		private siteRepo: SiteRepository,
		private dialogRef: MatDialogRef<EvidenceFormDialogComponent>,
		@Inject(MAT_DIALOG_DATA) private data: any
	) { }

	ngOnInit() {
		this.form = this.formBuilder.group({
			name: [
				this.data.evidence ? this.data.evidence.name : '',
				Validators.required
			],
			location_id: [
				this.data.evidence ? this.data.evidence.id : '',
				Validators.required
			],
			date: [
				this.data.evidence ? this.data.evidence.date : '',
				Validators.required
			]
		});
		this.siteRepo.getAll().subscribe(res => this.sitesList = res);
	}	

	saveEvidence() {
		if (this.form.valid) {
			if (typeof this.form.value.date !== 'string') {
				this.form.value.date = this.form.value.date.format();
			}
			if (this.data.evidence) {
				this.form.value.id = this.data.evidence.id;
				this.evidenceRepo.update(this.form.value)
					.subscribe(res => this.dialogRef.close(res));
			}
			else {
				this.evidenceRepo.create(this.form.value)
					.subscribe(res => this.dialogRef.close(res));
			}
		}
	}
}