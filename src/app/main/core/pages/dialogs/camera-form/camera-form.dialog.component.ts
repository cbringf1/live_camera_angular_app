import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { ISite } from 'app/main/core/repositories/site-repo/site.model';
import { SiteRepository } from 'app/main/core/repositories/site-repo/site.repository';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GeolocationService } from 'app/main/core/services/geo/geolocation.service';
import { ICamera } from 'app/main/core/repositories/camera-repo/camera.model';
import { positionFactory } from 'app/main/core/factories/position.factory';
import * as _ from 'lodash';
import { IPosition } from 'app/main/core/services/geo/position.model';
import { emptySiteFactory } from 'app/main/core/factories/empty-site.factory';
import { SiteMapComponent } from 'app/main/core/components/site-map/site-map.component';
import { CameraRepository } from 'app/main/core/repositories/camera-repo/camera.repository';
import { resolutions } from './resolutions';
import { SiteInfoService } from 'app/main/core/services/utils/resource-loader/site-info.service';
import { ISiteowner } from 'app/main/core/repositories/siteowner/siteowner.model';

@Component({
	selector: 'camera-form',
	templateUrl: './camera-form.dialog.component.html',
	styleUrls: ['./camera-form.dialog.component.scss']
})
export class CameraFormDialogComponent implements OnInit {
	sitesList: ISite[] = [];
	form: FormGroup;
	currentSite: ISite;
	cameraCoords: IPosition;
	currentPosition: IPosition;
	cameraToEdit: ICamera;
	disabledCameras: ICamera[] = [];
	httpError: string;
	@ViewChild('siteMap') siteMap: SiteMapComponent;
    private resolutionList: any[];
    title: string;
    siteowner: ISiteowner;

	constructor(
		@Inject(MAT_DIALOG_DATA) private data: any,
		private formBuilder: FormBuilder,
		public dialogRef: MatDialogRef<CameraFormDialogComponent>,
		private geolocationService: GeolocationService,
        private cameraRepo: CameraRepository,
        private siteInfoService: SiteInfoService
    ) {
		if (data.sitesList) {
			data.sitesList.subscribe(res => this.sitesList = res);
		}
		this.resolutionList = resolutions.map(r => new Object({ 'id': r[0], 'value': r[1] }));
	}

    ngOnInit() {
        this.title = this.data.title;

        // this.siteInfoService.getAllSiteInfo().subscribe((info: ISiteowner) => {
        //     this.siteowner = info;
        //     this.siteMap.computePosition(null, this.siteowner);
        //
        //     if (this.data && this.data.site) {
        //         this.currentSite = this.data.site;
        //         this.siteMap.computePosition(this.currentSite);
        //     }
        //     else {
        //         this.currentSite = emptySiteFactory();
        //         this.siteMap.computePosition();
        //     }
        // });
        
		this.initializeForm();

		// if (!this.data.editData) {
		// 	this.geolocationService.getPosition()
		// 		.subscribe(p => {
		// 			this.form.patchValue({
		// 				lat: p.lat,
		// 				lon: p.lng
		// 			});
		// 		});
		// }
	}

	private initializeForm() {
		const auxCamera: ICamera = this.data.editData ?
			this.data.editData.camera : null;
		let startPosition = null;

		if (auxCamera) {
			this.cameraToEdit = auxCamera;
		}
		if (this.data.editData && this.data.editData.site) {
			this.currentSite = this.data.editData.site;
			this.sitesList = [this.currentSite];
		}
		if (this.data.site) {
			this.currentSite = this.data.site;
			this.sitesList = [this.currentSite];
		}
		if (this.currentSite || auxCamera) {
			startPosition = positionFactory(
				auxCamera ? auxCamera.lat : this.currentSite.lat,
				auxCamera ? auxCamera.lon : this.currentSite.lon
			);
		}
		this.form = this.formBuilder.group({
			id: [auxCamera ? auxCamera.id : null],
			lat: [
				startPosition ? startPosition.lat : 0,
				Validators.required],
			lon: [
				startPosition ? startPosition.lng : 0,
				Validators.required],
			location: [
				auxCamera ? auxCamera.location : '',
				Validators.required],
			name: [
				auxCamera ? auxCamera.name : '',
				Validators.required
			],
			// distance_ft: [
			// 	auxCamera ? auxCamera.distance_ft : null,
			// 	Validators.pattern(/^[0-9\.]*$/)],
			// height_ft: [
			// 	auxCamera ? auxCamera.height_ft : null,
			// 	Validators.pattern(/^[0-9\.]*$/)],
			// fov: [
			// 	auxCamera ? auxCamera.fov : null,
			// 	Validators.pattern(/^[0-9\.]*$/)],
			// resolution: [auxCamera ? auxCamera.resolution : null],
			// model: [auxCamera ? auxCamera.model : null],
			make: [auxCamera ? auxCamera.make : null]
		});
	}

	positionLoaded(event: { position: IPosition }) {
		if (!this.currentSite) {
			this.currentSite = emptySiteFactory();
		}
		this.currentSite.lat = event.position.lat;
		this.currentSite.lon = event.position.lng;
	}

	selectionChange(event) {
		this.currentSite = this.sitesList.filter(s => s.id === event.value)[0];
	}

	cameraDragEnd(position: IPosition) {
		this.form.patchValue({
			lat: position.lat,
			lon: position.lng
		});
	}

	dragEnd(event: { position: IPosition, address: string }) {
		this.form.patchValue({
			lat: event.position.lat,
			lon: event.position.lng
		});
	}

	saveCamera() {
		if (this.form.valid) {
			let result = this.form.value;

			if (this.cameraToEdit) {
				result['id'] = this.cameraToEdit.id;
			}
			if (this.form.controls.location.disabled) {
				result['location'] = this.form.controls.location.value;
			}
			if(result.lat === 0 || result.lon === 0) {
				result.lat = this.currentSite.lat;
				result.lon = this.currentSite.lon;
			}
			if (this.data.editData) {
				this.cameraRepo.patch(_.omitBy(result, _.isNull) as ICamera)
					.subscribe(
						res => this.dialogRef.close(res),
						err => this.httpError = err.statusText
					);
			}
			else {
				this.cameraRepo.create(_.omitBy(result, _.isNull) as ICamera)
					.subscribe(
						res => this.dialogRef.close(res),
						err => this.httpError = err.statusText
					);
			}
			//this.dialogRef.close(_.omitBy(result, _.isNull));
		}
	}
}