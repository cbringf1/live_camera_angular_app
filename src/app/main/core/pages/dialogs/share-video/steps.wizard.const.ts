export const WizardSteps = {
    root: 'root',
    compatibilityCheck: 'compatibility_check',
    purchaseCore: 'purchase_core',
    preconfiguratorDvrNvr: 'preconfigurator_dvr',
    preconfiguratorCameras: 'preconfigurator_cameras',
    finish: 'finish'
};