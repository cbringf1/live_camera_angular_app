import { Component, OnInit, Inject, ViewChild, Renderer2, AfterViewInit, ChangeDetectorRef, ElementRef } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatStepper, MatDialog } from '@angular/material';
import { ISite } from 'app/main/core/repositories/site-repo/site.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RecordersRepository } from 'app/main/core/repositories/recorders/recorders.repository';
import { DvrNvr, CompatibilityCheckResponse, ContactInfo } from 'app/main/core/repositories/recorders/dnr-nvr.model';
import { WizardCamerasDataSource } from './table-camera-data-source';
import { ICamera } from 'app/main/core/repositories/camera-repo/camera.model';
import { ICameraRecorder } from 'app/main/core/repositories/camera-recorders/camera-recorders.model';
import { IFususcore } from 'app/main/core/repositories/fususcore/fususcore.model';
import { SiteRepository } from 'app/main/core/repositories/site-repo/site.repository';
import { NotificationService } from 'app/main/core/services/utils/notification.service';
import { UserService } from 'app/main/core/services/auth/user.service';
import { LocalAuthService } from 'app/main/core/services/auth/local-auth.service';
import { IUserModel } from 'app/main/core/services/auth/user.model';
import { WizardSteps } from './steps.wizard.const';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { defaults } from '../../../config/defaults';
import { CameraRepository } from 'app/main/core/repositories/camera-repo/camera.repository';
import { CameraRecorderRepository } from 'app/main/core/repositories/camera-recorders/camera-recorders.repository';
import { environment } from '../../../../../../environments/environment';
import { PaymentConfirmationDialogComponent } from '../payment-confirmation/payment-confirmation.component';
import * as moment from 'moment';

declare var $zoho;
@Component({
    selector: 'share-video',
    templateUrl: './share-video.component.html',
    styleUrls: ['./share-video.component.scss']
})
export class ShareVideoDialogComponent implements OnInit, AfterViewInit {
    @ViewChild('stepper') stepper: MatStepper;
    @ViewChild('stripeContainer') stripeContainer: ElementRef;
    site: ISite;
    fusucore: IFususcore;
    stepName: string;
    enableButtonNext: boolean;
    dvrNvrInformationForm: FormGroup;
    compatibilityCheckForm: FormGroup;
    fususcoreSelectionForm: FormGroup;
    contactForm: FormGroup;
    paymentForm: FormGroup;
    couponForm: FormGroup;
    compatibilityCheckResponse: CompatibilityCheckResponse;
    isNewDvrNvr: boolean;
    currentDvrNvr: DvrNvr;
    enableAddNewBtn: boolean;
    dataSource: WizardCamerasDataSource;
    initialData: ICamera[];
    displayedColumns: string[];
    enableButtonFinish: boolean;
    systemCompatible: boolean;
    user: IUserModel;
    step: string;
    tokenFormUrl: SafeResourceUrl;
    tokenForm: FormGroup;
    cardNumber: any;
    cardExpiry: any;
    cardCvc: any;
    stripe: any;
    error: string;
    token: string;
    elements: any;
    form: any;
    errorvisible: boolean = false;
    error_message = "";
    paymentDone: boolean;
    summaryDescription: string;
    paymentMethod: string;
    datePaid: string;
    submitPaymentBtn: boolean;
    cardCvcCompleted: boolean;
    cardNumberCompleted: boolean;
    cardExpiryCompleted: boolean;
    invalidCoupon: boolean;
    couponResId: string;
    couponResFinalAmount: number;
    couponUsed: boolean;
    initAmount: number;
    disabledCardInfo: boolean;

    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any,
        public dialogRef: MatDialogRef<ShareVideoDialogComponent>,
        private formBuilder: FormBuilder,
        private recordersRepository: RecordersRepository,
        private siteRepository: SiteRepository,
        private notificationService: NotificationService,
        private userService: UserService,
        private localAuthService: LocalAuthService,
        private sanitizer: DomSanitizer,
        private cameraRepository: CameraRepository,
        private cameraRecorderRepository: CameraRecorderRepository,
        private cd: ChangeDetectorRef,
        private dialog: MatDialog
    ) {
        this.user = this.data.user;
        this.site = this.data.site;
        this.step = this.data.step;
        this.enableButtonNext = false;
        this.isNewDvrNvr = true;
        this.enableAddNewBtn = false;
        this.enableButtonFinish = false;
        this.systemCompatible = false;
        this.paymentDone = false;
        this.submitPaymentBtn = false;
        this.invalidCoupon = false;
        this.disabledCardInfo = false;
        this.compatibilityCheckResponse = null;
        this.couponUsed = false;
        this.dataSource = new WizardCamerasDataSource(this.site.cameras);
        this.displayedColumns = ['index', 'type', 'name', 'make', 'model', 'host', 'username', 'password', 'actions'];
        this.tokenFormUrl = this.sanitizer.bypassSecurityTrustResourceUrl(defaults.tokenPurchaseFormUrl);
        this.fusucore = { 'name': 'fūsusCORE™', 'price': '$1000' };

        this.userService.getUserFormApi((this.localAuthService.getUser() as IUserModel).id).subscribe(user => {
            this.user = user
            this.selectUserCore(this.user.amount_cameras_share);
            this.paymentDone = this.user.is_core_purchased;
            this.checkPayment();
        });
    }

    ngOnInit() {
        this.setCamerasType();
        this.stepName = 'fūsusCORE™ Upgrade';
        this.initializeDvrNvrInformationForm();
        this.initializeCompatibilityCheckForm();
        this.initializeFususcoreSelection();
        this.initializeContactForm();
        this.initializePaymentForm();
        this.initializeCouponForm();
        // this.initializeTokenForm();
        this.goToStep(this.step);
    }

    ngAfterViewInit() {
        if (!this.paymentDone) {
            this.form = this.stripeContainer.nativeElement.querySelector('form');

            this.stripe = Stripe(environment.stripePublishKey);

            this.elements = this.stripe.elements({ locale: 'en' });

            this.cardNumber = this.elements.create('cardNumber');
            this.cardNumber.mount('#card-number');

            this.cardExpiry = this.elements.create('cardExpiry');
            this.cardExpiry.mount('#card-expiry');

            this.cardCvc = this.elements.create('cardCvc');
            this.cardCvc.mount('#card-cvc');

            this.cardNumber.on('change', (event: any) => { this.cardOnChange(event); });

            this.cardExpiry.on('change', (event: any) => { this.cardOnChange(event); });

            this.cardCvc.on('change', (event: any) => { this.cardOnChange(event); });
        }
    }

    checkPayment() {
        console.log("is_core_purchased", this.user.is_core_purchased);
        console.log("payments", this.user.payments);

        if (this.paymentDone) {
            if (this.user.payments) {
                // this.showPaymentConfirmationDialog(true);
                this.fusucore.price = '$' + this.user.payments[0].amount;
                this.initAmount = this.user.payments[0].amount;
                this.summaryDescription = this.user.payments[0].description;
                this.paymentMethod = this.user.payments[0].payment_method;
                this.datePaid = moment(this.user.payments[0].created_datetime).format('LL');
            }
        }
    }

    setCamerasType() {
        this.site.cameras.forEach(c => c.type = 'CAMERA');
    }

    showChat() {
        $zoho.salesiq.floatbutton.visible("show");
    }

    hideChat() {
        $zoho.salesiq.floatbutton.visible("hide");
    }

    removeRow(index: number) {
        this.site.cameras.splice(index, 1);
        this.dataSource.update(this.site.cameras);
    }

    stepChange(event: any) {
        switch (event.selectedIndex) {
            case 0: {
                this.stepName = 'fūsusCORE™ Upgrade';
                this.hideChat();
            } break;
            case 1: {
                this.stepName = 'Compatibility Check';
                this.updateUser('agree_share_video', true);
                this.updateUser('share_video_wizard_step', WizardSteps.compatibilityCheck);
                this.hideChat();
            } break;
            case 2: {
                this.stepName = 'Purchase CORE';
                this.checkPayment();
                this.updateUser('share_video_wizard_step', WizardSteps.purchaseCore);
                this.showChat();
            } break;
            // case 3: {
            //     this.stepName = 'Preconfigurator';
            //     this.updateUser('share_video_wizard_step', WizardSteps.preconfiguratorDvrNvr);
            // } break;
            case 3: {
                this.stepName = 'Pre-Configurator';
                this.updateUser('share_video_wizard_step', WizardSteps.preconfiguratorCameras);
                this.hideChat();
            } break;
            case 4: {
                this.stepName = 'Finish';
                this.updateUser('share_video_wizard_step', WizardSteps.finish);
                this.hideChat();
            } break;
        }
    }

    goToStep(step: string) {
        switch (step) {
            case WizardSteps.root: this.stepper.selectedIndex = 0; break;
            case WizardSteps.compatibilityCheck: this.stepper.selectedIndex = 1; break;
            case WizardSteps.purchaseCore: this.stepper.selectedIndex = 2; break;
            // case WizardSteps.preconfiguratorDvrNvr: this.stepper.selectedIndex = 3; break;
            case WizardSteps.preconfiguratorCameras: this.stepper.selectedIndex = 3; break;
            case WizardSteps.finish: this.stepper.selectedIndex = 4; break;
        }
    }

    update(camera: ICamera, event: { info: string, field: string }) {
        if (event != null) {
            const copy = this.dataSource.data().slice();
            camera[event.field] = event.info;
            this.dataSource.update(copy);
        }
    }

    enablePaymentBtn() {
        if (this.couponUsed) {
            if (this.couponResFinalAmount <= 0) {
                return true;
            }
        }
        return this.paymentForm.valid && this.submitPaymentBtn;
    }

    useCoupon() {
        console.log({
            coupon_code: this.couponForm.controls['couponCode'].value,
            amount: this.initAmount
        });

        this.invalidCoupon = false;
        if (this.couponForm.controls['couponCode'].value) {

            this.userService.sendCoupon({
                coupon_code: this.couponForm.controls['couponCode'].value,
                amount: this.initAmount
            }).subscribe(res => {
                this.couponResId = res.coupon_redem_id;
                this.couponResFinalAmount = res.final_amount as number;

                if (this.couponResFinalAmount > 0) {
                    this.fusucore.price = '$' + this.couponResFinalAmount;
                } else {
                    this.fusucore.price = '$0';
                }
                this.couponUsed = true;
                this.couponForm.controls['couponCode'].setValue('');
                this.couponForm.controls['couponCode'].disable();
                this.disabledCardInfo = this.couponResFinalAmount <= 0;
                this.invalidCoupon = false;

                if (this.disabledCardInfo) {
                    this.paymentForm.controls['cardName'].setValue(null);
                    this.paymentForm.controls['cardEmail'].setValue(null);
                    this.couponForm.controls['couponCode'].setValue(null);

                    this.paymentForm.controls['cardEmail'].disable();
                    this.paymentForm.controls['cardName'].disable();
                    this.couponForm.controls['couponCode'].disable();
                }
            }, err => {
                console.log(err);
                this.invalidCoupon = true;
                this.couponUsed = false;
                this.couponResId = null;
                this.disabledCardInfo = false;
                this.couponResFinalAmount = null;
            });
        }
    }

    addNewCamera() {
        let camera: ICamera = {
            is_rtsp_supported: false,
            lat: this.site.lat + (Math.random() * ((-0.00005 - 0.00005) +  0.00005)),
            location: this.site.id,
            lon: this.site.lon + (Math.random() * ((-0.00005 - 0.00005) +  0.00005)),
            name: 'new',
            password: null,
            username: null,
            streamType: null,
            imageStreamRefreshTime: null,
            liveUrls: null,
            publicToken: null,
            type: 'CAMERA'
        };
        this.site.cameras.push(camera);
        this.dataSource.update(this.site.cameras);     
        
        console.log((Math.random() * ((-0.00005 - 0.00005) +  0.00005)));
        
    }

    duplicate(index: number) {
        let camera: ICamera = {
            is_rtsp_supported: false,
            lat: this.site.lat + (Math.random() * ((-0.00005 - 0.00005) +  0.00005)),
            location: this.site.cameras[index].location,
            lon: this.site.lon + (Math.random() * ((-0.00005 - 0.00005) +  0.00005)),
            streamType: null,
            imageStreamRefreshTime: null,
            liveUrls: null,
            publicToken: null,
            type: this.site.cameras[index].type,
            name: this.site.cameras[index].name,
            make: this.site.cameras[index].make,
            model: this.site.cameras[index].model,
            host: this.site.cameras[index].host,
            username: this.site.cameras[index].username,
            password: this.site.cameras[index].password
        };
        this.site.cameras.push(camera);
        this.dataSource.update(this.site.cameras);
    }

    save() {
        if (this.dvrNvrInformationForm.valid) {
            let newDvrNvr = this.dvrNvrInformationForm.value;
            if (this.isNewDvrNvr) {
                this.recordersRepository
                    .createDvrNvr(newDvrNvr)
                    .subscribe((res: DvrNvr) => {
                        this.currentDvrNvr = res;
                        this.enableAddNewBtn = true;
                        this.enableButtonNext = true;
                        this.isNewDvrNvr = false;
                        this.notificationService.notify(['Success! DVR/NVR Information registered'], 4000);
                    }, () => {
                        this.isNewDvrNvr = true;
                    });
            } else {
                if (this.currentDvrNvr) {
                    newDvrNvr['id'] = this.currentDvrNvr.id;
                }
                this.recordersRepository.patch(newDvrNvr).subscribe((res: DvrNvr) => {
                    this.currentDvrNvr = res;
                    this.notificationService.notify(['Success! DVR/NVR Information updated'], 4000);
                });
            }
        }
    }

    check() {
        if (this.compatibilityCheckForm.valid) {
            let compatibilityCheck = this.compatibilityCheckForm.value;
            this.recordersRepository
                .compatibilityCheckFx(compatibilityCheck)
                .subscribe((res: CompatibilityCheckResponse) => {
                    console.log(res);
                    this.compatibilityCheckResponse = res;
                    this.systemCompatible = res.system_compatible;
                    this.enableButtonFinish = true;
                });
        }
    }

    selectCore() {
        let fususcoreSelection = this.fususcoreSelectionForm.value;

        let newUser = this.user;
        newUser['amount_cameras_share'] = fususcoreSelection['number_cameras'];

        this.userService.patchUser(this.user.id, newUser).subscribe(res => {
            this.user = res;
            this.selectUserCore(fususcoreSelection['number_cameras']);
        });
    }

    selectUserCore(numberCameras: number) {
        if (numberCameras >= 11) {
            console.log('FUSUCORE');
            this.fusucore.name = 'fūsusCORE™';
            this.fusucore.price = '$1000';
            this.initAmount = 1000;
        }
        else {
            console.log('FUSUCORE LITE');
            this.fusucore.name = 'fūsusCORE™ Lite';
            this.fusucore.price = '$100';
            this.initAmount = 100;
        }
    }

    saveCamerasInfo() {
        console.log("CAMERAS-TO-PATCH-BULK", this.site.cameras);

        console.log("CAMERAS-TO-PATCH-BULK", this.dataSource.data);

        this.cameraRecorderRepository.bulk(this.site.cameras).subscribe(res => {
            console.log("CAMERAS-PATCHED", res);
            this.siteRepository.reload();
        });
    }

    clear() {
        this.initializeDvrNvrInformationForm();
        this.isNewDvrNvr = true;
        this.enableAddNewBtn = false;
        this.currentDvrNvr = null;
    }

    private initializeDvrNvrInformationForm() {
        this.dvrNvrInformationForm = this.formBuilder.group({
            name: ['', Validators.required],
            location: [this.site.id, Validators.required],
            make: ['', Validators.required],
            model: ['', Validators.required],
            type: ['', Validators.required],
            host: ['', Validators.required],
            username: ['', Validators.required],
            password: ['', Validators.required],
            is_rtsp_supported: ['']
        });
    }

    private initializePaymentForm() {
        this.paymentForm = this.formBuilder.group({
            cardName: ['', Validators.required],
            cardEmail: ['', [Validators.required, Validators.email]]
        });
    }

    private initializeCouponForm() {
        this.couponForm = this.formBuilder.group({
            couponCode: ['']
        });
    }



    private initializeCompatibilityCheckForm() {
        this.compatibilityCheckForm = this.formBuilder.group({
            recorder_support_rtsp: ['', Validators.required],
            camera_type: ['', Validators.required],
            recorder_type: ['', Validators.required],
            contactFirstName: [''],
            contactLastName: [''],
            contactEmail: [''],
            contactPhone: ['']
        });
    }

    private initializeFususcoreSelection() {
        this.fususcoreSelectionForm = this.formBuilder.group({
            number_cameras: ['', Validators.required]
        });
    }

    updateUser(key: string, value: any) {
        this.user[key] = value;

        console.log("**********STEP CHANGED************");
        console.log("USER TO PATCH", this.user);


        this.userService.patchUser(this.user.id, this.user).subscribe(res => {
            console.log("USER TO PATCHED", res);

            console.log("**********STEP CHANGED************");
        });
    }

    private initializeContactForm() {
        this.contactForm = this.formBuilder.group({
            contactFirstName: ['', Validators.required],
            contactLastName: ['', Validators.required],
            contactEmail: ['', [Validators.required, Validators.email]],
            contactPhone: ['', Validators.required]
        });
    }

    finish() {
        this.stepper.selectedIndex = this.stepper._steps.length - 1;
    }

    checkAgain() {
        this.initializeCompatibilityCheckForm();
        this.initializeContactForm();
        this.initializeDvrNvrInformationForm();
        this.compatibilityCheckResponse = null;
        this.systemCompatible = false;
    }

    sendContactInfo() {
        if (this.contactForm.valid) {
            let contactInfo: ContactInfo = {
                first_name: this.contactForm.controls['contactFirstName'].value,
                last_name: this.contactForm.controls['contactLastName'].value,
                email: this.contactForm.controls['contactEmail'].value,
                phone: this.contactForm.controls['contactPhone'].value,
            };
            this.recordersRepository.sendContactInfo(contactInfo).subscribe(() => {
                this.notificationService.notify(['Success! Contact information sent'], 4000);
                this.initializeContactForm();
                this.finish();
            });
        }
    }

    completeWizard() {
        this.updateUser('is_wizard_completed', true);
    }

    backAtFinishStep() {
        this.stepper.selectedIndex = this.paymentDone ? 3 : 0;
    }

    initializeTokenForm() {
        this.tokenForm = this.formBuilder.group({
            token: ['']
        });
        window.addEventListener('message', (event) => {
            let token = JSON.parse(event.data);
            this.tokenForm.controls['token'].setValue(token);
        }, false);
    }

    cardOnChange(event: any) {

        console.log(this.cardCvc);
        console.log(this.cardExpiry);
        console.log(this.cardNumber);
        console.log(event);


        const savedErrors = {};
        // console.log("card.on change()");
        if (event.error) {
            //o error.classList.add('visible');
            this.errorvisible = true;
            savedErrors[0] = event.error.message;
            //o errorMessage.innerText = event.error.message;
            this.error_message = event.error.message;
            // console.log("displaying", this.error_message);
            this.submitPaymentBtn = false;
        } else {
            savedErrors[0] = null;

            // Loop over the saved errors and find the first one, if any.
            const nextError = Object.keys(savedErrors)
                .sort()
                .reduce((maybeFoundError, key) => {
                    return maybeFoundError || savedErrors[key];
                }, null);

            if (nextError) {
                // Now that they've fixed the current error, show another one.
                //o errorMessage.innerText = nextError;
                // console.log("displaying", nextError);
                this.error_message = nextError;
                this.submitPaymentBtn = false;
            } else {
                // The user fixed the last error; no more errors.
                //o error.classList.remove('visible');
                this.errorvisible = false;
                this.submitPaymentBtn = this.checkCardsComplete(event);
            }
        }
    }

    checkCardsComplete(event: any = null) {
        if (event) {
            switch (event.elementType) {
                case 'cardCvc': this.cardCvcCompleted = event.complete; break;
                case 'cardNumber': this.cardNumberCompleted = event.complete; break;
                case 'cardExpiry': this.cardExpiryCompleted = event.complete; break;
            }
        }
        return this.cardCvcCompleted && this.cardExpiryCompleted && this.cardNumberCompleted;
    }

    onSubmit() {
        // if (this.paymentForm.valid && this.checkCardsComplete()) {
        this.stripeContainer.nativeElement.classList.add('submitting');

        this.disableInputs();

        // const address1 = this.form.querySelector('#example5-address');
        // const city = this.form.querySelector('#example5-city');
        // const state = this.form.querySelector('#example5-state');
        // const zip = this.form.querySelector('#example5-zip');

        const additionalData = {
            name: this.paymentForm.controls['cardName'].value,
            email: this.paymentForm.controls['cardEmail'].value
        };
        console.log("onSubmit-additionalData", additionalData);

        if (this.couponUsed) {
            if (this.couponResFinalAmount <= 0) {
                this.sendStripeInfoToApi(additionalData.email, this.initAmount.toString());
            } else {
                this.stripe.createToken(this.cardNumber, this.cardCvc, this.cardExpiry, additionalData).then((result: any) => {
                    // Stop loading!
                    //o example.classList.remove('submitting');
                    this.stripeContainer.nativeElement.classList.remove('submitting');

                    if (result.token) {
                        // If we received a token, show the token ID.
                        //o example.querySelector('.token').innerText = result.token.id;
                        this.token = result.token.id;
                        this.stripeContainer.nativeElement.classList.add('submitted');
                        this.sendStripeInfoToApi(additionalData.email, this.initAmount.toString(), result.token);
                    } else {
                        // Otherwise, un-disable inputs.
                        this.enableInputs();
                    }
                });
            }
        } else {
            this.stripe.createToken(this.cardNumber, this.cardCvc, this.cardExpiry, additionalData).then((result: any) => {
                // Stop loading!
                //o example.classList.remove('submitting');
                this.stripeContainer.nativeElement.classList.remove('submitting');

                if (result.token) {
                    // If we received a token, show the token ID.
                    //o example.querySelector('.token').innerText = result.token.id;
                    this.token = result.token.id;
                    this.stripeContainer.nativeElement.classList.add('submitted');
                    this.sendStripeInfoToApi(additionalData.email, this.initAmount.toString(), result.token);
                } else {
                    // Otherwise, un-disable inputs.
                    this.enableInputs();
                }
            });
        }
        // }
    }

    enableInputs() {
        Array.prototype.forEach.call(
            this.form.querySelectorAll(
                "input[type='name'], input[type='email'], input[type='tel']"
            ),
            (input: any) => {
                input.removeAttribute('disabled');
            }
        );
    }

    disableInputs() {
        Array.prototype.forEach.call(
            this.form.querySelectorAll(
                "input[type='name'], input[type='email'], input[type='tel']"
            ),
            (input) => {
                input.setAttribute('disabled', 'true');
            }
        );
    }

    sendStripeInfoToApi(email: string, amount: string, stripeToken: any = null) {
        let info = {
            stripe_token: stripeToken ? stripeToken.id : null,
            amount: amount,
            receipt_email: email
        };
        if (this.couponUsed) {
            info['coupon_redem_id'] = this.couponResId;

            if (this.couponResFinalAmount <= 0) {
                delete info.stripe_token;
                delete info.receipt_email;
            }
        }
        console.log("sendStripeInfoToApi-info", info);

        this.userService.sendStripe(info).subscribe((res: any) => {
            console.log("sendStsendStripeInfoToApiripe-res", res);

            this.showPaymentConfirmationDialog(true);
            this.paymentDone = true;
            this.summaryDescription = res.description;
            this.paymentMethod = res.payment_method;
            this.datePaid = moment(res.created_datetime).format('LL');

        }, err => {
            console.log('sendStripeInfoToApi-error', err);

            this.showPaymentConfirmationDialog(false);
            this.enableInputs();
        });
    }

    showPaymentConfirmationDialog(result: boolean) {
        this.dialog.open(PaymentConfirmationDialogComponent, {
            height: '250px',
            width: '400px',
            disableClose: true,
            data: { result: result }
        });
    }
}