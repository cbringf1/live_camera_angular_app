import { Observable, BehaviorSubject } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { ICamera } from 'app/main/core/repositories/camera-repo/camera.model';

export class WizardCamerasDataSource extends DataSource<any> {
    private dataSubject = new BehaviorSubject<ICamera[]>([]);

    data() {
        return this.dataSubject.value;
    }

    update(data) {
        this.dataSubject.next(data);
    }

    constructor(data: ICamera[]) {
        super();
        this.dataSubject.next(data);
    }

    connect(): Observable<ICamera[]> {
        return this.dataSubject;
    }

    disconnect() { }
}