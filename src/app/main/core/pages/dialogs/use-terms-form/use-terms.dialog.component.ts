import { Component, ViewChild, ElementRef, OnInit } from "@angular/core";
import { MatDialogRef } from '@angular/material';
import { UserService } from 'app/main/core/services/auth/user.service';

@Component({
    selector: 'term-of-use',
    templateUrl: './use-terms.dialog.component.html',
    styleUrls: ['./use-terms.dialog.component.scss']
})
export class UseTermsDialogComponent implements OnInit {
    @ViewChild('dialog') dialog: ElementRef;
    scrollEnd: boolean;
    termsText: string;

    constructor(
        private dialogRef: MatDialogRef<UseTermsDialogComponent>,
        private userService: UserService
    ) {
        this.scrollEnd = false;
    }

    ngOnInit() {
        this.userService.getUseTerms()
            .subscribe(res => {
                console.log("ngOnInit", res);
                this.termsText = res;
            });

    }

    acceptTerms() {
        this.dialogRef.close({ accepted: true, wasRead: this.scrollEnd });
        // this.userService.acceptUseTerms()
        //     .subscribe(() => this.dialogRef.close({ accepted: true }));
    }

    cancel() {
        this.dialogRef.close({ accepted: false, wasRead: this.scrollEnd });
    }

    onScroll() {
        if (this.dialog.nativeElement.scrollTop === this.dialog.nativeElement.scrollHeight - this.dialog.nativeElement.clientHeight) {
            this.scrollEnd = true;
        }
    }
}