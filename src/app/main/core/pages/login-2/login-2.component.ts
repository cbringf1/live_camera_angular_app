import { Component, OnInit, ViewEncapsulation, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';
import { AuthStateService } from '../../services/auth/auth-state.service';
import { LocalAuthService } from '../../services/auth/local-auth.service';
import { NotificationService } from '../../services/utils/notification.service';
import { LogoLoaderService } from '../../services/utils/resource-loader/logo-loader.service';
import { Observable } from 'rxjs';
import { backgroundLoader, getBackgroundSubject, BG_KEY, LOGIN_LOGO_KEY } from '../../services/utils/resource-loader/dynamic-bg-loader';
import { defaults } from '../../config/defaults';
import { ISiteowner } from '../../repositories/siteowner/siteowner.model';
import { SiteInfoService } from '../../services/utils/resource-loader/site-info.service';

@Component({
    selector: 'login-2',
    templateUrl: './login-2.component.html',
    styleUrls: ['./login-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Login2Component implements OnInit {
    @ViewChild('video') video: ElementRef;

    loginForm: FormGroup;
    apiError: string;
    logoObservable: Observable<string>;
    backgroundImage: string;
    departmentName: string;
    showVideoTag: boolean;
    loginLogoImage: string;

	/**
	 * Constructor
	 *
	 * @param {FuseConfigService} _fuseConfigService
	 * @param {FormBuilder} _formBuilder
	 */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private router: Router,
        private localAuth: LocalAuthService,
        private notificationService: NotificationService,
        private logoLoaderService: LogoLoaderService,
        private siteInfoService: SiteInfoService) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        this.logoObservable = this.logoLoaderService.getLogo();
        this.backgroundImage = defaults.backgroundImage;
        this.loginLogoImage = defaults.loginLogoImage;
        this.showVideoTag = true;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

	/**
	 * On init
	 */
    ngOnInit(): void {
        this.initVideoEvents();
        
        if (this.localAuth.signingOut) {
            this.localAuth.signingOut = false;
            window.location.reload();
        }
        this.loginForm = this._formBuilder.group({
            username: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
        // backgroundLoader();

        getBackgroundSubject().subscribe(() => {
            const url = localStorage.getItem(BG_KEY);
            const loginUrl = localStorage.getItem(LOGIN_LOGO_KEY);

            this.backgroundImage = url || this.backgroundImage;
            this.loginLogoImage = loginUrl || this.loginLogoImage;
        });
        getBackgroundSubject().next();

        this.siteInfoService.getAllSiteInfo().subscribe((info: ISiteowner) => {
            this.departmentName = info.name;
        });

        const notif = this.notificationService.take(2);
        this.notificationService.notify(notif);
    }
    
    initVideoEvents() {
        this.video.nativeElement.addEventListener("playing", ()=> {
            this.showVideoTag = false;
        });
        this.video.nativeElement.addEventListener("pause", ()=> {
            this.showVideoTag = true;
        });
    }

    signInWithGoogle() {
        this.localAuth.googleSignIn();
    }

    signInWithEmail() {
        if (this.loginForm.valid) {
            this.localAuth.emailSignIn(this.loginForm.value)
                .subscribe(
                    res => console.log(res),
                    err => {
                        if (err.status === 400) {
                            this.apiError = err.error.non_field_errors[0];
                        }
                    }
                );
        }
    }
}
