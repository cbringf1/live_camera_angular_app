import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { LocalAuthService } from '../../services/auth/local-auth.service';
import { NotificationService } from '../../services/utils/notification.service';
import { Router } from '@angular/router';
import { backgroundLoader, getBackgroundSubject, BG_KEY, LOGIN_LOGO_KEY } from '../../services/utils/resource-loader/dynamic-bg-loader';
import { defaults } from '../../config/defaults';

@Component({
	selector: 'forgot-password-2',
	templateUrl: './forgot-password-2.component.html',
	styleUrls: ['./forgot-password-2.component.scss'],
	encapsulation: ViewEncapsulation.None,
	animations: fuseAnimations
})
export class ForgotPassword2Component implements OnInit {
	forgotPasswordForm: FormGroup;
    backgroundImage: string;
    loginLogoImage: string;
	
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
	constructor(
		private _fuseConfigService: FuseConfigService,
		private _formBuilder: FormBuilder,
		private localAuthService: LocalAuthService,
		private notificationService: NotificationService,
		private router: Router
	) {
		// Configure the layout
		this._fuseConfigService.config = {
			layout: {
				navbar: {
					hidden: true
				},
				toolbar: {
					hidden: true
				},
				footer: {
					hidden: true
				},
				sidepanel: {
					hidden: true
				}
			}
		};
        this.backgroundImage = defaults.backgroundImage;
        this.loginLogoImage = defaults.loginLogoImage;
	}

	// -----------------------------------------------------------------------------------------------------
	// @ Lifecycle hooks
	// -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
	ngOnInit(): void {
		this.forgotPasswordForm = this._formBuilder.group({
			email: ['', [Validators.required, Validators.email]]
		});
		// backgroundLoader();

		getBackgroundSubject().subscribe(() => { 
            const url = localStorage.getItem(BG_KEY);
            const loginUrl = localStorage.getItem(LOGIN_LOGO_KEY);

            this.backgroundImage = url || this.backgroundImage;
            this.loginLogoImage = loginUrl || this.loginLogoImage;
		});
		getBackgroundSubject().next();
	}

	sendRecoverLink() {
		if (this.forgotPasswordForm.valid) {
			this.localAuthService.sendRecoverLink(this.forgotPasswordForm.get('email').value)
				.subscribe(res => {
					this.notificationService.push(`
						If the email was found in our database, an email was sent with instructions
					`);
					this.router.navigate(['/login']);
				},
				err => console.log(err));
		}
	}
}
