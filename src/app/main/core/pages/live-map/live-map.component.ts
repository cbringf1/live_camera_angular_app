import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { SiteRepository } from '../../repositories/site-repo/site.repository';
import { CameraRepository } from '../../repositories/camera-repo/camera.repository';
import { ISite } from '../../repositories/site-repo/site.model';
import * as _ from 'lodash';
import { NavbarService } from '../../services/ui/navbar.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map, filter, switchMap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { MapComponent } from '../../components/map/map.component';
import { positionFactory } from '../../factories/position.factory';
import { SiteCamerasComponent } from '../../components/site-cameras/site-cameras.component';
import { ICamera } from '../../repositories/camera-repo/camera.model';
import { Subscription } from 'rxjs';

@Component({
    selector: 'live-map',
    templateUrl: './live-map.component.html',
    styleUrls: ['./live-map.component.scss'],
    host: {
        class: 'page-layout simple right-sidebar inner-scroll'
    }
})
export class LiveMapComponent implements OnInit, OnDestroy {
    currentSite: ISite;
    selectedCamera: ICamera;
    sites: ISite[] = [];
    @ViewChild('map') map: MapComponent;
    @ViewChild('cameras') cameras: SiteCamerasComponent;
    private subscriptions: Subscription;
    private fullMapSub: Subscription;
    private siteMapSub: Subscription;

    constructor(
        private siteRepo: SiteRepository,
        private cameraRepo: CameraRepository,
        private navbarService: NavbarService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {
        this.subscriptions = new Subscription(() => console.log('Unsubscribing all'));
    }

    ngOnInit() {
        this.navbarService.getSiteItemClickObservable()
            .subscribe(s => {
                this.currentSite = s;
                this.map.setSite(s);
            });
    }

    private loadSites(sites: ISite[], path: string) {
        if (path !== '_full') {
            this.currentSite = _.find(sites, (s: ISite) => s.id === path);
            if (this.currentSite) {
                this.map.setSite(this.currentSite);
            }
        }
        this.sites = sites;
    }

    private setMap() {
        this.siteRepo.getAll()
            .pipe(switchMap(res => {
                return this.route.paramMap
                    .pipe(map(p => new Object({
                        data: res,
                        id: p.get('id')
                    })));
            }))
            .subscribe((res: any) => {
                this.sites = res.data;
                if (res.id === '_full') {
                    this.loadFullMap(res.data);
                }
                else {
                    const site = this.sites.filter(s => s.id === res.id);

                    if (site.length > 0) {
                        this.loadSiteMap(site[0]);
                    }
                    else {
                        this.location.go('/map/_full');
                        this.loadFullMap(res.data);
                    }

                }
            })
        //this.subscriptions.add(this.fullMapSub);
        //this.subscriptions.add(this.siteMapSub);
    }

    private loadFullMap(sites: ISite[]) {
        this.currentSite = null;
        if (sites.length > 0) {
            this.map.boundClusters(sites);
        }
        else {
            this.map.loadCurrentPosition();
        }
    }

    private loadSiteMap(site: ISite) {
        this.map.setSite(site);
        this.currentSite = site;
    }

    onMapReady() {
        this.setMap();
    }

    cameraUpdated(camera: ICamera) {
        this.siteRepo.reload().subscribe(sites => {
            const site = sites.filter(s => s.id === camera.location)[0];
            this.router.navigate([`/map/${site.id}`]);
        });
    }

    clusterClick(site: ISite) {
        this.currentSite = site;
        this.location.go(`/map/${site.id}`);
    }

    markerClick(site: ISite) {
        this.currentSite = site;
        this.location.go(`/map/${site.id}`);
        this.map.setSite(site);
    }

    currentSiteUpdated(site: ISite) {
        this.siteRepo.reload().subscribe(() => {
            this.currentSite = site;
            this.router.navigate([`/map/${site.id}`]);
            this.navbarService.patchNavigationItem(site.id, {
                title: site.name
            });
        });
    }

    currentSiteDeleted() {
        //this.currentSite = null;
        //this.subscriptions.unsubscribe();
        //this.router.navigate(['/']);
    }

    cameraDetails(event) {
        this.cameras.editCamera(event.camera);
    }

    deleteCamera(event) {
        this.cameras.deleteCamera(event);
    }

    // selectCamera(camera: ICamera) {
    // 	this.map.mouseOver(camera);
    // }

    ngOnDestroy() {
        this.subscriptions.unsubscribe();
        this.subscriptions.remove(this.fullMapSub);
        this.subscriptions.remove(this.siteMapSub);
    }
}