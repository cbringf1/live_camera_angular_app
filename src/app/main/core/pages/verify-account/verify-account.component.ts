import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { LocalAuthService } from '../../services/auth/local-auth.service';
import { switchMap, filter } from 'rxjs/operators';
import { FuseConfigService } from '@fuse/services/config.service';
import { of } from 'rxjs';
import { backgroundLoader, getBackgroundSubject, EMAIL_BG_KEY } from '../../services/utils/resource-loader/dynamic-bg-loader';
import { defaults } from '../../config/defaults';

@Component({
	selector: 'verify-account',
	templateUrl: './verify-account.component.html',
	styleUrls: ['./verify-account.component.scss']
})
export class VerifyAccountComponent implements OnInit {
	confirmEmail: string = '';
	status: number = 1;
    emailConfirmationBackgroundImage: string;

	constructor(
		private route: ActivatedRoute,
		private localAuth: LocalAuthService,
		private router: Router,
		private _fuseConfigService: FuseConfigService) {
		this._fuseConfigService.config = {
			layout: {
				navbar: {
					hidden: true
				},
				toolbar: {
					hidden: true
				},
				footer: {
					hidden: true
				},
				sidepanel: {
					hidden: true
				}
			}
		};
        this.emailConfirmationBackgroundImage = defaults.emailConfirmationBackgroundImage;
	}

	ngOnInit() {
		// backgroundLoader();

		getBackgroundSubject().subscribe(() => { 
			const url = localStorage.getItem(EMAIL_BG_KEY);
            this.emailConfirmationBackgroundImage = url || this.emailConfirmationBackgroundImage;

		});
		getBackgroundSubject().next();

		this.confirmEmail = localStorage.getItem('confirm-email');

		this.route.paramMap
			.pipe(switchMap(p => {
				const key = p.get('id');

				if (key === '_unconfirmed') {
					return of(null);
				}
				else {
					return this.localAuth.confirmAccount(key);
				}
			}))
			.subscribe(
				res => {
					if (res) {
						this.status = 2;
					}
				},
				err => {
					if (err.status === 404) {
						this.status = 3;
					}
				}
			);
	}
}