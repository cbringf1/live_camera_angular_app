import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject, of } from 'rxjs';
import { takeUntil, switchMap, filter } from 'rxjs/internal/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalAuthService } from '../../services/auth/local-auth.service';
import * as _ from 'lodash';
import { NotificationService } from '../../services/utils/notification.service';
import { backgroundLoader, getBackgroundSubject, BG_KEY } from '../../services/utils/resource-loader/dynamic-bg-loader';
import { defaults } from '../../config/defaults';

@Component({
	selector: 'reset-password-2',
	templateUrl: './reset-password-2.component.html',
	styleUrls: ['./reset-password-2.component.scss'],
	encapsulation: ViewEncapsulation.None,
	animations: fuseAnimations
})
export class ResetPassword2Component implements OnInit, OnDestroy {
	resetPasswordForm: FormGroup;
	backgroundImage: string;

	// Private
	private _unsubscribeAll: Subject<any>;
	private apiErrors: any = {};

	constructor(
		private _fuseConfigService: FuseConfigService,
		private _formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private localAuth: LocalAuthService,
		private notificationService: NotificationService,
		private router: Router
	) {
		// Configure the layout
		this._fuseConfigService.config = {
			layout: {
				navbar: {
					hidden: true
				},

				toolbar: {
					hidden: true
				},
				footer: {
					hidden: true
				},
				sidepanel: {
					hidden: true
				}
			}
		};

		// Set the private defaults
		this._unsubscribeAll = new Subject();
		this.backgroundImage = defaults.backgroundImage;
	}

	// -----------------------------------------------------------------------------------------------------
	// @ Lifecycle hooks
	// -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
	ngOnInit(): void {
		this.resetPasswordForm = this._formBuilder.group({
			password: ['', Validators.required],
			passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
		});
		// backgroundLoader();

		getBackgroundSubject().subscribe(() => { 
			const url = localStorage.getItem(BG_KEY);
			this.backgroundImage = url || this.backgroundImage;
		});
		getBackgroundSubject().next();

		// Update the validity of the 'passwordConfirm' field
		// when the 'password' field changes
		this.resetPasswordForm.get('password').valueChanges
			.pipe(takeUntil(this._unsubscribeAll))
			.subscribe(() => {
				this.resetPasswordForm.get('passwordConfirm').updateValueAndValidity();
			});
	}

    /**
     * On destroy
     */
	ngOnDestroy(): void {
		// Unsubscribe from all subscriptions
		this._unsubscribeAll.next();
		this._unsubscribeAll.complete();
	}

	resetPassword() {
		this.route.paramMap
			.pipe(filter(() => this.resetPasswordForm.valid))
			.pipe(switchMap(p => {
				let data = _.omit(this.resetPasswordForm.value, ['email']);
				const tokens = atob(p.get('token')).split(':');

				return this.localAuth.resetPassword({
					uid: tokens[0],
					token: tokens[1],
					new_password1: this.resetPasswordForm.get('password').value,
					new_password2: this.resetPasswordForm.get('passwordConfirm').value
				});
			})).subscribe(
				(res: any) => {
					this.notificationService.push(res.detail);
					this.router.navigate(['/login']);
				},
				err => {
					if(err.status === 400) {
						this.apiErrors = err.error;

						if(err.error.uid || err.error.token) {
							this.apiErrors.non_field_errors = ['Unknown error'];
						}
					}
				});
	}
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

	if (!control.parent || !control) {
		return null;
	}

	const password = control.parent.get('password');
	const passwordConfirm = control.parent.get('passwordConfirm');

	if (!password || !passwordConfirm) {
		return null;
	}

	if (passwordConfirm.value === '') {
		return null;
	}

	if (password.value === passwordConfirm.value) {
		return null;
	}

	return { 'passwordsNotMatching': true };
};
