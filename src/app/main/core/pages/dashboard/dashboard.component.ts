import { Component, OnInit, ViewEncapsulation, NgZone, AfterViewInit } from "@angular/core";
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { MatDialog } from '@angular/material';
import { SiteFormDialogComponent } from '../dialogs/site-form/site-form.dialog.component';
import { SiteRepository } from '../../repositories/site-repo/site.repository';
import { ISite } from '../../repositories/site-repo/site.model';
import { CameraFormDialogComponent } from '../dialogs/camera-form/camera-form.dialog.component';
import { ICamera } from '../../repositories/camera-repo/camera.model';
import { CameraRepository } from '../../repositories/camera-repo/camera.repository';
import { NavbarService } from '../../services/ui/navbar.service';
import { ClickedAction } from '../../services/ui/clicked-action.enum';
import * as _ from 'lodash';
import { DialogsService } from '../../services/ui/dialog.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { filter } from 'rxjs/operators';
import { NavbarInitService } from '../../services/utils/navbar-init.service';
import { LocalAuthService } from '../../services/auth/local-auth.service';
import { IUserModel } from '../../services/auth/user.model';
import { ShareVideoDialogComponent } from '../dialogs/share-video/share-video.component';

@Component({
    selector: 'dashboard-page',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class DashboardComponent implements OnInit, AfterViewInit {
    currentSite: ISite;
    sites: ISite[] = [];
    user: IUserModel;

    constructor(
        private fuseConfigService: FuseConfigService,
        private navbarService: NavbarService,
        public dialog: MatDialog,
        private siteRepo: SiteRepository,
        private cameraRepo: CameraRepository,
        private ngZone: NgZone,
        private dialogService: DialogsService,
        private router: Router,
        private route: ActivatedRoute,
        private navbarInitService: NavbarInitService,
        private localAuthService: LocalAuthService
    ) {
        this.user = this.localAuthService.getUser();
        this.fuseConfigService.config = {
            layout: {
                style: 'vertical-layout-1'
            }
        };
    }

    ngOnInit(): void {
        this.navbarInitService.init();
    }

    ngAfterViewInit() {
        console.log("USER AT LOGIN", this.user);
        
        this.siteRepo.getSitesCount().toPromise().then(count => {
            if (count <= 0) {
                this.dialogService.showFirstLoginDialog(null, this.user);
            } else if (count == 1) {
                this.siteRepo.getAll().subscribe(sites => {
                    this.currentSite = sites[0];

                    if (!this.user.is_wizard_completed) {
                        if (!this.user.agree_share_video) {
                            this.gotoShareVideo('root', this.user);
                        } else {
                            this.gotoShareVideo(this.user.share_video_wizard_step, this.user);
                        }
                    }
                });
            }
        });
    }

    private showAddSiteDialog() {
        this.dialogService.showAddSiteDialog()
            .subscribe(site => {
                this.navbarService.addSiteItem(site);
                //this.currentSite = site;
                this.siteRepo.reload();
                if (this.router.url.startsWith('/map')) {
                    this.router.navigate([`/map/${site.id}`]);
                }
            })
    }

    private showAddCameraDialog() {
        this.dialogService.showAddCameraDialog()
            .subscribe(camera => {
                const auxSite = _.find(this.sites, s => s.id === camera.location);

                this.route.paramMap
                    .pipe(filter(p => p.get('id') !== camera.location))
                    .subscribe(() => this.router.navigate([`/map/${camera.location}`]));
                this.navbarService.updateCamerasCountBadge(auxSite.id, auxSite.cameras.length + 1);
                this.siteRepo.reload();
            });
    }

    gotoShareVideo(step: string, user: IUserModel) {
        this.dialog.open(ShareVideoDialogComponent, {
            maxWidth: '100vw',
            maxHeight: '100vh',
            height: '100%',
            width: '100%',
            disableClose: true,
            data: {
                site: this.currentSite,
                step: step,
                user: user
            }
        });
    }
}