import { Injectable } from '@angular/core';
import { CameraRepository } from '../../repositories/camera-repo/camera.repository';
import { Observable, Subject } from 'rxjs';
import { ICamera } from '../../repositories/camera-repo/camera.model';
import { SiteRepository } from '../../repositories/site-repo/site.repository';
import { map } from 'rxjs/operators';
import { ISite } from '../../repositories/site-repo/site.model';
import { Action } from 'rxjs/internal/scheduler/Action';
import { ActivatedRoute, Router } from '@angular/router';
import { LiveMapComponent } from '../../pages/live-map/live-map.component';

@Injectable()
export class CamerasSidebarService {
	private siteChangeSubject: Subject<ISite>;

	constructor(
		private sitesRepo: SiteRepository,
		private route: ActivatedRoute,
		private router: Router) {
		this.siteChangeSubject = new Subject();
	}

	getSiteChangeNotifier() {
		return this.siteChangeSubject.asObservable();
	}

	changeSite(siteId: string) {
		if (this.route.component !== LiveMapComponent) {
			this.router.navigate(['/map']);
		}
		this.sitesRepo.getById(siteId)
			.subscribe(s => this.siteChangeSubject.next(s));
	}
}