import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { LogoLoaderService } from './resource-loader/logo-loader.service';
import { environment } from 'environments/environment';
import { titleLoader } from './resource-loader/title-loader';
import { Location } from '@angular/common';
import { faviconLoader } from './resource-loader/favicon-loader';
import { backgroundManager, emailBackgroundManager, loginLogoManager } from './resource-loader/dynamic-bg-loader';
import { ISiteowner } from '../../repositories/siteowner/siteowner.model';
import { SKIP_INTERCEPTOR_HEADER } from '../../config/consts';
import { NavbarService } from '../ui/navbar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ClickedAction } from '../ui/clicked-action.enum';
import { DialogsService } from '../ui/dialog.service';
import { SiteRepository } from '../../repositories/site-repo/site.repository';
import { filter } from 'rxjs/operators';
import * as _ from 'lodash';
import { ISite } from '../../repositories/site-repo/site.model';
import { SiteInfoService } from './resource-loader/site-info.service';

@Injectable()
export class InitializationService {
    private sites: ISite[];

    constructor(
        private http: HttpClient,
        private titleService: Title,
        private logoLoaderService: LogoLoaderService,
        private siteInfoService: SiteInfoService
    ) { }

    init() {
        const siteOwner = window.location.hostname.split('.')[0];

        return new Promise((resolve, reject) => {
            this.http.get(`${environment.apiHost}/api/siteowners/${siteOwner}/`, {
                headers: new HttpHeaders().append(SKIP_INTERCEPTOR_HEADER, 'true')
            })
                .subscribe((res: ISiteowner) => {
                    titleLoader(res.name, this.titleService);
                    this.logoLoaderService.updateLogo(res.logo_url);
                    faviconLoader(res.favico_url);
                    backgroundManager(res.background_url);
                    emailBackgroundManager(res.email_confirmation_url);
                    loginLogoManager(res.login_logo_url);
                    resolve();
                }, err => {
                    resolve();
                });
        });
    }
}