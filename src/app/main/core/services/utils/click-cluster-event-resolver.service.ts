import { Injectable } from '@angular/core';
import { zoomChange, mapClick, centerChange } from './click-cluster-event.steps';
import { IMapEvent } from './map-event.model';
import { Observable, Subject, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { IPosition } from '../geo/position.model';
import { positionFactory } from '../../factories/position.factory';
import { ISite } from '../../repositories/site-repo/site.model';

@Injectable()
export class ClickClusterEventResolverService {
	private steps: string[];
	private events: IMapEvent[];
	private clusterEventSubject: Subject<IPosition>;

	constructor() {
		this.clusterEventSubject = new Subject();
		this.restartEvents();
	}

	private restartEvents() {
		this.steps = [];
		this.events = [];
	}

	listenClusterEvents() {
		return this.clusterEventSubject.asObservable();
	}

	addEvent(mapEvent: IMapEvent) {
		if (!event) return;
		if (this.events.length === 0) {
			of([])
				.pipe(delay(5))
				.subscribe(() => {
					if (this.events.length === 3) {
						const center = this.events.filter(e => e.event === centerChange)[0].data;

						this.clusterEventSubject.next(center as IPosition);
						this.restartEvents();
					}
					else {
						this.restartEvents();
					}
				});
		}
		mapEvent.t = event.timeStamp;

		if (this.steps.indexOf(mapEvent.event) < 0) {
			this.steps.push(mapEvent.event);
			this.events.push(mapEvent);
		}
	}

	forceEvent(position: IPosition) {
		this.clusterEventSubject.next(position);
	}
}