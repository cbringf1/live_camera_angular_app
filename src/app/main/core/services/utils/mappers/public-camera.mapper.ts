import * as _ from 'lodash';
import { ICamera } from 'app/main/core/repositories/camera-repo/camera.model';

export const publicCameraMapper = (publicCamera: any) => {
	let result = _.omit(publicCamera, [
		'unmanagedParams',
		'unmanagedType',
		'unmanagedUrl'
	]) as ICamera;

	if (!publicCamera.liveUrls) {
		result.streamType = 'Image';
		result.imageStreamUrl = publicCamera.unmanagedUrl;
		result.imageStreamRefreshTime = publicCamera.unmanagedParams.refreshTime;
	}
	else {
		result.streamType = 'Video';
	}	
	return result;
};