export const faviconLoader = (favicon: string) => {
	let link = document.createElement('link');
	let oldFavicon = document.querySelector('#favicon');

	link.setAttribute('rel', 'icon');
	link.setAttribute('type', 'image/x-icon');
	link.setAttribute('href', favicon);
	link.setAttribute('id', 'favicon');
	document.head.removeChild(oldFavicon);
	document.head.appendChild(link);
};