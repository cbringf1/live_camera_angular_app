import { Title } from '@angular/platform-browser';

export const titleLoader = (title: string, titleService: Title) => {
    titleService.setTitle(title);
};

