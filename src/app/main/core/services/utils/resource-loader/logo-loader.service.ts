import { Injectable } from '@angular/core';
import { defaults } from 'app/main/core/config/defaults';
import { of, Subject } from 'rxjs';
import { concat } from 'rxjs/operators';

@Injectable()
export class LogoLoaderService {
    private loadedLogo: string;
    private logoLoaderSubject: Subject<string>;

    constructor() {
        this.logoLoaderSubject = new Subject();
    }

    getLogo() {
        return of(this.loadedLogo || defaults.logo)
            .pipe(concat(this.logoLoaderSubject.asObservable()));
    }

    updateLogo(logo: string) {
        if (logo) {
            this.loadedLogo = logo;
            this.logoLoaderSubject.next(logo);
        }
    }
}