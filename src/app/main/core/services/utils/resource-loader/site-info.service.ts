import { Injectable } from '@angular/core';
import { defaults } from 'app/main/core/config/defaults';
import { of, Subject } from 'rxjs';
import { concat } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SKIP_INTERCEPTOR_HEADER } from 'app/main/core/config/consts';

@Injectable()
export class SiteInfoService {
    constructor(private http: HttpClient) { }

    getAllSiteInfo() {
        const siteOwner = window.location.hostname.split('.')[0];

        return this.http.get(`${environment.apiHost}/api/siteowners/${siteOwner}/`, {
            headers: new HttpHeaders().append(SKIP_INTERCEPTOR_HEADER, 'true')
        })
    }
}