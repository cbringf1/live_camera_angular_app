import { Subject } from 'rxjs';

export const BG_KEY = 'dynamic-bg-picture';
export const EMAIL_BG_KEY = 'email_dynamic-bg-picture';
export const LOGIN_LOGO_KEY = 'login-logo-dynamic-bg-picture';

const backgroundSubject: Subject<void> = new Subject<void>();

export const backgroundLoader = () => {
    const url = localStorage.getItem(BG_KEY);

    if (url) {
        const node = document.querySelector('.dynamic-bg');
        const style = `
                background: url('${url}') no-repeat;
                background-size: cover;
            `;
        node.setAttribute('style', style);
    }
};

export const backgroundManager = (url: string) => {
    if (url) {
        localStorage.setItem(BG_KEY, url);
        backgroundSubject.next();
    }
};

export const emailBackgroundManager = (url: string) => {
    if (url) {
        localStorage.setItem(EMAIL_BG_KEY, url);
        backgroundSubject.next();
    }
};

export const loginLogoManager = (url: string) => {
    if (url) {
        localStorage.setItem(LOGIN_LOGO_KEY, url);
        backgroundSubject.next();
    }
};

export const getBackgroundSubject = () => {
    return backgroundSubject;
};