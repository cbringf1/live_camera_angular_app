import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { MatSnackBar } from '@angular/material';

const NOTIFICATION_DELAY = 30000;

@Injectable()
export class NotificationService {
	private notificationDb: string[];

	constructor(private snackBar: MatSnackBar) {
		this.notificationDb = [];
	}

	push(notification: string) {
		this.notificationDb.push(notification);
	}

	take(count: number = 1) {
		const result = _.take(this.notificationDb, count);

		this.notificationDb = _.slice(this.notificationDb, count);
		return result;
	}

	notify(notifications: string[] = null, delay = NOTIFICATION_DELAY) {
		if(!notifications) {
			notifications = this.notificationDb;
		}
		notifications.forEach(n => {
			this.snackBar.open(n, 'Ok', {
				duration: delay,
				horizontalPosition: 'center',
				verticalPosition: 'top'
			})
		});
	}
}