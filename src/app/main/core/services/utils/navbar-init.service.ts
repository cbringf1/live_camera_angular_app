import { Injectable } from '@angular/core';
import { NavbarService } from '../ui/navbar.service';
import { DialogsService } from '../ui/dialog.service';
import { ClickedAction } from '../ui/clicked-action.enum';
import { SiteRepository } from '../../repositories/site-repo/site.repository';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { filter, switchMap } from 'rxjs/operators';
import { ISite } from '../../repositories/site-repo/site.model';
import { Subscription } from 'rxjs';

@Injectable()
export class NavbarInitService {
    private siteRepo: SiteRepository;
    private actionsSubscriptions: Subscription;

    constructor(
        private navbarService: NavbarService,
        private dialogService: DialogsService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.siteRepo = dialogService.siteRepo;
    }

    init() {
        if (!this.actionsSubscriptions) {
            this.actionsSubscriptions = this.navbarService.setActions()
                .subscribe(a => a === ClickedAction.AddCamera ?
                    this.showAddCameraDialog() : this.showAddSiteDialog());
        }
        this.navbarService.setSitesNavigationItems();
    }

    private showAddSiteDialog() {
        this.dialogService.showAddSiteDialog()
            .subscribe(site => {
                this.navbarService.addSiteItem(site);
                this.siteRepo.reload().subscribe(() => {
                    if (this.router.url.startsWith('/map')) {
                        this.router.navigate([`/map/${site.id}`]);
                    }
                });
            })
    }

    private showAddCameraDialog() {
        this.dialogService.showAddCameraDialog()
            .subscribe(camera => {
                this.siteRepo.reload().subscribe(sites => {
                    const site = sites.filter(s => s.id === camera.location)[0];
                    this.route.paramMap
                        .pipe(filter(p => p.get('id') !== camera.location))
                        .subscribe(() => {
                            if (this.router.url.startsWith('/map')) {
                                this.router.navigate([`/map/${camera.location}`]);
                            }
                        });
                    this.navbarService.updateCamerasCountBadge(site.id, site.cameras.length);
                });
            });
    }
}