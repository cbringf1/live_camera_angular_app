import * as _ from 'lodash';

export const nullifyFields = (obj: any) => {
	return _.mapValues(obj, v => {
		if (!v) {
			return null;
		}
		else {
			if (!v.toString().trim()) {
				return null;
			}
			else {
				return v;
			}
		}
	});
};