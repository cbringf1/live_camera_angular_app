export interface IMapEvent {
	t?: number,
	event: string;
	data: any;
}