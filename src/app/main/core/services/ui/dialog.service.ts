import { Injectable } from '@angular/core';
import { SiteRepository } from '../../repositories/site-repo/site.repository';
import { CameraRepository } from '../../repositories/camera-repo/camera.repository';
import { MatDialog } from '@angular/material';
import { CameraFormDialogComponent } from '../../pages/dialogs/camera-form/camera-form.dialog.component';
import { ISite } from '../../repositories/site-repo/site.model';
import { ICamera } from '../../repositories/camera-repo/camera.model';
import { pipe, of } from 'rxjs';
import { switchMap, filter } from 'rxjs/operators';
import { SiteFormDialogComponent } from '../../pages/dialogs/site-form/site-form.dialog.component';
import { DeleteAlertDialogComponent } from '../../pages/dialogs/delete-alert/delete-alert.dialog.component';
import * as _ from 'lodash';
import { IUserModel } from '../auth/user.model';
import { ProfileNameComponent } from '../../modules/settings/dialogs/profile-name/profile-name.component';
import { ProfilePasswordComponent } from '../../modules/settings/dialogs/profile-password/profile-password.component';
import { EvidenceFormDialogComponent } from '../../pages/dialogs/evidence-form/evidence-form.dialog.component';
import { UploadMediaDialogComponent } from '../../pages/dialogs/upload-media/upload-media.dialog.component';
import { IEvidence } from '../../repositories/evidence/evidence.model';
import { UseTermsDialogComponent } from '../../pages/dialogs/use-terms-form/use-terms.dialog.component';
import { FirstLoginFormDialogComponent } from '../../pages/dialogs/first-login-form/first-login-form.dialog.component';
@Injectable()
export class DialogsService {
    constructor(
        public siteRepo: SiteRepository,
        private cameraRepo: CameraRepository,
        private dialog: MatDialog
    ) { }

    showAddCameraDialog(site: ISite = null) {
        const auxData = site ? { site: site } : {};
        const dialogRef = this.dialog.open(CameraFormDialogComponent, {
            width: '90%',
            height: '90%',
            data: _.merge({
                sitesList: this.siteRepo.getAll(),
                title: 'Add camera',
            }, auxData)
        });

        return dialogRef.afterClosed()
            .pipe(filter(r => !!r));
    }

    showEditCameraDialog(camera: ICamera, site: ISite) {
        const dialogRef = this.dialog.open(CameraFormDialogComponent, {
            width: '90%',
            height: '90%',
            data: {
                editData: { camera, site: site },
                title: 'Edit camera',
            }
        });

        return dialogRef.afterClosed()
            .pipe(filter(r => !!r));
    }

    showAddSiteDialog() {
        const dialogRef = this.dialog.open(SiteFormDialogComponent, {
            data: {
                title: 'Add Location',
            },
            width: '90%',
            height: '90%'
        });

        return dialogRef.afterClosed()
            .pipe(filter(r => !!r));
    }

    showFirstLoginDialog(site: ISite = null, user: IUserModel) {
        const auxData = site ? { site: site } : {};
        const dialogRef = this.dialog.open(FirstLoginFormDialogComponent, {
            maxWidth: '100vw',
            maxHeight: '100vh',
            height: '100%',
            width: '100%',
            disableClose: true,
            data: _.merge({
                sitesList: this.siteRepo.getAll(),
                user: user
            }, auxData)
        });

        return dialogRef.afterClosed()
            .pipe(filter(r => !!r));
    }

    showEditSiteDialog(site: ISite) {
        const dialogRef = this.dialog.open(SiteFormDialogComponent, {
            width: '90%',
            height: '90%',
            data: {
                title: 'Edit Location',
                site: site
            }
        });

        return dialogRef.afterClosed()
            .pipe(filter(r => !!r));
    }

    showAddEvidenceDialog(evidence: IEvidence = null) {
        const options = {
            data: { evidence }
        };
        const dialogRef = this.dialog.open(EvidenceFormDialogComponent, options);

        return dialogRef.afterClosed()
            .pipe(filter(r => !!r));
    }

    showUploadMediaDialog(evidenceId, media = null) {
        const dialogRef = this.dialog.open(UploadMediaDialogComponent, {
            data: {
                evidenceId: evidenceId,
                media: media
            }
        });

        return dialogRef.afterClosed()
            .pipe(filter(r => !!r));
    }

    showDeleteAlertDialog(itemName, itemType, item: ISite | ICamera) {
        const dialogRef = this.dialog.open(DeleteAlertDialogComponent, {
            data: {
                itemName: itemName,
                itemType: itemType
            }
        });

        return dialogRef.afterClosed()
            .pipe(filter(r => !!r))
            .pipe(switchMap(() => {
                if (itemType === 'site') {
                    return this.siteRepo.delete(item as ISite);
                }
                else {
                    return this.cameraRepo.delete(item as ICamera);
                }
            }));
    }

    showDeleteAlert(itemName, itemType) {
        const dialogRef = this.dialog.open(DeleteAlertDialogComponent, {
            data: {
                itemName: itemName,
                itemType: itemType
            }
        });

        return dialogRef.afterClosed().pipe(filter(r => !!r));
    }

    showProfileNameDialog(user: IUserModel) {
        const dialogRef = this.dialog.open(ProfileNameComponent, {
            data: {
                user: user
            }
        });

        return dialogRef.afterClosed();
    }

    showProfilePasswordDialog(user: IUserModel) {
        const dialogRef = this.dialog.open(ProfilePasswordComponent, {
            data: {
                user: user
            }
        });

        return dialogRef.afterClosed();
    }

    showUseTermsDialog() {
        const dialogRef = this.dialog.open(UseTermsDialogComponent);

        dialogRef.afterClosed().subscribe(res => console.log(res));
    }
}