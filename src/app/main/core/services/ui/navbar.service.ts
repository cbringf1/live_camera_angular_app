import { Injectable } from '@angular/core';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { SiteRepository } from '../../repositories/site-repo/site.repository';
import { Subject, Observable } from 'rxjs';
import { ISite } from '../../repositories/site-repo/site.model';
import { parentNavigationItem, childNavigationItem, childUrlNavigationItem } from '../../factories/navigation-item.factory';
import { ClickedAction } from './clicked-action.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { LiveMapComponent } from '../../pages/live-map/live-map.component';
import { Location } from '@angular/common';

const SITE_ITEM_KEY = 'sites-item';

@Injectable()
export class NavbarService {
	private onItemClickSubject: Subject<ISite>;
	private onActionClickSubject: Subject<ClickedAction>;

	constructor(
		private fuseNavigationService: FuseNavigationService,
		private siteRepo: SiteRepository,
		public route: ActivatedRoute,
		public router: Router,
		private location: Location) {
		this.onItemClickSubject = new Subject();
		this.onActionClickSubject = new Subject();
	}

	setActions(): Observable<ClickedAction> {
		this.fuseNavigationService.updateNavigationItem('add-site', new Object({
			function: () => this.onActionClickSubject.next(ClickedAction.AddSite)
		}));
		this.fuseNavigationService.updateNavigationItem('add-camera', new Object({
			'function': () => this.onActionClickSubject.next(ClickedAction.AddCamera)
		}));
		return this.onActionClickSubject.asObservable();
	}

	setSitesNavigationItems() {
		this.siteRepo.getAll().subscribe(res => {
			const sitesNavigationItems = res
				.map(s => childNavigationItem(s.id, s.name, s.cameras.length, "location_on",
					this.navbarItemClickFn(s)));
			let navItem = this.fuseNavigationService.getNavigationItem(SITE_ITEM_KEY);

			if (navItem) {
				navItem.children = null;
			}
			this.fuseNavigationService.updateNavigationItem(SITE_ITEM_KEY, {
				badge: {
					title: sitesNavigationItems.length,
					bg: '#F44336',
					fg: '#FFFFFF'
				}
			});
			if (sitesNavigationItems.length > 0) {
				this.fuseNavigationService.updateNavigationItem(SITE_ITEM_KEY, {
					type: 'collapsable',
					children: [...sitesNavigationItems]
				});
			}
		});
		return this.onItemClickSubject.asObservable();
	}

	private navbarItemClickFn(site: ISite) {
		return () => {
			if (!this.router.url.startsWith('/map')) {
				this.router.navigate([`/map/${site.id}`]);
			}
			else {
				this.location.go(`/map/${site.id}`);
				this.onItemClickSubject.next(site);
			}
		};
	}

	getSiteItemClickObservable() {
		return this.onItemClickSubject.asObservable();
	}

	setManagementNavigationItems() {
		const managementItem = parentNavigationItem(
			'management',
			'Management',
			null,
			[
				childUrlNavigationItem('site-list', 'Location', null, null, '/dashboard/sites')
			]
		);
		if (!this.fuseNavigationService.getNavigationItem(managementItem.id)) {
			this.fuseNavigationService.addNavigationItem(managementItem, 'end');
		}
	}

	addSiteItem(site: ISite) {
		const auxItem = this.fuseNavigationService.getNavigationItem('sites-item');
		const subItems = auxItem.children || [];

		this.fuseNavigationService.updateNavigationItem(SITE_ITEM_KEY, {
			badge: {
				title: subItems.length + 1
			},
			type: 'collapsable',
			children: [
				...subItems,
				childNavigationItem(
					site.id,
					site.name,
					site.cameras.length,
					"location_on", this.navbarItemClickFn(site))
			]
		});
	}

	patchNavigationItem(id, data) {
		this.fuseNavigationService.updateNavigationItem(id, data);
	}

	updateCamerasCountBadge(id, count) {
		this.fuseNavigationService.updateNavigationItem(id, {
			badge: {
				title: count
			}
		})
	}

	deleteSiteNavigationItem(id: string) {
		const auxItem = this.fuseNavigationService.getNavigationItem('sites-item');

		console.log(auxItem.children.length);

		this.fuseNavigationService.updateNavigationItem(SITE_ITEM_KEY, {
			badge: {
				title: auxItem.children.length - 1
			},
			type: auxItem.children.length === 1 ? 'item' : 'collapsable'
		});
		this.fuseNavigationService.removeNavigationItem(id);
	}

	deleteNavigationItem(id: string) {
		this.fuseNavigationService.removeNavigationItem(id);
	}
}