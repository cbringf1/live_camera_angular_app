import { IUserModel } from './user.model';

export interface IAuthResponseModel {
	token: string;
	user: IUserModel;
}