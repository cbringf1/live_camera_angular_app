import { Injectable } from '@angular/core';
import { AuthService, SocialUser } from 'angularx-social-login';

@Injectable()
export class AuthStateService {
	user: SocialUser | any;
	redirectUrl: string;

	get isLoggedIn() {
		return !!this.user;
	}
}