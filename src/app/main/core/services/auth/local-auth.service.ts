
import { Injectable } from '@angular/core';
import { AuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { AuthStateService } from './auth-state.service';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
import { switchMap, map, filter } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { Router } from '@angular/router';
import { IAuthResponseModel } from './auth-response.model';
import * as _ from 'lodash';
import { IUserModel } from './user.model';
import { IRegisterUser } from './register-user.model';
import { defaults } from '../../config/defaults';
import { DialogsService } from '../ui/dialog.service';
import { MatDialog } from '@angular/material';
import { UseTermsDialogComponent } from '../../pages/dialogs/use-terms-form/use-terms.dialog.component';

const TOKEN_KEY = defaults.token_key;
const USER_KEY = defaults.user_key;
const toSnakeCase = ['firstName', 'lastName'];

@Injectable()
export class LocalAuthService {
	private defaultRedirectUrl = '/';
	private useTermsDialogRef;
	signingOut: boolean = false;

	constructor(
		private authService: AuthService,
		private authState: AuthStateService,
		private http: HttpClient,
		private router: Router,
		private dialog: MatDialog
	) { }

	googleSignIn() {
		from(this.authService.signIn(GoogleLoginProvider.PROVIDER_ID))
			.pipe(switchMap((user: SocialUser) => {
				return this.http.post(`${environment.apiHost}/api/auth/google/`, {
					access_token: user.authToken
				})
					.pipe(map((res: IAuthResponseModel) =>
						new Object({ user: res.user, token: res.token })));
			}))
			.subscribe((res: { user: IUserModel, token: string }) => this.doSignIn(res.token, res.user));
	}

	confirmAccount(key: string) {
		return this.http.post(`${environment.apiHost}/api/auth/registration/verify-email/`, new Object({
			key: key
		}))
			.pipe(filter((res: any) => res.detail === 'ok'));
	}

    emailSignUp(user: IRegisterUser) {
        let newUser = { ...user, domain: window.location.hostname.split('.')[0] };

		return this.http.post(`${environment.apiHost}/api/auth/registration/`, newUser)
			.pipe(map((res: any) => {
				localStorage.setItem('confirm-email', user.email);
				return res.detail;
			}));
	}

	emailSignIn(user: { email: string, password: string }) {
		return this.http.post(`${environment.apiHost}/api/auth/login/`, user)
			.pipe(map((res: IAuthResponseModel) => new Object({ user: res.user, token: res.token })))
			.pipe(map((res: { user: IUserModel, token: string }) => {
				this.doSignIn(res.token, res.user);
				return res;
			}));
	}

	private doSignIn(token: string, user: IUserModel) {
		const url = this.authState.redirectUrl ?
			this.authState.redirectUrl : this.defaultRedirectUrl;
		let localUser = _.omit(user, ['authToken', 'idToken']);

		localUser = _.mapKeys(localUser, (v, k) => {
			if (toSnakeCase.indexOf(k) >= 0) {
				return _.snakeCase(k);
			}
			return k;
		});
		if (!localUser.avatar_url) {
			localUser.avatar_url = 'assets/images/avatars/profile.jpg';
		}

		localStorage.setItem(TOKEN_KEY, token);
		localStorage.setItem(USER_KEY, JSON.stringify(localUser));
		this.router.navigate([url]);
	}

	isLoggedIn() {
		return !!localStorage.getItem(TOKEN_KEY);
	}

	getToken() {
		return localStorage.getItem(TOKEN_KEY);
	}

	getUser() {
		const result = JSON.parse(localStorage.getItem(USER_KEY));

		if((result && !result.agree_terms) && !this.useTermsDialogRef) {
			this.useTermsDialogRef = this.dialog.open(UseTermsDialogComponent, {
				disableClose: true
			});
		}
		return result;
	}

	signOut() {
		localStorage.removeItem(TOKEN_KEY);
		localStorage.removeItem(USER_KEY);
		this.authState.redirectUrl = '';
		this.router.navigate(['/login']);
		this.signingOut = true;
	}

	sendRecoverLink(email: string) {
		return this.http.post(`${environment.apiHost}/api/auth/password/reset/`, {
            email: email,
            domain: window.location.hostname.split('.')[0]
		});
	}

	resetPassword(data: any) {
		return this.http.post(`${environment.apiHost}/api/auth/password/reset/confirm/`, data);
	}

	updatePassword(data: any) {
		return this.http.post(`${environment.apiHost}/api/auth/password/change/`, data);
	}
}