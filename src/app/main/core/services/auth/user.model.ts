export interface IUserModel {
	id?: string;
	email: string;
	first_name: string;
	last_name: string;
	avatar_url: string;
	provider: string;
    agree_terms: boolean;
    agree_share_video?: boolean;
    is_wizard_completed?: boolean;
    share_video_wizard_step?: string;
    organization_name?: string;
    is_core_purchased?: boolean;
    payments: any[];
    amount_cameras_share; number;
}