import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, switchMap, filter, tap } from 'rxjs/operators';
import { defaults } from '../../config/defaults';
import { IUserModel } from './user.model';

const USER_KEY = defaults.user_key;

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    patchUser(id: string, userData: any) {
        return this.http.patch(`${environment.apiHost}/api/users/${id}/`, userData)
            .pipe(map((res: any) => {
                if (!res.avatar_url) {
                    res.avatar_url = 'assets/images/avatars/profile.jpg';
                }
                return res;
            }))
            .pipe(map(res => {
                localStorage.setItem(defaults.user_key, JSON.stringify(res));
                return res;
            }));
    }

    getUseTerms() {
        const domain = window.location.hostname.split('.')[0];
        return this.http.get(`${environment.apiHost}/api/siteowners/${domain}/terms_service/`)
            .pipe(map((res: any) => {
                console.log("getUseTerms", res);

                return res.terms_of_service as string;
            }));
    }

    sendCoupon(couponInfo: any) {
        return this.http.post(`${environment.apiHost}/api/payments/redem/`, couponInfo)
            .pipe(map((res: any) => {
                console.log("couponInfo-res", res);

                return res;
            }));
    }

    patchAvatar(file: File, userId: string) {
        return this.http.post(
            `${environment.apiHost}/api/users/${userId}/upload-avatar/`,
            file,
            {
                reportProgress: true,
                observe: 'events',
                headers: new HttpHeaders({
                    'Content-Disposition': `attachment;filename=${file.name}`,
                    'Content-Type': '*/*'
                })
            })
            .pipe(filter(res => res.type === 4))
            .pipe(switchMap(res => {
                return this.http.get(`${environment.apiHost}/api/users/${userId}/`)
                    .pipe(map(res => {
                        localStorage.setItem(USER_KEY, JSON.stringify(res));
                        return res;
                    }));
            }));
    }

    acceptUseTerms() {
        let user = JSON.parse(localStorage.getItem(USER_KEY));

        user.agree_terms = true;
        return this.http.patch(`${environment.apiHost}/api/users/${user.id}/`, user)
            .pipe(tap(res => localStorage.setItem(USER_KEY, JSON.stringify(user))));
    }

    deleteUser(userId: string) {
        return this.http.delete(`${environment.apiHost}/api/users/${userId}/`);
    }

    sendStripe(info: any) {
        return this.http.post(`${environment.apiHost}/api/payments/stripe/`, info);
    }

    getUserFormApi(userId: string) {
        const domain = window.location.hostname.split('.')[0];

        return this.http.get(`${environment.apiHost}/api/users/${userId}/`)
            .pipe(map((res: any) => {
                console.log(res);
                
                return res as IUserModel;
            }));
    }
}