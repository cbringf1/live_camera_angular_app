import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { LocalAuthService } from './local-auth.service';
import { SKIP_INTERCEPTOR_HEADER } from '../../config/consts';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
	constructor(private localAuthService: LocalAuthService) { }

	intercept(req: HttpRequest<any>, next: HttpHandler) {
		const token = this.localAuthService.getToken();

		if(req.headers.has(SKIP_INTERCEPTOR_HEADER)) {
			const clonedRequest = req.clone({
				headers: req.headers.delete(SKIP_INTERCEPTOR_HEADER)
			});

			return next.handle(clonedRequest); 
		}
		else if (token) {
			let tokenizedReq = req.clone({
				setHeaders: {
					Authorization: `JWT ${token}`
				}
			});
			return next.handle(tokenizedReq);
		}
		return next.handle(req); 
	}
}