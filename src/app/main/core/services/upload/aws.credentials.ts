export interface IAWSCredentials {
	aws_access_key_id: string,
	aws_secret_access_key: string,
	aws_session_token: string,
	aws_upload_bucket: string,
	aws_s3_object_key: string
}