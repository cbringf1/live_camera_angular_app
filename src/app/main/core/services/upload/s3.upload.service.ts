import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { LocalAuthService } from '../auth/local-auth.service';
import { IUserModel } from '../auth/user.model';
import { IAWSCredentials } from './aws.credentials';
import { Subject } from 'rxjs';

@Injectable()
export class S3UploadService {
	private uploadSubject: Subject<any>;

	constructor() {
		this.uploadSubject = new Subject();
	}

	upload(file: File, awsCredentials: IAWSCredentials) {
		const bucket = new S3({
			accessKeyId: awsCredentials.aws_access_key_id,
			secretAccessKey: awsCredentials.aws_secret_access_key,
			sessionToken: awsCredentials.aws_session_token
		});
		const params = {
			Bucket: awsCredentials.aws_upload_bucket,
			Key: awsCredentials.aws_s3_object_key,
			Body: file
		};

		bucket.upload(params, (err, data) => {
			if(err) {
				this.uploadSubject.error(err);
			}
			this.uploadSubject.next(data);
		});
		return this.uploadSubject.asObservable();
	}
}