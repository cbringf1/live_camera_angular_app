import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { LocalAuthService } from '../auth/local-auth.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
	constructor(private localAuth: LocalAuthService) {}

	intercept(req: HttpRequest<any>, next: HttpHandler) {
		return next.handle(req)
			.pipe(catchError((err: HttpErrorResponse) => {
				if(err.status === 401) {
					this.localAuth.signOut();
				}
				return throwError(err);
			}));
	}
}