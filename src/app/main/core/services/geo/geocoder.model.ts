export interface IGeocoderModel {
	fullFormattedAddress: string;
	localAddress: string;
	city: string;
	state: string;
	zip: string;
}