import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { IPosition } from './position.model';
import { MapsAPILoader } from '@agm/core';
import { switchMap } from 'rxjs/operators';
import { IGeocoderModel } from './geocoder.model';
import { geocoderFactory } from '../../factories/geocoder.factory';
import { positionFactory } from '../../factories/position.factory';
import { SiteInfoService } from '../utils/resource-loader/site-info.service';
import { ISiteowner } from 'app/main/core/repositories/siteowner/siteowner.model';

@Injectable()
export class GeolocationService {
    constructor(
        private mapsApiLoader: MapsAPILoader,
        private siteInfoService: SiteInfoService
    ) { }

    watchPosition(): Observable<IPosition> {
        return Observable.create(obs => {
            navigator.geolocation.watchPosition((pos: Position) => {
                obs.next(positionFactory(pos.coords.latitude, pos.coords.longitude));
            });
        });
    }

    getPosition(): Observable<IPosition> {
        return Observable.create(obs => {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(pos =>
                    obs.next(positionFactory(pos.coords.latitude, pos.coords.longitude)));
            }
        });
    }

    resolveAddress(position: IPosition): Observable<IGeocoderModel> {
        return from(this.mapsApiLoader.load())
            .pipe(switchMap(() => {
                const geocoder = new google.maps.Geocoder();
                const location = new google.maps.LatLng(position.lat, position.lng);

                return Observable.create(obs => {
                    geocoder.geocode({ location: location }, (result, status) =>
                        obs.next(geocoderFactory(result)));
                });
            }));
    }
}