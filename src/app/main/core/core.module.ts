import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreRoutingModule } from './core-routing.module';
import { MailLoginFormComponent } from './components/mail-login-form/mail-login-form.component';
import { SocialLoginComponent } from './components/social-login/social-login.component';
import {
    MatButtonModule, MatCheckboxModule,
    MatIconModule, MatInputModule,
    MatDialogModule, MatFormFieldModule,
    MatSelectModule, MatStepperModule,
    MatListModule, MatExpansionModule,
    MatButtonToggleModule, MatMenuModule,
    MatTableModule, MatChipsModule,
    MatCardModule, MatSnackBarModule,
    MatDatepickerModule,
    MatProgressBarModule
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { Login2Component } from './pages/login-2/login-2.component';
import {
    AuthService, AuthServiceConfig,
    GoogleLoginProvider, FacebookLoginProvider, SocialLoginModule
} from 'angularx-social-login';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthStateService } from './services/auth/auth-state.service';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from 'environments/environment';
import { LocalAuthService } from './services/auth/local-auth.service';
import { TokenInterceptorService } from './services/auth/token-interceptor.service';
import { GeolocationService } from './services/geo/geolocation.service';
import { SiteFormDialogComponent } from './pages/dialogs/site-form/site-form.dialog.component';
import { SiteRepository } from './repositories/site-repo/site.repository';
import { CameraFormDialogComponent } from './pages/dialogs/camera-form/camera-form.dialog.component';
import { HttpErrorInterceptor } from './services/error/http-error-interceptor.service';
import { CameraRepository } from './repositories/camera-repo/camera.repository';
import { CameraRecorderRepository } from './repositories/camera-recorders/camera-recorders.repository';
import { SiteCamerasComponent } from './components/site-cameras/site-cameras.component';
import { FuseSidebarModule, FuseWidgetModule } from '@fuse/components';
import { CamerasSidebarService } from './services/status/cameras-sidebar.service';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { MapComponent } from './components/map/map.component';
import { NavbarService } from './services/ui/navbar.service';
import { TruncateModule } from 'ng2-truncate';
import { DeleteAlertDialogComponent } from './pages/dialogs/delete-alert/delete-alert.dialog.component';
import { ClickClusterEventResolverService } from './services/utils/click-cluster-event-resolver.service';
import { SiteAdminComponent } from './modules/site-admin/site-admin-base/site-admin-base.component';
import { LiveMapComponent } from './pages/live-map/live-map.component';
import { SiteAdminModule } from './modules/site-admin/site-admin.module';
import { SiteCamerasModule } from './components/site-cameras/site-cameras.module';
import { SiteMapComponent } from './components/site-map/site-map.component';
import { SiteMapModule } from './components/site-map/site-map.module';
import { DialogsService } from './services/ui/dialog.service';
import { Register2Component } from './pages/register-2/register-2.component';
import { VerifyAccountComponent } from './pages/verify-account/verify-account.component';
import { SettingsModule } from './modules/settings/settings.module';
import { Error404Component } from './pages/404/error-404.component';
import { ForgotPassword2Component } from './pages/forgot-password-2/forgot-password-2.component';
import { ResetPassword2Component } from './pages/reset-password-2/reset-password-2.component';
import { NotificationService } from './services/utils/notification.service';
import { UserService } from './services/auth/user.service';
import { ErrorMsgComponent } from './components/error-msg/error-msg.component';
import { ErrorMsgModule } from './components/error-msg/error-msg.module';
import { ResourceLoader } from '@angular/compiler';
import { HttpClient } from '@angular/common/http';
import { LogoLoaderService } from './services/utils/resource-loader/logo-loader.service';
import { Title } from '@angular/platform-browser';
import { InitializationService } from './services/utils/initialization.service';
import { PlayerModule } from './components/player/player.module';
import { EvidenceFormDialogComponent } from './pages/dialogs/evidence-form/evidence-form.dialog.component';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { UploadMediaDialogComponent } from './pages/dialogs/upload-media/upload-media.dialog.component';
import { UseTermsDialogComponent } from './pages/dialogs/use-terms-form/use-terms.dialog.component';
import { PublicCamerasRepository } from './repositories/camera-repo/public-cameras.repo';
import { LayersModule } from './components/layers/layers.module';
import { FirstLoginFormDialogComponent } from './pages/dialogs/first-login-form/first-login-form.dialog.component';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { CameraConfirmationDialogComponent } from './pages/dialogs/camera-added-confirmation/camera-added-confirmation.component';
import { SiteInfoService } from './services/utils/resource-loader/site-info.service';
import { ShareVideoDialogComponent } from './pages/dialogs/share-video/share-video.component';
import { RecordersRepository } from './repositories/recorders/recorders.repository';
import { InlineEditModule } from './components/inline-edit/inline-edit.module';
import { SatPopoverModule } from '@ncstate/sat-popover';
import { PaymentConfirmationDialogComponent } from './pages/dialogs/payment-confirmation/payment-confirmation.component';

const config = new AuthServiceConfig([
    {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider(environment.google.client_id)
    }
]);

export function provideConfig() {
    return config;
}

export function initializeApp(
    appInitService: InitializationService) {
    return () => {
        appInitService.init();
        //navbarInitService.init();
    };
}

@NgModule({
    entryComponents: [
        SiteFormDialogComponent,
        CameraFormDialogComponent,
        DeleteAlertDialogComponent,
        EvidenceFormDialogComponent,
        UploadMediaDialogComponent,
        UseTermsDialogComponent,
        FirstLoginFormDialogComponent,
        CameraConfirmationDialogComponent,
        ShareVideoDialogComponent,
        PaymentConfirmationDialogComponent
    ],
    declarations: [
        Login2Component,
        DashboardComponent,
        SiteFormDialogComponent,
        CameraConfirmationDialogComponent,
        PaymentConfirmationDialogComponent,
        CameraFormDialogComponent,
        DeleteAlertDialogComponent,
        EvidenceFormDialogComponent,
        UploadMediaDialogComponent,
        UseTermsDialogComponent,
        MapComponent,
        LiveMapComponent,
        Register2Component,
        VerifyAccountComponent,
        Error404Component,
        ForgotPassword2Component,
        ResetPassword2Component,
        FirstLoginFormDialogComponent,
        ShareVideoDialogComponent
    ],
    imports: [
        CommonModule,
        CoreRoutingModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatMenuModule,
        MatCheckboxModule,
        MatCardModule,
        MatIconModule,
        MatInputModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSelectModule,
        MatStepperModule,
        MatExpansionModule,
        MatTableModule,
        MatChipsModule,
        MatProgressBarModule,
        FuseWidgetModule,
        FuseSidebarModule,
        FuseSharedModule,
        MatListModule,
        MatSnackBarModule,
        MatDatepickerModule,
        InlineEditModule,
        SatPopoverModule,
        AgmCoreModule.forRoot({
            apiKey: environment.googleApiKey,
            libraries: ["places"]
        }),
        AgmJsMarkerClustererModule,
        HttpClientModule,
        SiteAdminModule,
        SettingsModule,
        SiteCamerasModule,
        SiteMapModule,
        ErrorMsgModule,
        PlayerModule,
        LayersModule
    ],
    exports: [
        Error404Component
    ],
    providers: [
        InitializationService,
        {
            provide: APP_INITIALIZER,
            useFactory: initializeApp,
            multi: true,
            deps: [
                InitializationService
            ]
        },
        AuthStateService,
        AuthService,
        {
            provide: AuthServiceConfig,
            useFactory: provideConfig
        },
        LocalAuthService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptorService,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpErrorInterceptor,
            multi: true
        },
        GeolocationService,
        SiteRepository,
        CameraRepository,
        CameraRecorderRepository,
        CamerasSidebarService,
        NavbarService,
        ClickClusterEventResolverService,
        DialogsService,
        NotificationService,
        UserService,
        Title,
        LogoLoaderService,
        SiteInfoService,
        RecordersRepository,
        { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
        PublicCamerasRepository,
        {
            provide: STEPPER_GLOBAL_OPTIONS,
            useValue: { displayDefaultIndicatorType: false }
        }
    ]
})
export class CoreModule { }