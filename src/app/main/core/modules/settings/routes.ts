import { Routes } from '@angular/router';
import { SettingsBaseComponent } from './settings-base/settings-base.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from '../../auth/auth.guard';

export const routes: Routes = [
	{
		path: 'settings',
		component: SettingsBaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: '',
				redirectTo: 'profile',
				pathMatch: 'full'
			},
			{
				path: 'profile',
				component: ProfileComponent
			}
		]
	}
];