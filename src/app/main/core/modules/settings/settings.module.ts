import { NgModule } from '@angular/core';
import { SettingsRoutingModule } from './settings-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { SettingsBaseComponent } from './settings-base/settings-base.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatChipsModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSortModule, MatSnackBarModule, MatTableModule, MatTabsModule, MatGridListModule, MatListModule, MatDialogModule } from '@angular/material';
import { FuseSidebarModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { ErrorMsgModule } from '../../components/error-msg/error-msg.module';
import { ProfileNameComponent } from './dialogs/profile-name/profile-name.component';
import { ProfilePasswordComponent } from './dialogs/profile-password/profile-password.component';
import { NgxUploaderModule } from 'ngx-uploader';

@NgModule({
	declarations: [
		ProfileComponent,
		SettingsBaseComponent,
		ProfileNameComponent,
		ProfilePasswordComponent
	],
	entryComponents: [
		ProfileNameComponent,
		ProfilePasswordComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatButtonModule,
		MatChipsModule,
		MatExpansionModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatPaginatorModule,
		MatRippleModule,
		MatSelectModule,
		MatSortModule,
		MatSnackBarModule,
		MatTableModule,
		MatTabsModule,
		MatTableModule,
		MatGridListModule,
		FuseSidebarModule,
		FuseSharedModule,
		SettingsRoutingModule,
		ErrorMsgModule,
		MatListModule,
		MatDialogModule,
		NgxUploaderModule
	]
})
export class SettingsModule {}