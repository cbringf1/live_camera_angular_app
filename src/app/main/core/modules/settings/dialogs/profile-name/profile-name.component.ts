import { Component, OnInit, Inject } from "@angular/core";
import { IUserModel } from 'app/main/core/services/auth/user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService } from 'app/main/core/services/auth/user.service';
import { NotificationService } from 'app/main/core/services/utils/notification.service';

@Component({
	selector: 'profile-name-page',
	templateUrl: './profile-name.component.html',
	styleUrls: ['./profile-name.component.scss']
})
export class ProfileNameComponent implements OnInit {
	private user: IUserModel;
	private registerForm: FormGroup;

	constructor(
		private _formBuilder: FormBuilder,
		@Inject(MAT_DIALOG_DATA) private data: any,
		private userService: UserService,
		private notificationService: NotificationService,
		private dialogRef: MatDialogRef<ProfileNameComponent>
	) {
		this.user = data.user;
	}

	ngOnInit() {
		this.registerForm = this._formBuilder.group({
			first_name: [this.user.first_name || '', Validators.maxLength(50)],
			last_name: [this.user.last_name || '', Validators.maxLength(50)]
		});
	}

	updateUser() {
		this.userService.patchUser(this.user.id, this.registerForm.value)
			.subscribe(res => {
				this.notificationService.notify(['Contact info updated'], 5000);
				this.dialogRef.close(res);
			});
	}
}