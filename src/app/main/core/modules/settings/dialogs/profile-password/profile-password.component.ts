import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IUserModel } from 'app/main/core/services/auth/user.model';
import {
	FormGroup, FormBuilder,
	Validators, ValidatorFn,
	AbstractControl, ValidationErrors
} from '@angular/forms';
import { LocalAuthService } from 'app/main/core/services/auth/local-auth.service';
import { NotificationService } from 'app/main/core/services/utils/notification.service';

@Component({
	selector: 'profile-password-page',
	templateUrl: './profile-password.component.html',
	styleUrls: ['./profile-password.component.scss']
})
export class ProfilePasswordComponent implements OnInit {
	private passwordForm: FormGroup;
	private apiErrors: any = {};

	constructor(
		private _formBuilder: FormBuilder,
		private dialogRef: MatDialogRef<ProfilePasswordComponent>,
		private localAuthService: LocalAuthService,
		private notificationService: NotificationService
	) {}

	ngOnInit() {
		this.passwordForm = this._formBuilder.group({
			old_password: ['', Validators.required],
			new_password1: ['', Validators.required],
			new_password2: ['', [Validators.required, confirmPasswordValidator]]
		});
	}

	updatePassword() {
		this.apiErrors = {};
		if (this.passwordForm.valid) {
			this.localAuthService.updatePassword(this.passwordForm.value)
				.subscribe(
					res => {
						this.notificationService.notify(['Password updated'], 5000);
						this.dialogRef.close();
					},
					err => {
						if (err.status === 400) {
							this.apiErrors = err.error;
						}
					}
				);
		}
	}
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

	if (!control.parent || !control) {
		return null;
	}

	const password = control.parent.get('new_password1');
	const passwordConfirm = control.parent.get('new_password2');

	if (!password || !passwordConfirm) {
		return null;
	}

	if (passwordConfirm.value === '') {
		return null;
	}

	if (password.value === passwordConfirm.value) {
		return null;
	}

	return { 'passwordsNotMatching': true };
};
