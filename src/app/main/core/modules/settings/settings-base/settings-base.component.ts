import { Component } from '@angular/core';

@Component({
	selector: 'settings-base',
	template: '<router-outlet></router-outlet>'
})
export class SettingsBaseComponent {}