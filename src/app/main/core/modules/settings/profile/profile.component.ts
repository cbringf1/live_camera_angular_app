import { Component, OnInit, EventEmitter } from "@angular/core";
import { fuseAnimations } from '@fuse/animations';
import { ViewEncapsulation } from '@angular/compiler/src/core';
import { LocalAuthService } from 'app/main/core/services/auth/local-auth.service';
import { SocialUser } from 'angularx-social-login';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { UserService } from 'app/main/core/services/auth/user.service';
import { NotificationService } from 'app/main/core/services/utils/notification.service';
import { IUserModel } from 'app/main/core/services/auth/user.model';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { DialogsService } from 'app/main/core/services/ui/dialog.service';
import { UploadInput, UploadOutput, UploaderOptions } from 'ngx-uploader';
import { environment } from 'environments/environment';
import { HttpEventType } from '@angular/common/http';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
    private user: IUserModel;
    private uploadInput: EventEmitter<UploadInput>;
    private options: UploaderOptions;

    constructor(
        private localAuthService: LocalAuthService,
        private dialogService: DialogsService,
        private userService: UserService,
        private notificationService: NotificationService
    ) {
        this.uploadInput = new EventEmitter();
        this.options = {
            concurrency: 1,
            maxUploads: 1
        };
        this.user = this.localAuthService.getUser();
    }

    showProfileNameDialog() {
        this.dialogService.showProfileNameDialog(this.user)
            .subscribe(res => {
                if (res) {
                    this.user = res
                }
            });
    }


    showProfilePasswordDialog() {
        this.dialogService.showProfilePasswordDialog(this.user);
    }


    onFileChange(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];

            this.userService.patchAvatar(file, this.user.id)
                .subscribe((res: IUserModel) => {
                    this.user = res;
                    this.notificationService.notify(['Avatar updated!'], 5000);
                });

        }
    }

    uploadOutput(output: UploadOutput) {
        if (output.type === 'allAddedToQueue') {
            const event: UploadInput = {
                type: 'uploadAll',
                url: `${environment.apiHost}/api/users/${this.user.id}/upload-avatar/`,
                method: 'POST',
                headers: {
                    'Authorization': `JWT ${this.localAuthService.getToken()}`,
                    'Content-Disposition': `attachment;filename=2017-07-23.jpg`
                }
            };
            this.uploadInput.emit(event);
        }
        if (output.type === 'done') {
            console.log('finished');
        }
    }

    selectAvatar() {
        let node = document.querySelector('#upload-file-btn');
        let event = new Event('click');
        node.dispatchEvent(event);
    }

    deleteAccount() {
        this.dialogService.showDeleteAlert('account', this.localAuthService.getUser().email)
            .subscribe(() => {
                this.userService.deleteUser(this.localAuthService.getUser().id)
                    .subscribe(() => this.localAuthService.signOut());
            });
    }
}