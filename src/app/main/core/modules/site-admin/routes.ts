import { Routes } from '@angular/router';
import { SiteAdminComponent } from './site-admin-base/site-admin-base.component';
import { SiteListComponent } from './list/site-list.component';
import { SiteDetailsComponent } from './details/site-details.component';
import { AuthGuard } from '../../auth/auth.guard';
import { EvidenceListComponent } from './evidence-list/evidence-list.component';
import { EvidenceComponent } from './evidence/evidence.component';
import { InboxComponent } from './inbox/inbox.component';
import { LoadMessagesGuard } from './inbox/store/guards/load-messages.guard';
import { SelectMessageGuard } from './inbox/store/guards/select-message.guard';

export const routes: Routes = [
	{
		path: 'management',
		component: SiteAdminComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: '',
				redirectTo: 'sites',
				pathMatch: 'full'
			},
			{
				path: 'sites',
				component: SiteListComponent
			},
			{
				path: 'sites/:id',
				component: SiteDetailsComponent
			},
			{
				path: 'evidences',
				component: EvidenceListComponent
			},
			{
				path: 'evidences/:id',
				component: EvidenceComponent
			},
			{
				path: 'inbox',
				component: InboxComponent,
				canActivate: [LoadMessagesGuard]
			},
			{
				path: 'inbox/:messageId',
				component: InboxComponent,
				canActivate: [LoadMessagesGuard, SelectMessageGuard]
			}
		]
	}
];