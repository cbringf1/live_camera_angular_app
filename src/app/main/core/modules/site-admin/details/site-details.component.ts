import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { SiteRepository } from 'app/main/core/repositories/site-repo/site.repository';
import { ActivatedRoute, Router } from '@angular/router';
import { ISite } from 'app/main/core/repositories/site-repo/site.model';
import { switchMap, skip } from 'rxjs/operators';
import { positionFactory } from 'app/main/core/factories/position.factory';
import { MapsAPILoader } from '@agm/core';
import { of } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ICamera } from 'app/main/core/repositories/camera-repo/camera.model';
import { NavbarService } from 'app/main/core/services/ui/navbar.service';
import * as _ from 'lodash';
import { IPosition } from 'app/main/core/services/geo/position.model';
import { DialogsService } from 'app/main/core/services/ui/dialog.service';
import { IGeocoderModel } from 'app/main/core/services/geo/geocoder.model';
import { geocoderFactory } from 'app/main/core/factories/geocoder.factory';
import { SiteMapComponent } from 'app/main/core/components/site-map/site-map.component';
import { siteFormGroupData } from 'app/main/core/config/forms';
import { nullifyFields } from 'app/main/core/services/utils/nullify-fields';
import { HttpErrorResponse } from '@angular/common/http';
import { DepartmentsComponent } from 'app/main/core/components/departments/departments.component';
import { CameraListComponent } from 'app/main/core/components/camera-list/camera-list.component';
import { LocalAuthService } from 'app/main/core/services/auth/local-auth.service';
import { NotificationService } from 'app/main/core/services/utils/notification.service';

@Component({
    selector: 'site-details',
    templateUrl: './site-details.component.html',
    styleUrls: ['./site-details.component.scss']
})
export class SiteDetailsComponent implements OnInit {
    site: ISite;
    @ViewChild('search') searchElementRef: ElementRef;
    @ViewChild('siteMap') siteMap: SiteMapComponent;
    @ViewChild('camerasMap') camerasMap: SiteMapComponent;
    @ViewChild('departments') departments: DepartmentsComponent;
    @ViewChild('cameraList') cameraList: CameraListComponent;
    form: FormGroup;
    currentCamera: ICamera;
    httpErrors: any;
    private tab: any;
    private query: string;
    siteChanged: boolean;

    constructor(
        private formBuilder: FormBuilder,
        private siteRepo: SiteRepository,
        private route: ActivatedRoute,
        private ngZone: NgZone,
        private mapsApiLoader: MapsAPILoader,
        private navbarService: NavbarService,
        private router: Router,
        private dialogService: DialogsService,
        localAuth: LocalAuthService,
        private notificationService: NotificationService,
    ) {
        this.siteChanged = false;
        this.form = this.formBuilder.group(siteFormGroupData(null, localAuth.getUser()));
        this.route.paramMap
            .pipe(switchMap(p => this.siteRepo.getById(p.get('id'))))
            .subscribe(s => {
                this.site = s;
                this.updateForm(s);
                this.siteMap.computePosition(this.site);
                this.form.valueChanges.pipe(skip(1)).subscribe(() => {
                    this.siteChanged = true;
                });
            });
    }

    ngOnInit() {
        this.mapsApiLoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ["address"]
            });

            autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();

                    if (place.geometry) {
                        let address = geocoderFactory([place]);
                        let position = positionFactory(
                            place.geometry.location.lat(),
                            place.geometry.location.lng()
                        );
                        this.form.patchValue(this.parseGeocoderData(address, position));
                    }
                });
            });
        });
        this.form.valueChanges.
            subscribe(f => {
                if (f.primary_contact_phone && f.primary_contact_phone.startsWith("+1")) {
                    this.form.get('primary_contact_phone')
                        .setValue(f.primary_contact_phone.slice(2));
                }
                if (f.secondary_contact_phone && f.secondary_contact_phone.startsWith("+1")) {
                    this.form.get('secondary_contact_phone')
                        .setValue(f.secondary_contact_phone.slice(2));
                }
            });
    }

    positionLoaded(event: { position: IPosition, address: IGeocoderModel }) {
        this.updateGeocoderData(event);
    }

    dragEnd(event: { position: IPosition, address: IGeocoderModel }) {
        this.updateGeocoderData(event);
    }

    private updateGeocoderData(event: { position: IPosition, address: IGeocoderModel }) {
        let data = this.parseGeocoderData(event.address, event.position);

        this.form.patchValue(data);
        this.site = _.merge(this.site, data);
    }

    private parseGeocoderData(address: IGeocoderModel, position: IPosition) {
        let fieldsMap = {
            fullFormattedAddress: 'fullAddress',
            localAddress: 'address',
            lat: 'lat',
            lng: 'lon'
        };
        let auxPosition = _.mapKeys(position, (v, k) => fieldsMap[k]);
        let auxAddress = _.mapKeys(address, (v, k) => fieldsMap[k] || k);

        return _.merge(auxAddress, auxPosition);
    }

    private updateForm(site: ISite) {
        this.form.patchValue(this.site);
    }

    selectedCamera(camera: ICamera) {
        this.currentCamera = camera;
        this.camerasMap.mouseOver(camera.id);
    }

    deleteSite(site: ISite) {
        this.dialogService.showDeleteAlertDialog(site.name, 'site', site)
            .subscribe(() => {
                this.router.navigate(['/management/sites']);
                this.navbarService.deleteNavigationItem(site.id);
                this.siteRepo.reload();
            });
    }

    cameraUpdated(camera: ICamera) {
        this.siteRepo.reload().subscribe(sites => {
            this.site = sites.filter(s => s.id === camera.location)[0];
        });
    }

    cameraDeleted(camera: ICamera) {
        this.siteRepo.getById(camera.location)
            .subscribe(s => {
                this.site = s;
                this.currentCamera = null;
                this.navbarService.updateCamerasCountBadge(s.id, s.cameras.length);
                this.siteRepo.reload();
            });
    }

    saveSite() {
        if (this.form.valid) {
            let site = this.form.value;

            if (this.site) {
                site['id'] = this.site.id;
                if (site['primary_contact_phone']) {
                    site['primary_contact_phone'] = '+1' + site['primary_contact_phone'];
                }
                if (site['secondary_contact_phone']) {
                    site['secondary_contact_phone'] = '+1' + site['secondary_contact_phone'];
                }
            }
            site = nullifyFields(site);
            this.siteRepo.patch(site).subscribe(s => {
                this.site = s;
                this.navbarService.patchNavigationItem(s.id, {
                    title: s.name
                });
                this.siteRepo.reload();
                this.notificationService.notify(['Site info updated'], 5000);
                this.siteChanged = false;
            }, err => this.handleHttpErrors(err));
        }
    }

    private handleHttpErrors(err: HttpErrorResponse) {
        if (err.status === 400) {
            this.httpErrors = err.error;
        }
    }

    addCamera() {
        this.dialogService.showAddCameraDialog(this.site)
            .subscribe(camera => {
                this.siteRepo.reload().subscribe(sites => {
                    this.site = sites.filter(s => s.id === camera.location)[0];
                });
            });
    }

    refreshDepartments() {
        this.departments.refreshDepartments();
    }

    searchDepartments() {
        if (this.query) {
            this.departments.search(this.query);
        }
    }

    cameraDetails(event) {
        this.cameraList.editCamera(event.camera);
    }

    deleteCamera(event) {
        this.cameraList.deleteCamera(event);
    }
}