import { Component, Input, HostBinding, OnInit } from '@angular/core';
import { IMessage } from 'app/main/core/repositories/inbox/message.model';

@Component({
	selector: 'message-list-item',
	templateUrl: './message-list-item.component.html',
	styleUrls: ['./message-list-item.component.scss']
})
export class MessageListItemComponent implements OnInit {
	@Input() message: IMessage;
	@HostBinding('class.unread') unread: boolean;
	@HostBinding('class.selected') selected: boolean = false;

	ngOnInit() {
		this.unread = !this.message.is_seen;
	}
}