import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { InboxState } from '../store/inbox.reducers';
import * as fromActions from '../store/inbox.actions';
import { IMessage } from 'app/main/core/repositories/inbox/message.model';
import { Observable } from 'rxjs';
import { getMessagesLoadState } from '../store/inbox.selectors';

@Component({
	selector: 'message-list',
	templateUrl: './message-list.component.html',
	styleUrls: ['./message-list.component.scss']
})
export class MessageListComponent implements OnInit {
	@Input() messages: IMessage[];
	@Input() selectedMessage: IMessage;

	loadState$: Observable<{ loading: boolean, loaded: boolean }>;

	constructor(
		private router: Router,
		private store: Store<InboxState>
	) { }

	ngOnInit() {
		this.loadState$ = this.store.pipe(select(getMessagesLoadState));
	}

	readMessage(message: IMessage) {
		this.router.navigate([`management/inbox/${message.id}`]);
	}
}