import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { select, Store } from '@ngrx/store';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { getMessagesLoadState, getMessages } from '../inbox.selectors';
import { filter, switchMap, catchError, take, map } from 'rxjs/operators';
import { InboxState } from '../inbox.reducers';
import * as fromActions from '../inbox.actions';

@Injectable()
export class SelectMessageGuard implements CanActivate {
	constructor(
		private store: Store<InboxState>
	) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.store.pipe(
			select(getMessages),
			map(messages => {
				const message = messages.find(m => m.id === route.paramMap.get('messageId'));

				if (message && !message.is_seen) {
					this.store.dispatch(fromActions.MARK_AS_READ({ messageId: message.id }));
				}
				return true;
			})
		);
	}
}
