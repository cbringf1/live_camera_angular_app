import {
	Action, createReducer, on,
	createFeatureSelector, State, createSelector
} from '@ngrx/store';

import * as fromActions from './inbox.actions';
import { IMessage } from 'app/main/core/repositories/inbox/message.model';

import * as _ from 'lodash';

export interface AppState {
	inbox: InboxState
}

export interface InboxState {
	loading: boolean;
	loaded: boolean;
	messages: IMessage[];
	searchText: string;
}

export const selectInboxFeature = createFeatureSelector<InboxState>('inbox');

export const initialState: InboxState = {
	loading: false,
	loaded: false,
	messages: [],
	searchText: ''
};

const inboxReducer = createReducer(
	initialState,
	on(fromActions.SUCCESS_LOADED, (state, { messages }) => ({
		...state,
		messages,
		loaded: true,
		loading: false
	})),
	on(fromActions.LOADING_MESSAGES, (state) => ({ ...state, loading: true })),
	on(fromActions.MARK_AS_READ_SUCCESS, (state, { messageId }) => ({
		...state,
		messages: _.map(state.messages, m => {
			if (m.id === messageId) {
				return { ...m, is_seen: true };
			}
			return m;
		})
	})),
	on(fromActions.SET_SEARCH_TEXT, (state, { searchText }) => ({ ...state, searchText }))
);

export function reducer(state: InboxState | undefined, action: Action) {
	return inboxReducer(state, action);
}