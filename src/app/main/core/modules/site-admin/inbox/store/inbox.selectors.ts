import { createSelector } from '@ngrx/store';

import { InboxState, selectInboxFeature } from './inbox.reducers';
import * as fromRoot from '../../../../../../store'

import * as _ from 'lodash';

export const getSearchText = createSelector(
	selectInboxFeature,
	(state: InboxState) => state.searchText
);

export const getMessages = createSelector(
	selectInboxFeature,
	getSearchText,
	(state: InboxState, searchText: string) => _.filter(state.messages, m => (
		!searchText || m.sender_organization.toLowerCase().includes(searchText.toLowerCase())
	))
);

export const getMessagesLoadState = createSelector(
	selectInboxFeature,
	(state: InboxState) => _.pick(state, ['loading', 'loaded'])
);

export const getSelectedMessage = createSelector(
	getMessages,
	fromRoot.getRouterState,
	(messages, router) => router.state && _.find(messages, m => m.id === router.state.params.messageId)
);