import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromInbox from './inbox.reducers';
import { InboxEffects } from './inbox.effects';

@NgModule({
	imports: [
		StoreModule.forFeature('inbox', fromInbox.reducer),
		EffectsModule.forFeature([InboxEffects])
	]
})
export class InvoiceStoreModule {}