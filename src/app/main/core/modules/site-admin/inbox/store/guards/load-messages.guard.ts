import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { select, Store } from '@ngrx/store';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { getMessagesLoadState } from '../inbox.selectors';
import { filter, switchMap, catchError, take, map } from 'rxjs/operators';
import { InboxState } from '../inbox.reducers';
import { LOAD_MESSAGES, LOADING_MESSAGES } from '../inbox.actions';

@Injectable()
export class LoadMessagesGuard implements CanActivate {
	constructor(
		private store: Store<InboxState>
	) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.checkMessages().pipe(
			switchMap(() => of(true)),
			catchError(() => of(false))
		);
	}

	checkMessages(): Observable<any> {
		return this.store.pipe(
			select(getMessagesLoadState),
			map(state => {
				if(!state.loaded && !state.loading) {
					this.store.dispatch(LOADING_MESSAGES());
					this.store.dispatch(LOAD_MESSAGES());
				}
			}),
		)
	}
}
