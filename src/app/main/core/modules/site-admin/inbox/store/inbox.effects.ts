import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import * as fromActions from './inbox.actions';
import { InboxRepository } from 'app/main/core/repositories/inbox/inbox.repository';
import { EMPTY } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';

@Injectable()
export class InboxEffects {
	loadMessages$ = createEffect(() =>
		this.actions$.pipe(
			ofType(fromActions.LOAD_MESSAGES),
			mergeMap(() =>
				this.inboxRepo.getAll()
					.pipe(
						map(messages => fromActions.SUCCESS_LOADED({ messages })),
						catchError(() => EMPTY)
					)
			)
		)
	);

	markAsRead$ = createEffect(() =>
		this.actions$.pipe(
			ofType(fromActions.MARK_AS_READ),
			mergeMap(({ messageId }) =>
				this.inboxRepo.markAsRead(messageId)
					.pipe(
						map(message => fromActions.MARK_AS_READ_SUCCESS({ messageId: message.id })),
						catchError(() => EMPTY)
					)
			)
		)
	);	

	constructor(
		private actions$: Actions,
		private inboxRepo: InboxRepository
	) { }
}