import { createAction, props } from '@ngrx/store';

import { IMessage } from 'app/main/core/repositories/inbox/message.model';

export const LOAD_MESSAGES = createAction('[INBOX] LOAD MESSAGES - EFFECT');
export const SUCCESS_LOADED = createAction(
	'[INBOX] MESSAGES LOADED SUCCESSFULLY',
	props<{messages: IMessage[]}>()
);
export const LOADING_MESSAGES = createAction('[INBOX] LOADING MESSAGES');
export const MARK_AS_READ = createAction(
	'[INBOX] MARK API MESSAGE AS READ - EFFECT',
	props<{messageId: string}>()
);
export const MARK_AS_READ_SUCCESS = createAction(
	'[INBOX] MARK LOCAL MESSAGE AS READ',
	props<{messageId: string}>()
);
export const SET_SEARCH_TEXT = createAction(
	'[INBOX] SET MESSAGES SEARCH TEXT',
	props<{searchText: string}>()
);