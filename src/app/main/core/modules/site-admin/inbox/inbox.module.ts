import { NgModule } from '@angular/core';
import { InboxComponent } from './inbox.component';
import { MessageListComponent } from './message-list/message-list.component';
import { MessageListItemComponent } from './message-list/message-list-item/message-list-item.component';
import {
	MatButtonModule, MatCheckboxModule,
	MatDialogModule, MatFormFieldModule,
	MatIconModule, MatInputModule,
	MatMenuModule, MatRippleModule,
	MatSelectModule, MatToolbarModule,
	MatProgressSpinnerModule
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { InvoiceStoreModule } from './store/inbox-store.module';
import { AvatarModule } from 'ngx-avatar';
import { MessageDetailsComponent } from './message-details/message-details.component';

@NgModule({
	declarations: [
		InboxComponent,
		MessageListComponent,
		MessageListItemComponent,
		MessageDetailsComponent
	],
	imports: [
		MatButtonModule,
		MatCheckboxModule,
		MatDialogModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatMenuModule,
		MatRippleModule,
		MatSelectModule,
		MatToolbarModule,
		MatProgressSpinnerModule,

		FuseSharedModule,
		FuseSidebarModule,

		InvoiceStoreModule,

		AvatarModule
	]
})
export class InboxModule { }