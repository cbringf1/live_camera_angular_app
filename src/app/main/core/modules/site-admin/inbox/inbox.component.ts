import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { InboxState } from './store/inbox.reducers';
import { Store, select } from '@ngrx/store';
import { getMessages, getSelectedMessage, getSearchText } from './store/inbox.selectors';
import { LOAD_MESSAGES } from './store/inbox.actions';
import { IMessage } from 'app/main/core/repositories/inbox/message.model';
import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

import * as fromActions from './store/inbox.actions';

@Component({
	selector: 'inbox-page',
	templateUrl: './inbox.component.html',
	styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
	messages$: Observable<IMessage[]>;
	currentMessage$: Observable<IMessage>;
	searchText$: Observable<string>;

	searchInput: FormControl;

	constructor(private store: Store<InboxState>) {
		this.messages$ = this.store.pipe(select(getMessages));
		this.currentMessage$ = this.store.pipe(select(getSelectedMessage));
		this.searchText$ = this.store.pipe(select(getSearchText));

		this.searchInput = new FormControl();
	}

	ngOnInit() {
		this.searchText$.subscribe(searchText => this.searchInput.setValue(searchText));
		this.searchInput.valueChanges.pipe(
			debounceTime(300),
			distinctUntilChanged(),
		)
		.subscribe(searchText => this.store.dispatch(fromActions.SET_SEARCH_TEXT({ searchText })));
	}
}