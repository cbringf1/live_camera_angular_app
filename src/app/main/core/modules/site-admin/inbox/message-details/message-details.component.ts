import { Component, Input } from '@angular/core';
import { IMessage } from 'app/main/core/repositories/inbox/message.model';

@Component({
	selector: 'message-details',
	templateUrl: './message-details.component.html',
	styleUrls: ['./message-details.component.scss']
})
export class MessageDetailsComponent {
	@Input() message: IMessage;
}