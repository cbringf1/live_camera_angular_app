import { Component } from "@angular/core";

@Component({
	selector: 'site-admin-base',
	template: '<router-outlet></router-outlet>'
})
export class SiteAdminComponent {
}