import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EvidenceRepository } from 'app/main/core/repositories/evidence/evidence.repository';
import { DialogsService } from 'app/main/core/services/ui/dialog.service';
import { IEvidence } from 'app/main/core/repositories/evidence/evidence.model';
import * as _ from 'lodash';
import { switchMap } from 'rxjs/operators';

@Component({
	selector: 'evidence-list-page',
	templateUrl: './evidence-list.component.html',
	styleUrls: ['./evidence-list.component.scss']
})
export class EvidenceListComponent {
	displayedColumns = ['name', 'date'];
	dataSource: MatTableDataSource<any>;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	isLoadingResults: boolean;

	constructor(
		private router: Router,
		private evidenceRepo: EvidenceRepository,
		private dialogService: DialogsService
	) {
		this.isLoadingResults = false;
	}

	private createTableData() {
		this.evidenceRepo.getAll()
			.subscribe((res: any) => {
				this.dataSource = new MatTableDataSource(res);
				this.dataSource.paginator = this.paginator;
				this.isLoadingResults = false;
			});
	}

	ngOnInit() {
		this.createTableData();
	}

	evidenceDetails(evidence) {
		this.router.navigate([`/management/evidences/${evidence.id}`]);
	}

	addEvidence() {
		this.dialogService.showAddEvidenceDialog()
			.subscribe(res => this.dataSource.data = [... this.dataSource.data, res]);
	}

	editEvidence(evidence: IEvidence) {
		this.dialogService.showAddEvidenceDialog(evidence)
			.subscribe(res => {
				this.dataSource.data = [...this.dataSource.data.map(e => {
					if (e.id === res.id) {
						return res;
					}
					return e;
				})]
			});
	}

	deleteEvidence(evidence: IEvidence) {
		this.dialogService.showDeleteAlert(evidence.name, 'evidence')
			.pipe(switchMap(() => this.evidenceRepo.delete(evidence)))
			.subscribe(() => {
				_.remove(this.dataSource.data, e => e.id === evidence.id);
				this.dataSource.data = [...this.dataSource.data];
			});
	}

	applyFilter(query: string) {
		this.dataSource.filter = query.trim().toLowerCase();
		this.dataSource.paginator.firstPage();
	}

	refreshEvidences() {
		this.evidenceRepo.getAll()
			.subscribe((res: any) => this.dataSource.data = [...res]);
	}
}