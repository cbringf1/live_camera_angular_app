import { NgModule } from '@angular/core';
import { SiteListComponent } from './list/site-list.component';
import { SiteDetailsComponent } from './details/site-details.component';
import {
    MatTableModule, MatChipsModule, MatIconModule,
    MatButtonModule, MatExpansionModule,
    MatFormFieldModule, MatInputModule,
    MatPaginatorModule, MatRippleModule, MatSelectModule,
    MatSortModule, MatSnackBarModule, MatTabsModule,
    MatGridListModule, MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatMenuModule, MatDatepickerModule
} from '@angular/material';
import { FuseSidebarModule, FuseWidgetModule } from '@fuse/components';
import { SiteCamerasComponent } from '../../components/site-cameras/site-cameras.component';
import { SiteCamerasModule } from '../../components/site-cameras/site-cameras.module';
import { SiteAdminComponent } from './site-admin-base/site-admin-base.component';
import { TruncateModule } from 'ng2-truncate';
import { CommonModule } from '@angular/common';
import { CoreRoutingModule } from '../../core-routing.module';
import { GeolocationService } from '../../services/geo/geolocation.service';
import { AgmCoreModule } from '@agm/core';
import { environment } from 'environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CameraListModule } from '../../components/camera-list/camera-list.modue';
import { SiteMapModule } from '../../components/site-map/site-map.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FuseSharedModule } from '@fuse/shared.module';
import { SiteAdminRoutingModule } from './site-admin-routing.module';
import { SiteownerRepository } from '../../repositories/siteowner/siteowner.repository';
import { DepartmentsModule } from '../../components/departments/departments.module';
import { BrowserModule } from '@angular/platform-browser';
import { EvidenceComponent } from './evidence/evidence.component';
import { EvidenceListComponent } from './evidence-list/evidence-list.component';
import { EvidenceRepository } from '../../repositories/evidence/evidence.repository';
import { EvidenceFormDialogComponent } from '../../pages/dialogs/evidence-form/evidence-form.dialog.component';
import { S3UploadService } from '../../services/upload/s3.upload.service';
import { InboxComponent } from './inbox/inbox.component';
import { InboxRepository } from '../../repositories/inbox/inbox.repository';
import { InboxModule } from './inbox/inbox.module';
import { LoadMessagesGuard } from './inbox/store/guards/load-messages.guard';
import { SelectMessageGuard } from './inbox/store/guards/select-message.guard';

@NgModule({
    declarations: [
        SiteListComponent,
        SiteDetailsComponent,
        SiteAdminComponent,
        EvidenceComponent,
        EvidenceListComponent
    ],
    imports: [
        TruncateModule,
        SiteAdminRoutingModule,
        CameraListModule,
        MatMenuModule,
        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatTableModule,
        MatSlideToggleModule,
        MatProgressSpinnerModule,
        MatGridListModule,
        MatDatepickerModule,
        FuseWidgetModule,
        FuseSidebarModule,
        FuseSharedModule,
        SiteCamerasModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AgmCoreModule.forRoot({
            apiKey: environment.googleApiKey,
            libraries: ["places"]
        }),
        SiteMapModule,
        DepartmentsModule,
        InboxModule
    ],
    exports: [
        SiteListComponent,
        SiteDetailsComponent
    ],
    providers: [
        GeolocationService,
        EvidenceRepository,
        S3UploadService,
        InboxRepository,
        LoadMessagesGuard,
        SelectMessageGuard
    ]
})
export class SiteAdminModule { }