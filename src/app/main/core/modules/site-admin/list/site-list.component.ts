import { Component, NgZone, OnInit, ViewChild, ElementRef } from "@angular/core";
import { MatTableDataSource, MatDialog, MatPaginator } from '@angular/material';
import { ISite } from 'app/main/core/repositories/site-repo/site.model';
import { SiteRepository } from 'app/main/core/repositories/site-repo/site.repository';
import { Router } from '@angular/router';
import { GeolocationService } from 'app/main/core/services/geo/geolocation.service';
import { positionFactory } from 'app/main/core/factories/position.factory';
import { map, filter } from 'rxjs/operators';
import { siteListRowFactory } from 'app/main/core/factories/site-list-row.factory';
import { SiteFormDialogComponent } from 'app/main/core/pages/dialogs/site-form/site-form.dialog.component';
import { NavbarService } from 'app/main/core/services/ui/navbar.service';
import { IPosition } from 'app/main/core/services/geo/position.model';
import { DialogsService } from 'app/main/core/services/ui/dialog.service';
import * as _ from 'lodash';

@Component({
	selector: 'site-list',
	templateUrl: './site-list.component.html',
	styleUrls: ['./site-list.component.scss']
})
export class SiteListComponent implements OnInit {
	displayedColumns = ['name', 'address', 'city', 'state', 'zip', 'cameras'];
	dataSource: MatTableDataSource<any>;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	isLoadingResults: boolean;

	constructor(
		private siteRepo: SiteRepository,
		private router: Router,
		private navbarService: NavbarService,
		private dialogService: DialogsService
	) {
		this.isLoadingResults = false;
	}

	private createTableData() {
		this.siteRepo.getAll()
			.subscribe(sites => {
				this.dataSource = new MatTableDataSource(sites.map(s => siteListRowFactory(s)));
				this.dataSource.paginator = this.paginator;
				this.isLoadingResults = false;
			})
	}

	ngOnInit() {
		this.createTableData();
	}

	siteDetails(site) {
		this.router.navigate([`/management/sites/${site.id}`]);
	}

	addSite() {
		this.dialogService.showAddSiteDialog()
			.subscribe(site => {
				this.navbarService.addSiteItem(site);
				this.siteRepo.reload();
			});
	}

	applyFilter(query: string) {
		this.dataSource.filter = query.trim().toLowerCase();
		this.dataSource.paginator.firstPage();
	}

	refreshSites() {
		this.siteRepo.reload();
	}
}