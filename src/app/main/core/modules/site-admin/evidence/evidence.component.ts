import { Component, ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { EvidenceRepository } from 'app/main/core/repositories/evidence/evidence.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogsService } from 'app/main/core/services/ui/dialog.service';
import { IEvidence } from 'app/main/core/repositories/evidence/evidence.model';
import { switchMap, map } from 'rxjs/operators';
import { pipe } from 'rxjs';
import * as _ from 'lodash';
import { ISite } from 'app/main/core/repositories/site-repo/site.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SiteRepository } from 'app/main/core/repositories/site-repo/site.repository';

@Component({
	selector: 'evidence-page',
	templateUrl: './evidence.component.html',
	styleUrls: ['./evidence.component.scss']
})
export class EvidenceComponent implements OnInit {
	displayedColumns = ['name', 'date', 'actions'];
	dataSource: MatTableDataSource<any>;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	isLoadingResults: boolean;
	evidence: any;
	sitesList: ISite[];
	form: FormGroup;
	httpError: string;

	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		private evidenceRepo: EvidenceRepository,
		private siteRepo: SiteRepository,
		private dialogService: DialogsService,
		private route: ActivatedRoute
	) {
		this.isLoadingResults = false;
		this.form = this.formBuilder.group({
			name: ['', Validators.required],
			location_id: ['', Validators.required],
			date: ['', Validators.required]
		});
	}

	ngOnInit() {
		this.route.paramMap
			.pipe(map(p => p.get('id')))
			.pipe(switchMap(evidenceId => this.evidenceRepo.get(evidenceId)))
			.subscribe(res => {
				this.evidence = res;
				this.createTableData();
				this.form.patchValue(_.pick(this.evidence, [
					'name',
					'date'
				]));
				this.form.patchValue({
					location_id: this.evidence.location.id
				});
			});
		this.siteRepo.getAll().subscribe(res => this.sitesList = res);
	}

	private createTableData() {
		this.evidenceRepo.getAllMedia(this.evidence.id)
			.subscribe((res: any) => {
				this.dataSource = new MatTableDataSource(res);
				this.dataSource.paginator = this.paginator;
				this.isLoadingResults = false;
			});
	}

	evidenceDetails(evidence) {
		this.router.navigate([`/management/evidences/${evidence.id}`]);
	}

	addEvidence() {
		this.dialogService.showUploadMediaDialog(this.evidence.id)
			.subscribe(res => this.dataSource.data = [... this.dataSource.data, res]);
	}

	deleteEvidence() {
		this.dialogService.showDeleteAlert(this.evidence.name, 'evidence')
			.pipe(switchMap(() => this.evidenceRepo.delete(this.evidence)))
			.subscribe(() => this.router.navigate(['/management/evidences']));
	}

	editEvidenceMedia(media: any) {
		this.dialogService.showUploadMediaDialog(this.evidence.id, media)
			.subscribe(res => {
				let index = _.findIndex(this.dataSource.data, i => i.id === res.id);
				let aux = this.dataSource.data;

				aux[index] = res;
				this.dataSource.data = [...aux];
			});
	}

	deleteEvidenceMedia(media: any) {
		this.dialogService.showDeleteAlert(media.name, 'media')
			.pipe(switchMap(() => this.evidenceRepo.deleteMedia(media, this.evidence.id)))
			.subscribe(() => {
				_.remove(this.dataSource.data, e => e.id === media.id);
				this.dataSource.data = [...this.dataSource.data];
			});
	}

	applyFilter(query: string) {
		this.dataSource.filter = query.trim().toLowerCase();
		this.dataSource.paginator.firstPage();
	}

	refreshSites() {
		//this.siteRepo.reload();
	}
}