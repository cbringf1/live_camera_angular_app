import { IPlayerSize } from '../components/player/drivers/player-size.model';

export const CAMERA_MARKER_ICON = {
	url: '/assets/icons/markers/video_camera.svg',
	scaledSize: {
		width: 25,
		height: 25
	}
};

export const CAMERA_MARKER_ICON_PUBLIC = {
	url: '/assets/icons/markers/camera-public.svg',
	scaledSize: {
		width: 25,
		height: 25
	}
};

export const CAMERA_MARKER_ICON_SELECTED = {
	url: '/assets/icons/markers/video_camera_selected.svg',
	scaledSize: {
		width: 25,
		height: 25
	}
};

export const MARKER_PLAYER_SIZE: IPlayerSize = {
	height: 200,
	width: 300
};