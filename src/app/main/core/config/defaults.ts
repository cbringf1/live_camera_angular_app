export const defaults = {
  version: "v1.5.1",
  user_key: "X",
  token_key: "X",
  position: {
    lat: 38.004145,
    lng: -95.827462
  },
  siteoOwner: {
    lat: 33.97227,
    lon: -84.22153
  },
  tokenPurchaseFormUrl: "X",
  backgroundImage: "/assets/images/backgrounds/camera-bg.jpg",
  loginLogoImage: "assets/images/logos/fusus-registry-logo.svg",
  emailConfirmationBackgroundImage:
    "/assets/images/backgrounds/email_confirmation.jpg",
  logo: "assets/images/logos/fususHorizonalWhite.svg",
  noMediaLogo: "assets/images/default/no_media.png"
};
