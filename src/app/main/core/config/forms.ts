import { Validators } from '@angular/forms';
import { ISite } from '../repositories/site-repo/site.model';
import { defaults } from './defaults';
import { phoneValidator } from '../validators/phone.validator';
import { IUserModel } from '../services/auth/user.model';

export const siteFormGroupData = (site: ISite = null, user: IUserModel) => {
	const primaryPhone = site && site.primary_contact_phone ?
		site.primary_contact_phone.slice(2) : null;
	const secondaryPhone = site && site.secondary_contact_phone ?
		site.secondary_contact_phone.slice(2) : null;

	return {
		fullAddress: [''],
		address: ['', Validators.required],
		name: [site ? site.name : '', Validators.required],
		lat: [
			site ? site.lat : 0,
			Validators.required
		],
		lon: [
			site ? site.lon : 0,
			Validators.required
		],
        type: [site ? site.type : '', Validators.required],
		state: ['', Validators.required],
		zip: ['', Validators.required],
		city: ['', Validators.required],
		primary_contact_name: [site && site.primary_contact_name ?
			site.primary_contact_name : `${user.first_name} ${user.last_name}`],
		primary_contact_email: [site && site.primary_contact_email ?
			site.primary_contact_email : user.email, Validators.email],
		primary_contact_phone: [primaryPhone, phoneValidator('primary_contact_phone')],
		secondary_contact_name: [site && site.secondary_contact_name ?
			site.secondary_contact_name : null],
		secondary_contact_email: [site && site.secondary_contact_email ?
			site.secondary_contact_email : null, Validators.email],
		secondary_contact_phone: [secondaryPhone, phoneValidator('secondary_contact_phone')]
	}
};