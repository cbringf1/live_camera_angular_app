import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { DvrNvr, ContactInfo } from './dnr-nvr.model';
import { CompatibilityCheck, CompatibilityCheckResponse } from './dnr-nvr.model';

@Injectable()
export class RecordersRepository {
    private endpoint = '/api/camerarecorders/';
    private endpoint_compatibility = '/api/locations/check_compatibility/';
    private endpoint_contact = '/api/locations/compatibility_contact/';

    constructor(private http: HttpClient) { }

    createDvrNvr(dvrNvr: DvrNvr) {
        return this.http.post(`${environment.apiHost}${this.endpoint}`, dvrNvr)
            .pipe(map(res => res as DvrNvr));
    }

    patch(dvrNvr: DvrNvr) {
        return this.http.patch(
            `${environment.apiHost}${this.endpoint}${dvrNvr.id}/`, _.omit(dvrNvr, ['id']))
            .pipe(map(res => res as DvrNvr));
    }

    compatibilityCheckFx(compatibilityCheck: CompatibilityCheck) {
        return this.http.post(
            `${environment.apiHost}${this.endpoint_compatibility}`, compatibilityCheck)
            .pipe(map(res => res as CompatibilityCheckResponse));
    }

    sendContactInfo(contactInfo: ContactInfo) {
        return this.http.post(
            `${environment.apiHost}${this.endpoint_contact}`, contactInfo)
            .pipe(map(res => res));
    }
}