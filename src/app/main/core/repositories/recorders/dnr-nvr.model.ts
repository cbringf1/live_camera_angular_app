export interface DvrNvr {
    id?: string;
    name: string;  
    location: string;
    make: string;
    model: string;
    type: string;
    host: string;
    username: string;
    password: string;
    is_rtsp_supported: boolean;
}

export interface CompatibilityCheck {
    recorder_make?: string;
    recorder_model?: string;
    recorder_type?: string;
    camera_type?: string;
    recorder_support_rtsp?: string;
}

export interface CompatibilityCheckResponse {
    system_compatible?: boolean;
}

export interface ContactInfo {
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
}