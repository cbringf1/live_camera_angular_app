import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';
import { IMessage } from './message.model';

@Injectable()
export class InboxRepository {
	constructor(private http: HttpClient) { }

	getAll() {
		return this.http.get(`${environment.apiHost}/api/messages/`)
			.pipe(map((res: { results: IMessage[] }) => res.results));
	}

	markAsRead(messageId: string) {
		return this.http.patch(`${environment.apiHost}/api/messages/${messageId}/`, {
			is_seen: true
		}).pipe(map(res => res as IMessage));
	}
}