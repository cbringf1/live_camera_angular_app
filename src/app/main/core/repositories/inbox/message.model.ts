export interface IMessage {
	id?: string;
	subject: string;
	body: string;
	is_seen: string;
	sender_name: string;
	sender_organization: string;
	sender_mail: string;
}