import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICameraRecorder } from './camera-recorders.model';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { testCameras } from '../../components/player/test-camera';
import { of } from 'rxjs';

@Injectable()
export class CameraRecorderRepository {
    constructor(private http: HttpClient) { }
    bulk(camerarecorder: ICameraRecorder[]) {
        return this.http.patch(`${environment.apiHost}/api/cameras_camerarecorders/bulk/`, camerarecorder)
            .pipe(map((res: any) => res.results));
    }

}