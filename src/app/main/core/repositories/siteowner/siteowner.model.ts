export interface ISiteowner {
	id?: string;
	background_url: string;
	domain: string;
	favico_url: string;
	is_shared: boolean;
    lat: number;
    lon: number;
    logo_url: string;
    login_logo_url?: string;
	name: string;
	support_url: string;
    email_confirmation_url: string;
}