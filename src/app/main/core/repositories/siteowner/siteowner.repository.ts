import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';
import { ISiteowner } from './siteowner.model';

@Injectable()
export class SiteownerRepository {
	constructor(private http: HttpClient) { }

    getAll(siteId: string, pageSize: number, pageIndex: number, query: string) {
        let domain = window.location.hostname.split('.')[0];

		return this.http.get(`${environment.apiHost}/api/siteowners/`, {
            params: {
                'domain': domain,
				'location': siteId,
				'page': `${pageIndex}`,
				'page_size': `${pageSize}`,
				'search': query || ''
			}
		})
			.pipe(map((res: any) => {
				return {
					results: res.results as ISiteowner[],
					total: res.count
				}
			}));
	}

	share(domain: string, siteId: string) {
		return this.http.post(
			`${environment.apiHost}/api/siteowners/${domain}/share/?location=${siteId}`, {});
	}

	deleteShare(domain: string, siteId: string) {
		return this.http.delete(
			`${environment.apiHost}/api/siteowners/${domain}/share/?location=${siteId}`);
	}
}