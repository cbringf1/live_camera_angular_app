import { ICamera } from '../camera-repo/camera.model';

export interface ISite {
    id?: string;
    name: string;
    lat: number;
    lon: number;
    address: string;
    type?: string;
    city: string;
    state: string;
    zip: string;
    domain?: string;
    cameras: ICamera[];
    fullAddress?: string;
    primary_contact_phone?: string;
    primary_contact_email?: string;
    primary_contact_name?: string;
    secondary_contact_phone?: string;
    secondary_contact_email?: string;
    secondary_contact_name?: string;
}