import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISite } from './site.model';
import { environment } from 'environments/environment';
import { map, concat } from 'rxjs/operators';
import * as _ from 'lodash';
import { Subject, Observable, of } from 'rxjs';

@Injectable()
export class SiteRepository {
	private siteListSubject: Subject<ISite[]>;
	private sitesRequestObservable: Observable<ISite[]>;
	private sites: ISite[];
	private endpoint = '/api/locations/';
	private total: number;

	constructor(private http: HttpClient) {
		this.siteListSubject = new Subject();
	}

    create(site: ISite) {
        let newSite = { ...site, domain: window.location.hostname.split('.')[0] };

		return this.http.post(`${environment.apiHost}${this.endpoint}`, newSite)
			.pipe(map(res => res as ISite));
	}

	getAll() {
		if (!this.sites) {
			if (!this.sitesRequestObservable) {
				this.sitesRequestObservable = this.loadAll();
			}
			return this.sitesRequestObservable
				.pipe(concat(this.siteListSubject.asObservable()));
		}
		else {
			return of(this.sites)
				.pipe(concat(this.siteListSubject.asObservable()));
		}
	}

	find(page: number = -1, pageSize: number = -1, query: string = null) {
		const queryFields = [['page', page], ['page_size', pageSize], ['search', query]]
			.filter(p => {
				if (typeof p[1] === 'string') {
					return !!p[1];
				}
				else {
					return p[1] > -1;
				}
			});
		return this.http.get(`${environment.apiHost}${this.endpoint}`, {
			params: _.fromPairs(queryFields)
		})
			.pipe(map((res: any) => {
				this.total = res.count;
				return res.results as ISite[];
			}));
	}

	private loadAll() {
		return this.http.get(`${environment.apiHost}${this.endpoint}`)
			.pipe(map((res: any) => {
				this.total = res.count;
				return res.results as ISite[];
			}))
			.pipe(map(res => {
				this.sites = res;
				return this.sites;
			}));
	}

	getById(siteId: string) {
		return this.http.get(`${environment.apiHost}${this.endpoint}${siteId}/`)
			.pipe(map(res => res as ISite));
	}

	patch(site: ISite) {
		return this.http.patch(
			`${environment.apiHost}${this.endpoint}${site.id}/`, _.omit(site, ['id']))
			.pipe(map(s => s as ISite));
	}

	delete(site: ISite) {
		return this.http.delete(`${environment.apiHost}${this.endpoint}${site.id}/`);
	}

	reload(fn: Function = () => { }) {
		this.loadAll()
			.subscribe(res => {
				this.siteListSubject.next(res);
				fn();
			});
		return this.siteListSubject.asObservable();
	}

	reloadListener() {
		return this.siteListSubject.asObservable();
    }
    
    getSitesCount() {
        return this.http.get(`${environment.apiHost}${this.endpoint}`)
            .pipe(map((res: any) => {
                return res.count;
            }));
    }
}