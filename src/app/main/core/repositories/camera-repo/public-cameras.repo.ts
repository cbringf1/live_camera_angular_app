import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';
import { ICamera } from './camera.model';
import { SKIP_INTERCEPTOR_HEADER } from '../../config/consts';
import { publicCameraMapper } from '../../services/utils/mappers/public-camera.mapper';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { _MatPaginatorBase } from '@angular/material';
import { v4 as uuid } from 'uuid';

@Injectable()
export class PublicCamerasRepository {
	constructor(private http: HttpClient) { }

	getAll(): Observable<ICamera[]> {
		return this.http
			.get(`${environment.fieldwatchApi}/api/public/cameras/`, {
				headers: new HttpHeaders().append(SKIP_INTERCEPTOR_HEADER, 'true')
			})
			.pipe(map((res: any) =>
				res.results.map(c => new Object({ ...c, id: uuid() })) as any[]));
	}


	get(id: string) {
		return this.http.get(`${environment.fieldwatchApi}/api/public/cameras/${id}/`, {
			headers: new HttpHeaders().append(SKIP_INTERCEPTOR_HEADER, 'true')
		})
			.pipe(map(res => publicCameraMapper(res)));
	}
}