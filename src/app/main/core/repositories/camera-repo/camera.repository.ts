import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICamera } from './camera.model';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { testCameras } from '../../components/player/test-camera';
import { of } from 'rxjs';

@Injectable()
export class CameraRepository {
    constructor(private http: HttpClient) { }

    create(camera: ICamera) {
        return this.http.post(`${environment.apiHost}/api/cameras/`, camera)
            .pipe(map(c => c as ICamera));
    }

    getDetails(camera: ICamera) {
        return of(camera);
    }

    bulk(cameras: ICamera[]) {
        return this.http.patch(`${environment.apiHost}/api/cameras/bulk/`, cameras)
            .pipe(map((res: any) => res.results));
    }

    getAll() {
        return this.http.get(`${environment.apiHost}/api/cameras/`)
            .pipe(map((res: any) => res.results as ICamera[]));
    }

    patch(camera: ICamera) {
        return this.http.patch(
            `${environment.apiHost}/api/cameras/${camera.id}/`, _.omit(camera, ['id']))
            .pipe(map(c => c as ICamera));
    }

    delete(camera: ICamera) {
        return this.http.delete(`${environment.apiHost}/api/cameras/${camera.id}/`);
    }
}