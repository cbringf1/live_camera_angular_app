export interface ICamera {
	id?: string;
	name: string;
	location: string;
	lat: number;
	lon: number;
	distance_ft?: number;
	fov?: number;
	height_ft?: number;
	make?: string;
	model?: string;
	resolution?: string;
	streamType: string;
	imageStreamUrl?: string;
	imageStreamRefreshTime: number;
	liveUrls: { [key: string]: string };
    publicToken: string;
    host?: string;
    username: string;
    password: string;
    is_rtsp_supported: boolean;
    type?: string;
}