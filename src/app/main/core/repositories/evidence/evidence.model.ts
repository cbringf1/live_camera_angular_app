export interface IEvidence {
	id?: string;
	name: string;
	center_latitude: number;
	center_longitude: number;
}