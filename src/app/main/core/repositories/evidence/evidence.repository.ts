import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { IEvidence } from './evidence.model';
import { map, filter, switchMap } from 'rxjs/operators';
import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';
import { S3UploadService } from '../../services/upload/s3.upload.service';

@Injectable()
export class EvidenceRepository {
	constructor(
		private http: HttpClient,
		private uploadService: S3UploadService
	) { }

	get(evidenceId: string) {
		return this.http.get(`${environment.apiHost}/api/evidences/${evidenceId}/`)
			.pipe(map(res => res as IEvidence));
	}

	getAll() {
		return this.http.get(`${environment.apiHost}/api/evidences/`)
			.pipe(map((res: any) => res.results as IEvidence[]));
	}

	getAllMedia(evidenceId: string) {
		return this.http.get(`${environment.apiHost}/api/evidences/${evidenceId}/media/`)
			.pipe(map((res: any) => res.results as any[]));
	}

	create(evidence: IEvidence) {
		return this.http.post(`${environment.apiHost}/api/evidences/`, evidence)
			.pipe(map(e => e as IEvidence));
	}

	createMedia(media: any, evidenceId: string) {
		return this.http.post(`${environment.apiHost}/api/evidences/${evidenceId}/media/`, media);
	}

	update(evidence: IEvidence) {
		return this.http.patch(`${environment.apiHost}/api/evidences/${evidence.id}/`, evidence);
	}

	patch(evidenceMedia: any, evidenceId: string) {
		return this.http.patch(
			`${environment.apiHost}/api/evidences/${evidenceId}/media/${evidenceMedia.id}/`,
			evidenceMedia
		);
	}

	uploadMediaFile(file: File, mediaRes: any) {
		return this.uploadService.upload(file, mediaRes.aws_credentials);
	}

	delete(evidence: IEvidence) {
		return this.http.delete(`${environment.apiHost}/api/evidences/${evidence.id}/`);
	}

	deleteMedia(media: any, evidenceId: string) {
		return this.http.delete(`${environment.apiHost}/api/evidences/${evidenceId}/media/${media.id}/`);
	}
}