import { Routes } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuard } from './auth/auth.guard';
import { LiveMapComponent } from './pages/live-map/live-map.component';
import { SiteAdminComponent } from './modules/site-admin/site-admin-base/site-admin-base.component';
import { SiteListComponent } from './modules/site-admin/list/site-list.component';
import { SiteDetailsComponent } from './modules/site-admin/details/site-details.component';

export const routes: Routes = [
	{
		path: '',
		component: DashboardComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: '',
				redirectTo: '/map/_full',
				pathMatch: 'full'
			},
			{
				path: 'map/:id',
				component: LiveMapComponent
			}
		]
	},
];