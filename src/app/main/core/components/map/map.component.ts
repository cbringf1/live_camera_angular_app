declare let SLDP: any;

import { Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter, NgZone, ViewChild, ElementRef } from "@angular/core";
import { ISite } from '../../repositories/site-repo/site.model';
import { positionFactory } from '../../factories/position.factory';
import { filter, take, first } from 'rxjs/operators';
import * as turf from '@turf/turf';
import { ClickClusterEventResolverService } from '../../services/utils/click-cluster-event-resolver.service';
import { centerChange, mapClick, zoomChange } from '../../services/utils/click-cluster-event.steps';
import { IPosition } from '../../services/geo/position.model';
import { mapStyles } from '../../services/utils/map.styles';
import { defaults } from '../../config/defaults';
import { GeolocationService } from '../../services/geo/geolocation.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { ICamera } from '../../repositories/camera-repo/camera.model';
import {
	CAMERA_MARKER_ICON, CAMERA_MARKER_ICON_SELECTED,
	MARKER_PLAYER_SIZE, CAMERA_MARKER_ICON_PUBLIC
} from '../../config/markers';
import { IPlayerSize } from '../player/drivers/player-size.model';
import { CameraRepository } from '../../repositories/camera-repo/camera.repository';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import { AgmInfoWindow } from '@agm/core';
import { PlayerComponent } from '../player/player.component';
import { PlayerService } from '../player/services/player.service';
import { PublicCamerasRepository } from '../../repositories/camera-repo/public-cameras.repo';
import { publicCameraMapper } from '../../services/utils/mappers/public-camera.mapper';
import { LayerType } from '../layers/layers';

@Component({
	selector: 'map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
	@Input() sites: ISite[];
	@Input() selectedSite: ISite;
	@Output() clusterClick: EventEmitter<ISite>;
	@Output() mapReady: EventEmitter<{}>;
	@Output() markerClick: EventEmitter<ISite>;
	@Output() cameraDetails: EventEmitter<{ camera: ICamera, site: ISite }>;
	@Output() deleteCamera: EventEmitter<ICamera>;
	@Output() onCameraFocus: EventEmitter<ICamera>;

	@ViewChild('player') player: PlayerComponent;
	mapStyles = mapStyles;
	private selectedSiteZoom: number = 22;
	private selectedClusterMaxZoom: number = 17;
	private position: IPosition;
	private _nativeMap: google.maps.Map;
	private openCameraInfoWindow: string;
	private geolocationSubscription: Subscription;
	private currentZoom: number;
	private cameraIcon: any;
	private cameraIconPublic: any;
	private selectedCameraIcon: any;
	private playerSize: IPlayerSize;
	private publicCameras: Observable<ICamera[]>;
	private layers = { [LayerType.MY_CAMERAS]: true };
	ready: boolean;

	constructor(
		private clusterClickResolverService: ClickClusterEventResolverService,
		private geolocationService: GeolocationService,
		private route: ActivatedRoute,
		private cameraRepo: CameraRepository,
		private sanitizer: DomSanitizer,
		private playerService: PlayerService,
		private publicCamerasRepo: PublicCamerasRepository
	) {
		this.clusterClick = new EventEmitter();
		this.mapReady = new EventEmitter();
		this.markerClick = new EventEmitter();
		this.cameraDetails = new EventEmitter();
		this.deleteCamera = new EventEmitter();
		this.onCameraFocus = new EventEmitter();
		this.cameraIcon = CAMERA_MARKER_ICON;
		this.cameraIconPublic = CAMERA_MARKER_ICON_PUBLIC;
		this.selectedCameraIcon = CAMERA_MARKER_ICON_SELECTED;
		this.playerSize = MARKER_PLAYER_SIZE;
	}

	get mapSites() {
		return this.sites.filter(s => this.selectedSite ? s.id !== this.selectedSite.id : true);
	}

	ngOnInit() {
		this.publicCameras = this.publicCamerasRepo.getAll();
		this.clusterClickResolverService.listenClusterEvents()
			.pipe(filter(() => !!this.sites))
			.subscribe(position => {
				const target = turf.point([position.lat, position.lng]);
				const sitesPoints = turf.featureCollection(
					this.sites
						.map(s => turf.point([s.lat, s.lon]))
				);
				const p = turf.nearestPoint(target, sitesPoints);
				const auxSite = this.sites
					.filter(s =>
						s.lat === p.geometry.coordinates[0] &&
						s.lon === p.geometry.coordinates[1])[0];

				this.clusterClick.emit(auxSite);
			});
	}

	private targetMap(position: IPosition) {
		if (this._nativeMap) {
			this._nativeMap.setCenter(position);
			this._nativeMap.setZoom(this.selectedSiteZoom);
		}
	}

	getClusterMaxZoom(siteCluster: string) {
		if (this.selectedSite && this.selectedSite.id === siteCluster) {
			return this.selectedClusterMaxZoom;
		}
		else {
			return 18;
		}
	}

	centerChange(e) {
		this.clusterClickResolverService.addEvent({
			event: centerChange,
			data: e
		});
	}

	mapClick(e) {
		this.clusterClickResolverService.addEvent({
			event: mapClick,
			data: e
		});
	}

	zoomChange(e) {
		this.currentZoom = e;
		this.clusterClickResolverService.addEvent({
			event: zoomChange,
			data: e
		});
	}

	private _mapReady(event: google.maps.Map) {
		this._nativeMap = event;
		//this.setInitialMap();
		this.ready = true;
		this.mapReady.emit({});
	}

	boundClusters(sites: ISite[]) {
		if (!this.ready) return;

		let bounds = new google.maps.LatLngBounds();

		sites.forEach((site: ISite) => {
			bounds.extend(
				new google.maps.LatLng(site.lat, site.lon)
			);
		});
		this._nativeMap.fitBounds(bounds);
		this.sites = sites;
	}

	loadCurrentPosition() {
		let pos = new google.maps.LatLng(defaults.position.lat, defaults.position.lng);

		this._nativeMap.setCenter(pos);
		this.geolocationSubscription = this.geolocationService.getPosition()
			.subscribe(p =>
				this._nativeMap.setCenter(new google.maps.LatLng(p.lat, p.lng)));
	}

	mouseOut(cameraId: string) {
		//this.openCameraInfoWindow = null;
	}

	onMarkerClick(camera: ICamera, publicCamera = false) {
		this.openCameraInfoWindow = camera.id;

		if (publicCamera) {
			this.playerService.play(publicCameraMapper(camera));
		}
	}

	mouseClick(event: ISite) {
		this.markerClick.emit(event);
	}

	setSite(site: ISite) {
		if (!site) return;
		this.selectedSite = site;
		if (this.geolocationSubscription) {
			this.geolocationSubscription.unsubscribe();
			this.geolocationSubscription = null;
		}
		this.targetMap(positionFactory(site.lat, site.lon));
	}

	details(camera: ICamera, site: ISite) {
		this.cameraDetails.emit({
			camera: camera,
			site: site
		});
	}

	delete(camera: ICamera) {
		this.deleteCamera.emit(camera);
	}

	cameraClicked(camera: ICamera) {
		this.playerService.play(camera);
	}

	infoWindowClose(event) {
		//this.openCameraInfoWindow = null;
	}
}
