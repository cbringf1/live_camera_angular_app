import { NgModule } from "@angular/core";
import { CameraListComponent } from './camera-list.component';
import { CommonModule } from '@angular/common';
import {
	MatMenuModule, MatIconModule,
	MatListModule, MatButtonToggleModule,
	MatButtonModule, MatCheckboxModule,
	MatInputModule, MatDialogModule,
	MatFormFieldModule, MatSelectModule,
	MatStepperModule, MatExpansionModule,
	MatTableModule, MatChipsModule, MatPaginatorModule
} from '@angular/material';
import { FuseWidgetModule, FuseSidebarModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

@NgModule({
	declarations: [
		CameraListComponent
	],
	imports: [
		CommonModule,
		MatListModule,
		MatButtonModule,
		MatButtonToggleModule,
		MatMenuModule,
		MatCheckboxModule,
		MatIconModule,
		MatInputModule,
		MatDialogModule,
		MatFormFieldModule,
		MatSelectModule,
		MatStepperModule,
		MatExpansionModule,
		MatTableModule,
		MatChipsModule,
		MatPaginatorModule,
		FuseWidgetModule,
		FuseSidebarModule,
		FuseSharedModule
	],
	exports: [
		CameraListComponent
	]
})
export class CameraListModule { }