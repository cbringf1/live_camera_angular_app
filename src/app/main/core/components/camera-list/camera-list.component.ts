import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter, ViewChild } from "@angular/core";
import { ICamera } from '../../repositories/camera-repo/camera.model';
import { MatTableDataSource, MatDialog, MatPaginator } from '@angular/material';
import { CameraRepository } from '../../repositories/camera-repo/camera.repository';
import { CameraFormDialogComponent } from '../../pages/dialogs/camera-form/camera-form.dialog.component';
import { SiteRepository } from '../../repositories/site-repo/site.repository';
import { filter } from 'rxjs/operators';
import { DeleteAlertDialogComponent } from '../../pages/dialogs/delete-alert/delete-alert.dialog.component';
import { NavbarService } from '../../services/ui/navbar.service';
import { DialogsService } from '../../services/ui/dialog.service';
import { ISite } from '../../repositories/site-repo/site.model';

@Component({
	selector: 'camera-list',
	templateUrl: './camera-list.component.html',
	styleUrls: ['./camera-list.component.scss']
})
export class CameraListComponent implements OnChanges {
	@Input() cameras: ICamera[];
	@Input() site: ISite;
	@Output() selectedCamera: EventEmitter<ICamera>;
	@Output() cameraDeleted: EventEmitter<ICamera>;
	@Output() cameraUpdated: EventEmitter<ICamera>;
	displayedColumns = ['name', 'actions'];
	dataSource: MatTableDataSource<ICamera>;

	@ViewChild(MatPaginator) paginator: MatPaginator;

	constructor(
		private cameraRepo: CameraRepository,
		private dialog: MatDialog,
		private siteRepo: SiteRepository,
		private dialogService: DialogsService) {
		this.selectedCamera = new EventEmitter();
		this.cameraDeleted = new EventEmitter();
		this.cameraUpdated = new EventEmitter();
		if (this.cameras) {
			this.dataSource = new MatTableDataSource(this.cameras);
			this.dataSource.paginator = this.paginator;
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.cameras && !changes.cameras.firstChange) {
			this.dataSource = new MatTableDataSource(this.cameras);
			this.dataSource.paginator = this.paginator;
		}
	}

	selectCamera(camera: ICamera) {
		this.selectedCamera.emit(camera);
	}

	editCamera(camera: ICamera) {
		this.dialogService.showEditCameraDialog(camera, this.site)
			.subscribe(c => this.cameraUpdated.emit(c));
	}

	deleteCamera(camera: ICamera) {
		this.dialogService.showDeleteAlertDialog(camera.name, 'camera', camera)
			.subscribe(() => this.cameraDeleted.emit(camera));
	}
}