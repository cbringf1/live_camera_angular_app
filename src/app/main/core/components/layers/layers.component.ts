import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatSelectionList } from '@angular/material';
import { layers } from './layers';

@Component({
	selector: 'layers',
	templateUrl: './layers.component.html',
	styleUrls: ['./layers.component.scss']
})
export class LayersComponent {
	@Input() padding: number;
	@Output() layerChanged: EventEmitter<any>;
	@ViewChild('options') optionsList: MatSelectionList;

	layers = layers;

	constructor() {
		this.layerChanged = new EventEmitter();
	}

	toggleLayer(layer) {
		layer.enabled = !layer.enabled;
		this.layerChanged.emit(layer);
	}
}