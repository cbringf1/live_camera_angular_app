export enum LayerType {
	PUBLIC_CAMERAS = 'p-c',
	MY_CAMERAS = 'm-c'
}

export const layers = [//{
	// type: LayerType.MY_CAMERAS,
	// name: 'My Cameras',
	// enabled: true
/*},*/ {
	type: LayerType.PUBLIC_CAMERAS,
	name: 'Public Cameras',
	enabled: false,
	icon: 'videocam'
}];