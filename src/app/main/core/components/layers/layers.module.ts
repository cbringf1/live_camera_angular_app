import { NgModule } from '@angular/core';
import { LayersComponent } from './layers.component';
import { MatListModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
	declarations: [
		LayersComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		MatListModule,
		MatIconModule,
		MatTooltipModule
	],
	exports: [
		LayersComponent
	]
})
export class LayersModule {}