import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ISiteowner } from '../../repositories/siteowner/siteowner.model';
import { SiteownerRepository } from '../../repositories/siteowner/siteowner.repository';
import { ISite } from '../../repositories/site-repo/site.model';
import { query } from '@angular/core/src/render3';

@Component({
	selector: 'departments',
	templateUrl: './departments.component.html',
	styleUrls: ['./departments.component.scss']
})
export class DepartmentsComponent implements OnChanges {
	displayedColumns = ['name', 'state', 'share'];
	dataSource: MatTableDataSource<any>;
	@Input() site: ISite;
	@Input() query: string;
	private totalDepartments = 0;
	private pageSize = 5;
	private currentPage = 1;

	constructor(private siteownerRepo: SiteownerRepository) { }

	ngOnChanges(changes: SimpleChanges) {
		if (changes.site && changes.site.currentValue) {
			this.createTable();
		}
	}

	private createTable(query = null) {
		this.siteownerRepo.getAll(this.site.id, this.pageSize, this.currentPage, query || this.query)
			.subscribe(res => {
				this.dataSource = new MatTableDataSource(res.results);
				this.totalDepartments = res.total;
			});
	}

	toggleShared(data: ISiteowner) {
		if (!this.site) return;
		if (!data.is_shared) {
			this.siteownerRepo.share(data.domain, this.site.id)
				.subscribe((res: any) => data.is_shared = res.is_shared);
		}
		else {
			this.siteownerRepo.deleteShare(data.domain, this.site.id)
				.subscribe((res: any) => data.is_shared = res.is_shared);
		}

	}

	refreshDepartments() {
		this.createTable();
	}

	paginatorEvent(event) {
		this.currentPage = event.pageIndex + 1;
		this.pageSize = event.pageSize;
		this.createTable();
	}

	search(query) {
		this.createTable(query);
	}
}