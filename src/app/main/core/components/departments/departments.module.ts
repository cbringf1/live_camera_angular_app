import { NgModule } from '@angular/core';
import { DepartmentsComponent } from './departments.component';
import { CommonModule } from '@angular/common';
import { SiteownerRepository } from '../../repositories/siteowner/siteowner.repository';
import { MatTableModule, MatSlideToggleModule, MatPaginatorModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';

@NgModule({
	declarations: [
		DepartmentsComponent
	],
	imports: [
		CommonModule,
		MatTableModule,
		MatSlideToggleModule,
		MatPaginatorModule,
		FuseSharedModule
	],
	exports: [
		DepartmentsComponent
	],
	providers: [
		SiteownerRepository
	]
})
export class DepartmentsModule {}