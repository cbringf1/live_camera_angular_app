import { NgModule } from '@angular/core';
import { ErrorMsgComponent } from './error-msg.component';
import { CommonModule } from '@angular/common';

@NgModule({
	declarations: [
		ErrorMsgComponent
	],
	imports: [
		CommonModule
	],
	exports: [
		ErrorMsgComponent
	]
})
export class ErrorMsgModule {}