import { Component, Input, Optional, Host } from '@angular/core';
import { SatPopover } from '@ncstate/sat-popover';
import { filter } from 'rxjs/operators/filter';

@Component({
    selector: './inline-edit',
    templateUrl: './inline-edit.component.html',
    styleUrls: ['./inline-edit.component.scss']
})
export class InlineEditComponent {
    info: string;
    private _value: string;

    @Input() field: string;

    @Input() get value(): string {
        return this._value;
    }

    set value(x: string) {
        this.info = this._value = x;
    }

    constructor(@Optional() @Host() public popover: SatPopover) {
        this.info = '';
        this._value = '';
    }

    ngOnInit() {
        if (this.popover) {
            this.popover.closed.pipe(filter(val => val == null))
                .subscribe(() => this.info = this.value || '');
        }
    }

    onSubmit() {
        if (this.popover) {
            this.popover.close({ info: this.info, field: this.field });
        }
    }

    onCancel() {
        if (this.popover) {
            this.popover.close();
        }
    }
}