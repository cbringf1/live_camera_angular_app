import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import {
    MatMenuModule, MatIconModule,
    MatListModule, MatButtonToggleModule,
    MatButtonModule, MatCheckboxModule,
    MatInputModule, MatDialogModule,
    MatFormFieldModule, MatSelectModule,
    MatStepperModule, MatExpansionModule,
    MatTableModule, MatChipsModule, MatPaginatorModule
} from '@angular/material';
import { FuseWidgetModule, FuseSidebarModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { InlineEditComponent } from './inline-edit.component';

@NgModule({
    declarations: [
        InlineEditComponent
    ],
    imports: [
        CommonModule,
        MatListModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatMenuModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSelectModule,
        MatStepperModule,
        MatExpansionModule,
        MatTableModule,
        MatChipsModule,
        MatPaginatorModule,
        FuseWidgetModule,
        FuseSidebarModule,
        FuseSharedModule
    ],
    exports: [
        InlineEditComponent
    ]
})
export class InlineEditModule { }