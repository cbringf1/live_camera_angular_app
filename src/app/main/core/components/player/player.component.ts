import {
	Component, Input,
	OnInit, OnDestroy, ViewChild,
	ElementRef, Renderer2,
	AfterViewInit, ChangeDetectionStrategy, NgZone
} from "@angular/core";
import { StreamType } from './stream-type.enum';
import { Subscription, interval, of, timer } from 'rxjs';
import { switchMap, timeout, delay, map, filter } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ImgSrcDirective } from '@angular/flex-layout';
import { ImgStreamPlayerService } from './services/img-stream.player.service';
import * as _ from 'lodash';
import { PlayerState } from './player-state.enum';
import { IPlayerSize } from './drivers/player-size.model';
import { ICamera } from '../../repositories/camera-repo/camera.model';
import { getDriver } from './drivers/driver.register';
import { IStreamDriver } from './drivers/stream.driver';
import { IDriverOptions } from './drivers/driver-options';
import { CameraRepository } from '../../repositories/camera-repo/camera.repository';
import { DomSanitizer } from '@angular/platform-browser';
import { PlayerService } from './services/player.service';
import { PublicCamerasRepository } from '../../repositories/camera-repo/public-cameras.repo';

@Component({
	selector: 'player',
	templateUrl: './player.component.html',
	styleUrls: ['./player.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerComponent implements OnDestroy, AfterViewInit {
	@Input() size: IPlayerSize;
	@Input() left: number;

	@ViewChild('playerContainerRef') private playerContainerRef: ElementRef;

	private player: IStreamDriver;
	private playerOptions: any;

	constructor(
		private cameraRepo: PublicCamerasRepository,
		private ngZone: NgZone,
		private playerService: PlayerService
	) {}

	ngAfterViewInit() {
		this.ngZone.runOutsideAngular(() => {
			this.buildPlayerOptions(this.playerService.activeCamera);
			this.player = getDriver(
				this.playerContainerRef,
				this.playerOptions as IDriverOptions
			);
			this.player.play();
		});
	}

	private buildPlayerOptions(camera: ICamera) {
		this.playerOptions = _.pick(camera, [
			'streamType',
			'imageStreamUrl',
			'imageStreamRefreshTime',
			'liveUrls'
		]);
		this.playerOptions['playerSize'] = this.size || {
			height: 200,
			width: 300
		};
	}

	ngOnDestroy() {
		if (this.player) {
			this.ngZone.run(() => this.player.dispose());
		}
	}
}