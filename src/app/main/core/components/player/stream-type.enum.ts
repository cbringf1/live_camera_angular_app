export enum StreamType {
	IMAGE = 'Image',
	VIDEO = 'Video'
}