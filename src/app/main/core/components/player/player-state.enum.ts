export enum PlayerState {
	LOADING = 'loading',
	PLAYING = 'playing'
}