import { NgModule } from "@angular/core";
import { PlayerComponent } from './player.component';
import { ImgStreamPlayerService } from './services/img-stream.player.service';
import { CameraRepository } from '../../repositories/camera-repo/camera.repository';
import { BrowserModule } from '@angular/platform-browser';
import { PlayerService } from './services/player.service';

@NgModule({
	declarations: [
		PlayerComponent
	],
	imports: [
		BrowserModule
	],
	exports: [
		PlayerComponent
	],
	providers: [
		ImgStreamPlayerService,
		CameraRepository,
		PlayerService
	]
})
export class PlayerModule {}