import { Injectable, ElementRef } from '@angular/core';
import { timer } from 'rxjs';

@Injectable()
export class ImgStreamPlayerService {
	play(frameRef: ElementRef, streamUrl: string, refreshRate: number) {
		return timer(0, refreshRate * 1000)
			.subscribe(() => {
				const auxFrame = document.createElement('img');

				auxFrame.src = streamUrl;
				auxFrame.onload = () =>
					frameRef.nativeElement.setAttribute('src', `${streamUrl}?${Date.now()}`);
			})
	}
}