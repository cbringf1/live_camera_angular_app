import { Injectable, ElementRef, Renderer2 } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ICamera } from 'app/main/core/repositories/camera-repo/camera.model';
import { CameraRepository } from 'app/main/core/repositories/camera-repo/camera.repository';
import { getDriver } from '../drivers/driver.register';
import { IStreamDriver } from '../drivers/stream.driver';
import { IPlayerSize } from '../drivers/player-size.model';
import { Subject, of } from 'rxjs';

@Injectable()
export class PlayerService {
	activeCamera: ICamera;

	private playSubject: Subject<{}>;

	constructor() { 
		this.playSubject = new Subject();
	}

	get ready() {
		return !!this.activeCamera;
	}

	onPlay() {
		return this.playSubject.asObservable();
	}



	play(camera: ICamera) {
		this.activeCamera = camera;
	}

	stop() {
		this.activeCamera = null;
	}
}