import { BaseStreamDriver } from '../base-stream.driver';
import { ElementRef } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { IPlayerSize } from '../player-size.model';
import { loadingElementFactory, frameElementFactory } from './driver-elements.factory';
import { IDriverOptions } from '../driver-options';
import { LOADING_FRAME_ID, IMAGE_FRAME_ID } from './constants';

export class ImageStreamDriver extends BaseStreamDriver {
	private playerSubscription: Subscription;
	private loading: boolean;
	private frameElementRef: HTMLImageElement;

	constructor(
		playerContainerElementRef: ElementRef,
		driverOptions: IDriverOptions
	) {
		super(playerContainerElementRef, driverOptions);
		this.playerElement = document.querySelector(`#${this.playerContainerElementRef.nativeElement.id}`);
		this.setPlayer();
		this.loading = true;
	}

	private setPlayer() {
		this.playerElement.appendChild(loadingElementFactory(
			LOADING_FRAME_ID,
			this.driverOptions.playerSize
		));
	}

	play() {
		this.playerSubscription = timer(0, this.driverOptions.imageStreamRefreshTime * 1000)
			.subscribe(t => {
				const auxFrame = document.createElement('img');

				auxFrame.src = this.driverOptions.imageStreamUrl;
				auxFrame.onload = () => {
					if (this.loading) {
						this.switchFromLoading();
						this.loading = false;
					}
					this.frameElementRef.setAttribute('src', `${this.driverOptions.imageStreamUrl}?${Date.now()}`);
				};
			});
	}

	private switchFromLoading() {
		const loadingElement = document.querySelector(`#${LOADING_FRAME_ID}`);

		this.playerElement.removeChild(loadingElement);
		this.frameElementRef = frameElementFactory(IMAGE_FRAME_ID, this.driverOptions.playerSize);
		this.playerElement.appendChild(this.frameElementRef);
	}

	dispose() {
		this.playerSubscription.unsubscribe();
	}
}