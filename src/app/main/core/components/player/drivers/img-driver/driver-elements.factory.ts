import { IPlayerSize } from '../player-size.model';

export const loadingElementFactory = (id: string, size: IPlayerSize) => {
	const result = document.createElement('span');
	const text = document.createElement('p');

	result.setAttribute('style', `
		height:${size.height}px; 
		width:${size.width}px;
		display:inline-block;
		background-color:black;
		color: white;`
	);
	text.setAttribute('style', `
		text-align: center;
		margin-top: ${(size.height - 10) / 2}px;
	`);
	result.setAttribute('id', id);
	result.setAttribute('class', 'img-loading-frame');
	text.innerText = 'Loading ...';
	result.appendChild(text);
	return result;
}

export const frameElementFactory = (id: string, size: IPlayerSize) => {
	const result = document.createElement('img');

	result.setAttribute('id', id);
	result.setAttribute('style', `height:${size.height}px; width:${size.width}px`);
	return result;
}