export interface IPlayerSize {
	height: number;
	width: number;
}