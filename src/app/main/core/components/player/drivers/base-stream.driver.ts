import { IStreamDriver } from './stream.driver';
import { ElementRef } from '@angular/core';
import { IPlayerSize } from './player-size.model';
import { IDriverOptions } from './driver-options';
import { SafeHtml } from '@angular/platform-browser';

export abstract class BaseStreamDriver implements IStreamDriver {
	protected playerId: string = 'rd-player';
	protected playerElement: HTMLDivElement;

	constructor(
		protected playerContainerElementRef: ElementRef,
		protected driverOptions: IDriverOptions
	) {}

	abstract play();
	abstract dispose();
}