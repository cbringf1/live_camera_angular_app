import { IDisposable } from './disposable';
import { SafeHtml } from '@angular/platform-browser';

export interface IStreamDriver extends IDisposable {
	play();
}