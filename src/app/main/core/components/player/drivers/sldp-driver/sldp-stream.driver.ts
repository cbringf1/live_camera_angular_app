declare const SLDP: any;

import { BaseStreamDriver } from '../base-stream.driver';
import { ElementRef } from '@angular/core';
import { IPlayerSize } from '../player-size.model';
import { IDriverOptions } from '../driver-options';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { of } from 'rxjs';
import { SafeHtml } from '@angular/platform-browser';

export class SldpStreamDriver extends BaseStreamDriver {
	private SLDP: any;
	private sldpPlayer: any;

	constructor(
		playerElementRef: ElementRef,
		driverOptions: IDriverOptions
	) {
		super(playerElementRef, driverOptions);
	}


	play(autoplay: boolean = false, height: number = 480, width: number = 424) {
		this.sldpPlayer = SLDP.init({
			container: this.playerContainerElementRef.nativeElement.id,
			stream_url: this.driverOptions.liveUrls.sldp,
			initial_resolution: '240p',
			buffering: 500,
			autoplay: true,
			height: 200,
			width: width
		});
	}

	dispose() {
		this.sldpPlayer.destroy();
	}
}