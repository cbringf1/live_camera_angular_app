import { StreamType } from '../stream-type.enum';
import { ImageStreamDriver } from './img-driver/img-stream.driver';
import { SldpStreamDriver } from './sldp-driver/sldp-stream.driver';
import { ElementRef } from '@angular/core';
import { IDriverOptions } from './driver-options';

const driverRegister = {
	[StreamType.IMAGE]: ImageStreamDriver,
	[StreamType.VIDEO]: SldpStreamDriver
};

export const getDriver = (playerContainerRef: ElementRef, options: IDriverOptions) => {
	const driverRef = driverRegister[options.streamType];
	const constructorFn = driverRef.bind.apply(driverRef, [driverRef, playerContainerRef, options]);

	return new constructorFn();
};