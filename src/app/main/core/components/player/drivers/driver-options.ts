import { IPlayerSize } from './player-size.model';
import { DomSanitizer } from '@angular/platform-browser';
import { Renderer2, ElementRef } from '@angular/core';

export interface IDriverOptions {
	streamType: string;
	imageStreamUrl: string;
	imageStreamRefreshTime?: number;
	playerSize: IPlayerSize;
	liveUrls: { [key: string]: string };
	htmlSanitizer: DomSanitizer,
	renderer: Renderer2;
	playerRef: ElementRef;
}