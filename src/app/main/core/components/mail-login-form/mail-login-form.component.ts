import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
	selector: 'mail-login-form',
	templateUrl: './mail-login-form.component.html',
	styleUrls: ['./mail-login-form.component.scss']
})
export class MailLoginFormComponent implements OnInit {
	loginForm: FormGroup;

	constructor(private formBuilder: FormBuilder) {}

	ngOnInit(): void {
		this.loginForm = this.formBuilder.group({
			email: ['', [Validators.required, Validators.email]],
			password: ['', Validators.required]
		});
	}
}