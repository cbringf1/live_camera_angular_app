import { NgModule } from "@angular/core";
import { SiteMapComponent } from './site-map.component';
import { AgmCoreModule } from '@agm/core';
import { environment } from 'environments/environment';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material';

@NgModule({
	declarations: [
		SiteMapComponent
	],
	imports: [
		CommonModule,
		AgmCoreModule.forRoot({
			apiKey: environment.googleApiKey
		}),
		AgmJsMarkerClustererModule,
		MatIconModule
	],
	exports: [
		SiteMapComponent
	]
})
export class SiteMapModule { }