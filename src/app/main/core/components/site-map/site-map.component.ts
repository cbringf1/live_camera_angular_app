import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter, OnInit } from "@angular/core";
import { ISite } from '../../repositories/site-repo/site.model';
import { ICamera } from '../../repositories/camera-repo/camera.model';
import { ISiteowner } from '../../repositories/siteowner/siteowner.model';
import { GeolocationService } from '../../services/geo/geolocation.service';
import { positionFactory } from '../../factories/position.factory';
import { MouseEvent } from '@agm/core';
import { IPosition } from '../../services/geo/position.model';
import { switchMap, filter } from 'rxjs/operators';
import { emptySiteFactory } from '../../factories/empty-site.factory';
import * as _ from 'lodash';
import { mapStyles } from '../../services/utils/map.styles';
import { IGeocoderModel } from '../../services/geo/geocoder.model';
import { defaults } from '../../config/defaults';
import { CAMERA_MARKER_ICON, CAMERA_MARKER_ICON_SELECTED } from '../../config/markers';

@Component({
    selector: 'site-map',
    templateUrl: './site-map.component.html',
    styleUrls: ['./site-map.component.scss']
})
export class SiteMapComponent {
    @Input() site: ISite & { address: string };
    @Input() showCameraOnlyActionsName: boolean;
    @Input() showSite: boolean;
    @Input() showSiteNoName: boolean;
    @Input() showCameras: boolean;
    @Input() selectedCameraId: string;
    @Input() selectedCameraDrag: boolean;
    @Input() zoom: number;
    @Input() icon: number;
    @Input() showCameraActions: boolean;
    @Input() isNewCameraToAdd: boolean;
    @Output() siteDragEnd: EventEmitter<{ position: IPosition, address: IGeocoderModel }>;
    @Output() cameraDragEnd: EventEmitter<IPosition>;
    @Output() positionLoaded: EventEmitter<{ position: IPosition, address: IGeocoderModel }>;
    @Output() selectedCameraDragEnd: EventEmitter<IPosition>;
    @Output() cameraDetails: EventEmitter<{ camera: ICamera, site: ISite }>;
    @Output() deleteCamera: EventEmitter<ICamera>;
    @Output() selectedCameraByClick: EventEmitter<ICamera>;
    address: string;
    mapStyles = mapStyles;
    defaultCameraMarkers: any[];
    private defPos = defaults.position;
    private openCameraInfoWindow: string;
    private cameraIcon;
    private selectedCameraIcon;

    constructor(private geolocationService: GeolocationService) {
        this.siteDragEnd = new EventEmitter();
        this.cameraDragEnd = new EventEmitter();
        this.positionLoaded = new EventEmitter();
        this.selectedCameraDragEnd = new EventEmitter();
        this.cameraDetails = new EventEmitter();
        this.deleteCamera = new EventEmitter();
        this.cameraIcon = CAMERA_MARKER_ICON;
        this.selectedCameraIcon = CAMERA_MARKER_ICON_SELECTED;
        this.defaultCameraMarkers = new Array<any>();
        this.selectedCameraByClick = new EventEmitter();
    }

    addDefaultCamMarker(site: ISite) {
        this.defaultCameraMarkers = new Array<any>();
        this.defaultCameraMarkers.push({
            lat: site.lat,
            lon: site.lon,
        });
    }

    computePosition(site: ISite = null, siteowner: ISiteowner = null) {
        if (site) {
            console.log('site compute location');
            this.geolocationService
                .resolveAddress(positionFactory(site.lat, site.lon))
                .subscribe(a => {
                    this.positionLoaded.emit({
                        position: positionFactory(
                            this.site.lat, this.site.lon
                        ),
                        address: a
                    });
                    this.address = a.fullFormattedAddress;
                });
        }
        else if (siteowner) {
            siteowner.lat = siteowner.lat == null ? defaults.siteoOwner.lat : siteowner.lat;
            siteowner.lon = siteowner.lon == null ? defaults.siteoOwner.lon : siteowner.lon;
            
            console.log('sitowner compute location');
            this.geolocationService
                .resolveAddress(positionFactory(siteowner.lat, siteowner.lon))
                .subscribe(a => {
                    this.positionLoaded.emit({
                        position: positionFactory(
                            siteowner.lat, siteowner.lon
                        ),
                        address: a
                    });
                    this.address = a.fullFormattedAddress;
                });
        }
        else {
            let position = null;
            console.log('gps compute location');
            this.geolocationService.getPosition()
                .pipe(switchMap(p => {
                    position = p;
                    this.defPos = p;
                    return this.geolocationService.resolveAddress(p);
                }))
                .subscribe(a => this.positionLoaded.emit({
                    position: position,
                    address: a
                }));
        }
    }

    private validPosition(site: ISite) {
        return site && (site.lat && site.lon);
    }

    dragEnd(data: MouseEvent, site: boolean) {
        const result = positionFactory(data.coords.lat, data.coords.lng);

        if (site) {
            this.geolocationService.resolveAddress(result)
                .subscribe(a => this.siteDragEnd.emit({
                    position: result,
                    address: a
                }));
        }
        else {
            this.cameraDragEnd.emit(result);
        }
    }

    selectedCameraDragEvent(data: MouseEvent) {
        this.selectedCameraDragEnd.emit(
            positionFactory(data.coords.lat, data.coords.lng)
        );
    }

    mouseOver(cameraId: string) {
        this.openCameraInfoWindow = cameraId;
    }

    private _cameraDetails(camera: ICamera, site: ISite) {
        this.cameraDetails.emit({
            camera: camera,
            site: site
        });
    }

    clickedMarker(camera: ICamera){
        this.selectedCameraByClick.emit(camera);
    }
}