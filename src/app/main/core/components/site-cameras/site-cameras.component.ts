import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from "@angular/core";
import { CameraRepository } from '../../repositories/camera-repo/camera.repository';
import { Observable, of } from 'rxjs';
import { ICamera } from '../../repositories/camera-repo/camera.model';
import { CamerasSidebarService } from '../../services/status/cameras-sidebar.service';
import { ISite } from '../../repositories/site-repo/site.model';
import { MatDialog } from '@angular/material';
import { CameraFormDialogComponent } from '../../pages/dialogs/camera-form/camera-form.dialog.component';
import { SiteRepository } from '../../repositories/site-repo/site.repository';
import { SiteFormDialogComponent } from '../../pages/dialogs/site-form/site-form.dialog.component';
import { NavbarService } from '../../services/ui/navbar.service';
import { GeolocationService } from '../../services/geo/geolocation.service';
import { positionFactory } from '../../factories/position.factory';
import { map, filter, switchMap } from 'rxjs/operators';
import { IPosition } from '../../services/geo/position.model';
import { DeleteAlertDialogComponent } from '../../pages/dialogs/delete-alert/delete-alert.dialog.component';
import { DialogsService } from '../../services/ui/dialog.service';

@Component({
	selector: 'site-cameras',
	templateUrl: './site-cameras.component.html',
	styleUrls: ['./site-cameras.component.scss']
})
export class SiteCamerasComponent {
	@Input() site: ISite;
	@Input() selectedCamera: ICamera;
	@Output() cameraUpdated: EventEmitter<ICamera>;
	@Output() siteDeleted: EventEmitter<{}>;
	@Output() siteUpdated: EventEmitter<ISite>;
	@Output() cameraSelected: EventEmitter<ICamera>;	
	address: any;

	constructor(
		private cameraRepo: CameraRepository,
		private siteRepo: SiteRepository,
		public dialog: MatDialog,
		private navbarService: NavbarService,
		private dialogService: DialogsService
	) {
		this.cameraUpdated = new EventEmitter();
		this.siteDeleted = new EventEmitter();
		this.siteUpdated = new EventEmitter();
		this.cameraSelected = new EventEmitter();
	}

	editCamera(camera: ICamera) {
		this.dialogService.showEditCameraDialog(camera, this.site)
			.subscribe(c => this.cameraUpdated.emit(c));
	}

	editSite(site: ISite) {
		this.dialogService.showEditSiteDialog(site)
			.subscribe(s => this.siteUpdated.emit(s));
	}

	deleteSite(site: ISite) {
		this.dialogService.showDeleteAlertDialog(site.name, 'site', site)
			.subscribe(() => {
				this.siteRepo.reload();
				this.navbarService.deleteSiteNavigationItem(site.id);
			});
	}

	deleteCamera(camera: ICamera) {
		this.dialogService.showDeleteAlertDialog(camera.name, 'camera', camera)
			.subscribe(() => {
				this.site.cameras.splice(this.site.cameras.findIndex(c => c.id === camera.id), 1);
				this.navbarService.updateCamerasCountBadge(this.site.id, this.site.cameras.length);
			});
	}

	selectCamera(camera: ICamera) {
		this.cameraSelected.emit(camera);
	}
}