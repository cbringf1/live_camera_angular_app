import { NgModule } from "@angular/core";
import { SiteCamerasComponent } from './site-cameras.component';
import { TruncateModule } from 'ng2-truncate';
import {
	MatMenuModule, MatIconModule,
	MatListModule, MatButtonToggleModule,
	MatButtonModule, MatCheckboxModule,
	MatInputModule, MatDialogModule,
	MatFormFieldModule, MatSelectModule,
	MatStepperModule, MatExpansionModule,
	MatTableModule, MatChipsModule
} from '@angular/material';
import { CommonModule } from '@angular/common';
import { CoreRoutingModule } from '../../core-routing.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule, FuseSidebarModule } from '@fuse/components';

@NgModule({
	declarations: [
		SiteCamerasComponent
	],
	imports: [
		TruncateModule,
		CommonModule,
		MatListModule,
		MatButtonModule,
		MatButtonToggleModule,
		MatMenuModule,
		MatCheckboxModule,
		MatIconModule,
		MatInputModule,
		MatDialogModule,
		MatFormFieldModule,
		MatSelectModule,
		MatStepperModule,
		MatExpansionModule,
		MatTableModule,
		MatChipsModule,
		FuseWidgetModule,
		FuseSidebarModule,
		FuseSharedModule
	],
	exports: [
		SiteCamerasComponent
	]
})
export class SiteCamerasModule { }