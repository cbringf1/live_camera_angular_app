import { ValidatorFn, AbstractControl } from '@angular/forms';
import { parsePhoneNumberFromString } from 'libphonenumber-js';

export const phoneValidator = (fieldName: string): ValidatorFn => {
    return (control: AbstractControl) => {
        if (!control || !control.parent) {
            return null;
        }

        const phone = control.parent.get(fieldName);

        if (!phone || !phone.value) {
            return null;
        }

        const phoneNumber = parsePhoneNumberFromString(`+1${phone.value}`);

        return (phoneNumber && phoneNumber.isValid && phoneNumber.number.length >= 12) ? null :
            { 'invalidFormat': { value: phone.value } };
    };
};