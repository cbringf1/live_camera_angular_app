import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { SampleModule } from 'app/main/sample/sample.module';
import { CoreModule } from 'app/main/core/core.module';
import { Login2Component } from 'app/main/core/pages/login-2/login-2.component';
import { Register2Component } from './main/core/pages/register-2/register-2.component';
import { VerifyAccountComponent } from './main/core/pages/verify-account/verify-account.component';
import { Error404Component } from './main/core/pages/404/error-404.component';
import { ForgotPassword2Component } from './main/core/pages/forgot-password-2/forgot-password-2.component';
import { ResetPassword2Component } from './main/core/pages/reset-password-2/reset-password-2.component';
import { NavbarInitService } from './main/core/services/utils/navbar-init.service';

import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { reducers, CustomSerializer } from './store';
import { environment } from 'environments/environment';

const appRoutes: Routes = [
	{
		path: 'login',
		component: Login2Component

	},
	{
		path: 'register',
		component: Register2Component
	},
	{
		path: 'forgot-password',
		component: ForgotPassword2Component
	},
	{
		path: 'reset_code/:token',
		component: ResetPassword2Component
	},
	{
		path: 'account-confirm/:id',
		component: VerifyAccountComponent
	},
	{
		path: '404',
		component: Error404Component
	},
	{
		path: '**',
		redirectTo: '404'
	}
];

// const config = new AuthServiceConfig([
// 	{
// 		id: GoogleLoginProvider.PROVIDER_ID,
// 		provider: new GoogleLoginProvider('624796833023-clhjgupm0pu6vgga7k5i5bsfp6qp6egh.apps.googleusercontent.com')
// 	}
// ]);

// const config = new AuthServiceConfig([
// 	{
// 		id: GoogleLoginProvider.PROVIDER_ID,
// 		provider: new GoogleLoginProvider('624796833023-clhjgupm0pu6vgga7k5i5bsfp6qp6egh.apps.googleusercontent.com')
// 	}
// ]);

// export function provideConfig() {
// 	return config;
// }

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		StoreModule.forRoot(reducers),
		EffectsModule.forRoot([]),
		StoreRouterConnectingModule,
		environment.production ? [] : StoreDevtoolsModule.instrument(),

		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		RouterModule.forRoot(appRoutes),
		CoreModule,

		TranslateModule.forRoot(),

		// Material moment date module
		MatMomentDateModule,

		// Material
		MatButtonModule,
		MatIconModule,

		// Fuse modules
		FuseModule.forRoot(fuseConfig),
		FuseProgressBarModule,
		FuseSharedModule,
		FuseSidebarModule,
		FuseThemeOptionsModule,

		// App modules
		LayoutModule,
		SampleModule,
		//SocialLoginModule
	],
	providers: [
		NavbarInitService,
		{ provide: RouterStateSerializer, useClass: CustomSerializer }
	],
	bootstrap: [
		AppComponent
	]
})
export class AppModule {
}

// In providers
// {
// 		provide: AuthServiceConfig,
// 		useFactory: provideConfig
// 	}
