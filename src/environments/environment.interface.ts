export interface IEnvironment {
	production: boolean;
	name: string;
	google: { client_id: string; secret_key: string };
	googleApiKey: string;
	apiHost: string;
	hmr: boolean;
	fieldwatchApi: string;
	stripePublishKey: string;
}